import 'package:alcorp_app/app/data/models/new.model.dart';
import 'package:alcorp_app/app/data/repository/news.repository.dart';
import 'package:alcorp_app/app/routes/app_pages.dart';
import 'package:get/get.dart';
import 'package:meta/meta.dart';

class NewsController extends GetxController {
  final NewsRepository repository;
  NewsController({@required this.repository}) : assert(repository != null);

  final _new = New().obs;
  get news => this._new.value;
  set news(value) => this._new.value = value;

  getAll() {
    repository.getAll().then((data) {
      this.news = data ?? New();
    });
  }

  back(post) {
    this.news = post;
    Get.back(canPop: false);
  }
}
