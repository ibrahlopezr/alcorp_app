part of './app_pages.dart';

abstract class Routes {
  static const MAIN = '/';

  static const NEWS = '/news';
}
