import 'package:alcorp_app/app/bindings/news.binding.dart';
import 'package:alcorp_app/app/ui/android/news/news.page.dart';
import 'package:get/get.dart';
part './app_routes.dart';

class AppPages {
  static final pages = [
    GetPage(
      name: Routes.NEWS,
      page: () => NewsPage(),
      binding: NewsBinding(),
    ),
  ];
}
