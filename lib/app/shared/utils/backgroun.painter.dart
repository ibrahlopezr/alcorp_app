import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:flutter/material.dart';

class BackgroundPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    //My paint lapiz
    final paint = new Paint()
      ..color = CustomColors.primaryColor
      ..style = PaintingStyle.fill;

    // final path = new Path()
    //   ..lineTo(size.width * .5, 0)
    //   ..lineTo(0, size.height * .5);
    final path = new Path();
    path.lineTo(0, size.height * .6);
    path.lineTo(size.width, size.height * .3);
    path.lineTo(size.width, 0);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(BackgroundPainter oldDelegate) => false;

  @override
  bool shouldRebuildSemantics(BackgroundPainter oldDelegate) => false;
}
