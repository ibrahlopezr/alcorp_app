import 'dart:math';

import 'package:alcorp_app/app/core/models/data_item.model.dart';
import 'package:flutter/material.dart';

class DonutChartPainter extends CustomPainter {
  final List<DataItem> dataset;
  TextStyle textFieldTextBigStyle;
  TextStyle labelStyle;
  DonutChartPainter(this.dataset, double fullAngle);
  Paint midPaint;

  @override
  void paint(Canvas canvas, Size size) {
    final c = Offset(size.width / 2.0, size.height / 2.0);
    final radius = size.width * 0.9;
    final rect = Rect.fromCenter(center: c, width: radius, height: radius);
    final fullAngle = 360.0;
    var startAngle = 0.0;

    //draw sectors
    dataset.forEach((di) {
      final sweepAngle = di.value * fullAngle * pi / 180.0;
      drawSector(canvas, di, rect, startAngle, sweepAngle);
      drawLabels(canvas, c, radius, startAngle, sweepAngle, di.label);
      startAngle += sweepAngle;
    });

    // draw the middle
    canvas.drawCircle(c, radius * 0.3, midPaint);

    // draw title
    drawTextCentered(canvas, c, "Favourite\nMovie\nGenres",
        textFieldTextBigStyle, radius * 0.6, (Size sz) {});
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) => false;

  //#region Custom functions.
  void drawSector(Canvas canvas, DataItem di, Rect rect, double startAngle,
      double sweepAngle) {
    final paint = Paint()
      ..style = PaintingStyle.fill
      ..color = di.color;
    canvas.drawArc(rect, startAngle, sweepAngle, true, paint);
  }

  TextPainter measureText(
      String s, TextStyle style, double maxWidth, TextAlign align) {
    final span = TextSpan(
      text: s,
    );
    final tp = TextPainter(
        text: span, textAlign: align, textDirection: TextDirection.ltr);
    // ellipsis: "...");
    tp.layout(minWidth: 0, maxWidth: maxWidth);
    return tp;
  }

  Size drawTextCentered(Canvas canvas, Offset position, String text,
      TextStyle style, double maxWidth, Function(Size sz) bgCb) {
    final tp = measureText(text, style, maxWidth, TextAlign.center);
    final pos = position + Offset(-tp.width / 2.0, -tp.height / 2.0);
    bgCb(tp.size);
    tp.paint(canvas, pos);
    return tp.size;
  }

  void drawLabels(Canvas canvas, Offset c, double radius, double startAngle,
      double sweepAngle, String label) {
    final r = radius * 0.4;
    final dx = r * cos(startAngle + sweepAngle / 2.0);
    final dy = r * sin(startAngle + sweepAngle / 2.0);
    final position = c + Offset(dx, dy);
    drawTextCentered(canvas, position, label, labelStyle, 100.0, (Size sz) {
      final rect = Rect.fromCenter(
          center: position, width: sz.width + 5, height: sz.height + 5);
      final rrect = RRect.fromRectAndRadius(rect, Radius.circular(5));
      canvas.drawRRect(rrect, midPaint);
    });
  }
  //#endregion
}
