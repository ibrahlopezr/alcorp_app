import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:flutter/material.dart';

class PrimaryButton extends StatelessWidget {
  const PrimaryButton({
    Key key,
    @required this.onPressed,
    @required this.child,
    @required this.minWidth,
    @required this.height,
  }) : super(key: key);
  final GestureTapCallback onPressed;
  final Widget child;
  final double minWidth;
  final double height;

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: CustomColors.primaryColor.withOpacity(0.05),
            blurRadius: 20,
            offset: Offset(5, 5),
          ),
        ],
      ),
      child: ButtonTheme(
        // minWidth: 170.0,
        // height: 45.0,
        minWidth: minWidth == 0 ? 50.0 : minWidth,
        height: height == 0 ? 25.0 : height,
        child: RaisedButton(
          elevation: 6,
          colorBrightness: Brightness.light,
          highlightColor: CustomColors.primaryColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(45.0),
          ),
          color: CustomColors.primaryColor,
          onPressed: onPressed,
          child: child,
        ),
      ),
    );
  }
}
