import 'package:flutter/material.dart';

import 'button.widget.dart';

class OtherMetricsButton extends StatelessWidget {
  const OtherMetricsButton({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: MediaQuery.of(context).size.width * .05),
      child: PrimaryButton(
        minWidth: 10,
        height: 50,
        onPressed: () async {
          // globals.isGerente
          Navigator.of(context).pushNamed("OtherMetrics");
          // Global.tabController.animateTo(4);
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: Text(
            'Calificar otras métricas',
            style: TextStyle(
              color: Colors.white.withOpacity(0.85),
              fontFamily: 'Roboto',
            ),
          ),
        ),
      ),
    );
  }
}
