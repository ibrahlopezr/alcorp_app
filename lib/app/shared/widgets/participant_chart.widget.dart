import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:flutter/material.dart';

import 'circle_dot.widget.dart';
import 'line_progress_bar.widget.dart';

class ParticipantChart extends StatelessWidget {
  const ParticipantChart({
    Key key,
    @required this.value,
    @required this.participants,
    @required this.color,
    @required this.text,
  }) : super(key: key);

  final double value;
  final String participants;
  final MaterialColor color;
  final String text;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      crossAxisAlignment: CrossAxisAlignment.end,
      children: [
        Padding(
          padding: const EdgeInsets.only(
            left: 25.0,
            right: 25.0,
            bottom: 10.0,
          ),
          child: Row(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  CircleDotWidget(
                    color: color,
                    width: 8.0,
                    height: 8.0,
                    padding: EdgeInsets.only(right: 8.0),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Text(
                        text,
                        textAlign: TextAlign.left,
                        style: TextStyle(
                          color: Colors.black54,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ],
              ),
              Padding(
                padding: const EdgeInsets.only(left: 15.0),
                child: Text(
                  participants ?? "0",
                  textAlign: TextAlign.end,
                  style: TextStyle(
                    color: Colors.black87,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            ],
          ),
        ),
        LineProgressBar(
          color: color,
          padding: const EdgeInsets.only(
            left: 25.0,
            right: 25.0,
            bottom: 10.0,
          ),
          value: value,
        ),
      ],
    );
  }
}
