import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:flutter/material.dart';

import 'line_progress_bar.widget.dart';

class SuggestedMenuProgressLine extends StatefulWidget {
  const SuggestedMenuProgressLine({
    Key key,
    @required this.text,
    @required this.porcentaje,
    @required this.value,
  }) : super(key: key);

  final String text;
  final String porcentaje;
  final double value;

  @override
  _SuggestedMenuProgressLineState createState() =>
      _SuggestedMenuProgressLineState();
}

class _SuggestedMenuProgressLineState extends State<SuggestedMenuProgressLine> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 10.0,
        left: 25.0,
        right: 25.0,
      ),
      child: Row(
        children: [
          Expanded(
            flex: 5,
            child: LineProgressBar(
              padding: EdgeInsets.only(right: 10.0),
              value: widget.value,
              color: CustomColors.primaryColor,
            ),
          ),
          Expanded(
            flex: 2,
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                widget.text,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
          Expanded(
            flex: 2,
            child: Align(
              alignment: Alignment.topRight,
              child: Text(
                widget.porcentaje,
                style: TextStyle(
                  color: Colors.black,
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
