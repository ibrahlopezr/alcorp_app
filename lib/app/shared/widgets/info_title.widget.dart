import 'package:flutter/material.dart';

class InfoTitle extends StatefulWidget {
  const InfoTitle({
    Key key,
    @required this.title,
  }) : super(key: key);

  final String title;

  @override
  _InfoTitleState createState() => _InfoTitleState();
}

class _InfoTitleState extends State<InfoTitle> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 25.0,
        left: 25.0,
        right: 25.0,
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Text(
            widget.title ?? "",
            style: TextStyle(
              fontWeight: FontWeight.w800,
              color: Colors.black54,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Icon(
              Icons.info_outline,
              color: Colors.black54,
            ),
          )
        ],
      ),
    );
  }
}
