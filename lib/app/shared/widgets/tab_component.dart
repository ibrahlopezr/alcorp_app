import 'package:alcorp_app/app/core/models/roles.model.dart';
import 'package:alcorp_app/app/pages/configuration/modules.page.dart';
import 'package:alcorp_app/app/pages/configuration/configuration_builder.page.dart';
import 'package:alcorp_app/app/pages/configuration/default.page.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:new_version/new_version.dart';

import '../../shared/utils/globals.dart' as globals;
import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:alcorp_app/app/pages/Incident/incident.page.dart';
import 'package:alcorp_app/app/pages/Incident/incident_builder.page.dart';
import 'package:alcorp_app/app/pages/configuration/configuration.page.dart';
import 'package:alcorp_app/app/pages/home/home_page.dart';
import 'package:alcorp_app/app/pages/home/page_builder.page.dart';
import 'package:alcorp_app/app/pages/home/weekly_menu.page.dart';
import 'package:alcorp_app/app/pages/menu/menu_page.dart';
import 'package:alcorp_app/app/shared/utils/global.utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

const kMainColor = Color(0xFF573851);
TabController tabController;

// ignore: must_be_immutable
class TabBarComponent extends StatefulWidget {
  int currentPageIndex;
  TabBarComponent({Key key, this.currentPageIndex = 0}) : super(key: key);

  @override
  _TabBarComponentState createState() =>
      _TabBarComponentState(pageIndex: currentPageIndex);
}

class _TabBarComponentState extends State<TabBarComponent>
    with SingleTickerProviderStateMixin {
  int pageIndex;
  String _currentPage;
  NewVersion newVersion;
  VersionStatus status;

  //Constructor.
  _TabBarComponentState({@required this.pageIndex});

  Future statusVersion(BuildContext context) async {
    newVersion = new NewVersion(context: context);
    status = await newVersion.getVersionStatus();
    // print('Status: ${jsonEncode(status)}');
    // newVersion.showUpdateDialog(status);
    newVersion.showAlertIfNecessary();
    // return status.canUpdate;
  }

  @override
  void initState() {
    super.initState();
    statusVersion(context);
    _currentPage = pageIndex == 5
        ? 'WorkTeam'
        : pageIndex == 0
            ? globals.role == Roles.supervisor || globals.role == Roles.gerente
                ? 'WorkTeam'
                : 'home'
            : globals.role == Roles.supervisor || globals.role == Roles.gerente
                ? 'WorkTeam'
                : 'home';
    pageIndex = pageIndex == 5 ? 0 : pageIndex;

    Global.tabController = TabController(
        length: globals.role == Roles.operador ? 4 : 5,
        initialIndex: pageIndex,
        vsync: this)
      ..addListener(() {
        setState(() {
          pageIndex = Global.tabController.index;
        });
      });
  }

  @override
  void dispose() {
    super.dispose();
    Global.tabController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // CustomFiveBottomNavigationBar(context);
    if (globals.role == Roles.operador) {
      return CustomFourBottomNavigationBar(context);
    } else {
      return CustomFiveBottomNavigationBar(context);
    }
  }

  Scaffold CustomFiveBottomNavigationBar(BuildContext context) {
    var items = <BottomNavigationBarItem>[
      BottomNavigationBarItem(
        icon: SvgPicture.asset(
          'assets/icons/home.svg',
          color: pageIndex == 0
              ? CustomColors.primaryColor
              : CustomColors.greyColor,
        ),
        label: "Inicio",
      ),
      BottomNavigationBarItem(
        icon: SvgPicture.asset(
          'assets/icons/menu.svg',
          color: pageIndex == 1
              ? CustomColors.primaryColor
              : CustomColors.greyColor,
        ),
        label: "Menú",
      ),
      BottomNavigationBarItem(
        icon: SvgPicture.asset(
          'assets/icons/gear.svg',
          color: pageIndex == 2
              ? CustomColors.primaryColor
              : CustomColors.greyColor,
        ),
        label: "Ajustes",
      ),
      BottomNavigationBarItem(
        icon: SvgPicture.asset(
          'assets/icons/incidents.svg',
          color: pageIndex == 3
              ? CustomColors.primaryColor
              : CustomColors.greyColor,
        ),
        label: "Incidencias",
      ),
      BottomNavigationBarItem(
        icon: Icon(Icons.folder_shared_outlined),
        label: "Modulos",
      ),
    ];
    return Scaffold(
      bottomNavigationBar: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: pageIndex,
          onTap: onTabTapped,
          backgroundColor: Colors.white,
          selectedIconTheme: IconThemeData(
            color: CustomColors.primaryColor,
          ),
          unselectedIconTheme: IconThemeData(color: CustomColors.greyColor),
          fixedColor: CustomColors.primaryColor,
          elevation: 20,
          selectedFontSize: 12.0,
          items: items,
        ),
      ),
      body: TabBarView(
        // allowImplicitScrolling: false,
        // onPageChanged: onPageChanged,
        controller: Global.tabController,
        children: <Widget>[
          PageBuilder(
            route: _currentPage,
          ), //0
          MenuPage(), //1
          ConfigurationPage(), //2
          IncidentPageBuilder(),
          ConfigurationPageBuilder()
        ],
      ),
      //drawer: SlideDrawer(),
    );
  }

  Scaffold CustomFourBottomNavigationBar(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: SizedBox(
        width: MediaQuery.of(context).size.width,
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          currentIndex: pageIndex,
          onTap: onTabTapped,
          backgroundColor: Colors.white,
          selectedIconTheme: IconThemeData(color: CustomColors.primaryColor),
          unselectedIconTheme: IconThemeData(color: CustomColors.greyColor),
          fixedColor: CustomColors.primaryColor,
          elevation: 20,
          selectedFontSize: 12.0,
          items: <BottomNavigationBarItem>[
            BottomNavigationBarItem(
              icon: SvgPicture.asset(
                'assets/icons/home.svg',
                color: pageIndex == 0
                    ? CustomColors.primaryColor
                    : CustomColors.greyColor,
              ),
              label: "Inicio",
            ),
            BottomNavigationBarItem(
              icon: SvgPicture.asset(
                'assets/icons/menu.svg',
                color: pageIndex == 1
                    ? CustomColors.primaryColor
                    : CustomColors.greyColor,
              ),
              label: "Menú",
            ),
            BottomNavigationBarItem(
              icon: SvgPicture.asset(
                'assets/icons/gear.svg',
                color: pageIndex == 2
                    ? CustomColors.primaryColor
                    : CustomColors.greyColor,
              ),
              label: "Ajustes",
            ),
            BottomNavigationBarItem(
              icon: SvgPicture.asset(
                'assets/icons/incidents.svg',
                color: pageIndex == 3
                    ? CustomColors.primaryColor
                    : CustomColors.greyColor,
              ),
              label: "Incidencias",
            )
          ],
        ),
      ),
      body: TabBarView(
        // allowImplicitScrolling: false,
        // onPageChanged: onPageChanged,
        controller: Global.tabController,
        children: <Widget>[
          PageBuilder(
            route: _currentPage,
          ), //0
          MenuPage(), //1
          ConfigurationPage(), //2
          IncidentPageBuilder(),
          // ConfigurationPageBuilder()
        ],
      ),
      //drawer: SlideDrawer(),
    );
  }

  void onPageChanged(int page) {
    setState(() {
      this.pageIndex = page;
    });
  }

  Future<void> onTabTapped(int index) async {
    onPageChanged(index);

    Global.tabController.animateTo(
      index,
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeIn,
    );
    String tabpageName = '';
    switch (index) {
      case 0:
        tabpageName = 'Inicio';
        break;
      case 1:
        tabpageName = 'Menú';
        break;
      case 2:
        tabpageName = 'Ajustes';
        break;
      case 2:
        tabpageName = 'Incidencias';
        break;
      default:
    }

    await FirebaseAnalytics.instance.logSelectContent(
      contentType: 'onPageChanged',
      itemId: tabpageName,
    );
  }

  // Mostrar indicador
  Widget showIndicator(bool show) {
    if (show) {
      return Column(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              width: double.infinity,
              height: 2,
              color: CustomColors.primaryColor,
              margin: const EdgeInsets.only(right: 4),
            ),
          ),
          Expanded(
            flex: 4,
            child: SvgPicture.asset(
              'assets/icons/incidents.svg',
              color: CustomColors.primaryColor,
            ),
          )
        ],
      );
    } else {
      return SvgPicture.asset(
        'assets/icons/incidents.svg',
        color: CustomColors.greyColor,
      );
    }
  }

  void onAddButtonTapped(int index) {
    pageIndex = index;
    // use this to animate to the page
    Global.tabController.animateTo(
      index,
      duration: const Duration(milliseconds: 500),
      curve: Curves.easeIn,
    );

    // or this to jump to it without animating
    // _tabController.jumpTo(index);
  }
}

class CustomAppBar extends StatefulWidget implements PreferredSizeWidget {
  CustomAppBar({Key key, this.leading})
      : preferredSize = Size.fromHeight(kToolbarHeight),
        super(key: key);
  final Widget leading;

  @override
  final Size preferredSize; // default is 56.0

  @override
  _CustomAppBarState createState() => _CustomAppBarState(leading: leading);
}

class _CustomAppBarState extends State<CustomAppBar> {
  final Widget leading;
  _CustomAppBarState({this.leading});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      leading: leading,
      iconTheme: IconThemeData(
        color: Colors.black, //change your color here
      ),
      centerTitle: true,
      title: Padding(
        padding: const EdgeInsets.symmetric(vertical: 15.0),
        child: SizedBox(
          height: 45.0,
          child: Image.asset('assets/images/logo.png', fit: BoxFit.fitWidth),
        ),
      ),
      backgroundColor: Colors.white,
      elevation: 3,
    );
  }
}
