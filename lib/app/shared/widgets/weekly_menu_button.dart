import 'package:flutter/material.dart';

import 'button.widget.dart';

class DayMenuButton extends StatelessWidget {
  const DayMenuButton({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: MediaQuery.of(context).size.width * .04),
      child: PrimaryButton(
        minWidth: 10,
        height: 50,
        onPressed: () async {
          Navigator.of(context).pushNamed("home");
          // Global.tabController.animateTo(4);
        },
        child: Padding(
          padding: const EdgeInsets.symmetric(vertical: 10.0),
          child: Text(
            'Calificar el platillo del día',
            style: TextStyle(
              color: Colors.white.withOpacity(0.85),
              fontFamily: 'Roboto',
            ),
          ),
        ),
      ),
    );
  }
}
