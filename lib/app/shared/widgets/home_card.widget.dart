import 'dart:math';
import 'dart:ui';
import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:alcorp_app/app/core/models/calificacion_platillo.model.dart';
import 'package:alcorp_app/app/core/models/roles.model.dart';
import 'package:alcorp_app/app/core/services/menu.service.dart';
import 'package:alcorp_app/app/pages/home/home_page.dart';

import '../../shared/utils/globals.dart' as globals;

import 'package:alcorp_app/app/core/models/platillo.model.dart';
import 'package:alcorp_app/app/core/services/auth.service.dart';
import 'package:flutter/material.dart';

import 'other_metrics_button.dart';

class HomeCard extends StatefulWidget {
  const HomeCard({Key key}) : super(key: key);
  @override
  State<HomeCard> createState() => _HomeCardState();
}

class _HomeCardState extends State<HomeCard> {
  PageController _pageController;
  int _currentPage = 0;
  Future<List<Platillo>> _getPlatilloFuture;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    this._getPlatilloFuture = this.getPlatillo();
    _pageController =
        PageController(initialPage: _currentPage, viewportFraction: 0.85);
  }

  @override
  void dispose() {
    super.dispose();
    _pageController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder<List<Platillo>>(
      key: _scaffoldKey,
      future: this._getPlatilloFuture,
      // initialData: true,
      builder: (_, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          if (snapshot.data != null && snapshot.data.length > 0) {
            return _buildContent(context, snapshot.data);
          } else {
            return _doneData(context);
          }
        } else if (snapshot.connectionState == ConnectionState.waiting &&
            !snapshot.hasError) {
          return loading(context);
        } else {
          return Center(
            child: Text(
              'Ocurrio un error al obtener la informacion\nIntente de nuevo mas tarde.',
              style: TextStyle(fontSize: 16.0),
            ),
          );
        }
      },
    );
  }

  Container _doneData(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Stack(
        alignment: Alignment.center,
        children: [
          // Background Calificacion de metricas
          Column(
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/images/bckg-2.png"),
                      //image: SvgPicture.asset("assets/images/bckg-login.svg"),
                      fit: BoxFit.fitHeight,
                      colorFilter: ColorFilter.mode(
                        Colors.black.withOpacity(0.30),
                        BlendMode.darken,
                      ),
                    ),
                  ),
                  //child: Image.asset('assets/images/login-bg.jpg', ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width * .85,
                height: MediaQuery.of(context).size.height * .40,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        child: Opacity(
                          opacity: 0.9,
                          child: Card(
                            elevation: 2,
                            borderOnForeground: true,
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.white, width: 1),
                              borderRadius: BorderRadius.circular(25),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  FittedBox(
                                    fit: BoxFit.fitHeight,
                                    child: Text(
                                      'Ya calificaste los platillos del día\nVuelve mañana',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 50,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black87,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Visibility(
                visible:
                    globals.role == Roles.gerente || globals.role == Roles.rh,
                child: OtherMetricsButton(),
              ),
              WeeklyMenuButton(),
            ],
          )
        ],
      ),
    );
  }

  Widget loading(BuildContext context) {
    double _sigmaX = 0.0; // from 0-10
    double _sigmaY = 0.0; // from 0-10
    double _opacity = 0.1; // from 0-1.0
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Expanded(
              flex: 1,
              child: Container(
                margin: const EdgeInsets.symmetric(horizontal: 15.0),
                child: Stack(
                  children: [
                    Opacity(
                      opacity: 0.6,
                      child: Card(
                        elevation: 2,
                        borderOnForeground: true,
                        shape: RoundedRectangleBorder(
                          side: BorderSide(color: Colors.white, width: 1),
                          borderRadius: BorderRadius.circular(20),
                        ),
                        child: BackdropFilter(
                          filter: ImageFilter.blur(
                            sigmaX: _sigmaX,
                            sigmaY: _sigmaY,
                          ),
                          child: Container(
                            width: MediaQuery.of(context).size.width * .90,
                            height: MediaQuery.of(context).size.width * .90,
                            color: Colors.grey[100].withOpacity(_opacity),
                            child: Center(
                              child: Container(
                                width: 50,
                                height: 50,
                                child: CircularProgressIndicator(
                                  color: CustomColors.primaryColor,
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ],
    );
  }

  Column _buildContent(BuildContext context, List<Platillo> platillos) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        SizedBox(
          width: MediaQuery.of(context).size.width * 1,
          height: MediaQuery.of(context).size.width * 1,
          child: PageView.builder(
            itemCount: platillos.length,
            physics: const ClampingScrollPhysics(),
            controller: _pageController,
            itemBuilder: (contex, i) {
              return _buildHomeCard(platillos[i], context);
            },
          ),
        ),
      ],
    );
  }

// WIDGETS.
  Widget _buildHomeView(BuildContext context, List<Platillo> platillos, int i) {
    return AnimatedBuilder(
      animation: _pageController,
      builder: (context, child) {
        double value = 0.0;
        if (_pageController.position.haveDimensions) {
          value = i.toDouble() - (_pageController.page ?? 0);
          value = (value * 0.04).clamp(-1, 1);
        }

        return Transform.rotate(
          angle: pi * value,
          child: _buildHomeCard(platillos[i], context),
        );
      },
    );
  }

  Widget _buildHomeCard(Platillo platillo, BuildContext context) {
    double _sigmaX = 0.0; // from 0-10
    double _sigmaY = 0.0; // from 0-10
    double _opacity = 0.1; // from 0-1.0
    return Row(
      children: [
        Expanded(
          flex: 1,
          child: Stack(
            children: [
              Opacity(
                opacity: 0.6,
                child: Card(
                  elevation: 2,
                  borderOnForeground: true,
                  shape: RoundedRectangleBorder(
                    side: BorderSide(color: Colors.white, width: 1),
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: _sigmaX, sigmaY: _sigmaY),
                    child: Container(
                      color: Colors.grey[100].withOpacity(_opacity),
                    ),
                  ),
                ),
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Expanded(
                    flex: 1,
                    child: Text(
                      '¿Cómo calificas la comida del dia de hoy?',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.bold,
                        color: Colors.black87,
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: FittedBox(
                      fit: BoxFit.fitWidth,
                      child: Text(
                        platillo.titulo != null ? platillo.titulo : "Comida",
                        style: TextStyle(
                          fontSize: 32,
                          fontWeight: FontWeight.bold,
                          color: Colors.black87,
                        ),
                      ),
                    ),
                  ),
                  Expanded(
                    flex: 3,
                    child: Row(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () async {
                                final idEmpleado =
                                    await AuthService.getCurrentUserId();
                                const value = "mala";
                                await realizarVotacion(
                                    value, platillo, idEmpleado);
                              },
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width * .3,
                                    child: Image.asset(
                                      'assets/icons/emoji_4.png',
                                      fit: BoxFit.fitHeight,
                                    ),
                                  ),
                                  Text(
                                    'Mala',
                                    style: TextStyle(
                                      color: CustomColors.greyColor,
                                      fontSize: 12.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () async {
                                final idEmpleado =
                                    await AuthService.getCurrentUserId();
                                const value = "regular";
                                await realizarVotacion(
                                    value, platillo, idEmpleado);
                              },
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width * .3,
                                    child: Image.asset(
                                      'assets/icons/emoji_5.png',
                                      fit: BoxFit.fitHeight,
                                    ),
                                  ),
                                  Text(
                                    'Regular',
                                    style: TextStyle(
                                      color: CustomColors.greyColor,
                                      fontSize: 12.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Expanded(
                            flex: 1,
                            child: GestureDetector(
                              onTap: () async {
                                final idEmpleado =
                                    await AuthService.getCurrentUserId();
                                const value = "buena";
                                await realizarVotacion(
                                    value, platillo, idEmpleado);
                              },
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  SizedBox(
                                    width:
                                        MediaQuery.of(context).size.width * .3,
                                    child: Image.asset(
                                      'assets/icons/emoji_6.png',
                                      fit: BoxFit.fitHeight,
                                    ),
                                  ),
                                  Text(
                                    'Buena',
                                    style: TextStyle(
                                      color: CustomColors.greyColor,
                                      fontSize: 12.0,
                                      fontWeight: FontWeight.bold,
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ],
          ),
        ),
      ],
    );
  }

  //Functions
  Future<List<Platillo>> getPlatillo() async {
    final data = await MenuService.getCurrentPlatillosMenu();
    return data;
  }

  Future<void> realizarVotacion(
    String value,
    Platillo platillo,
    int idEmpleado,
  ) async {
    var objCalificacion = new CalificacionPlatillo(
      id: 0,
      calificacion_cualitativa: value,
      calificacion_cuantitativa: 0,
      id_platillo: platillo.id,
      id_empleado: idEmpleado,
      usuario_creacion: idEmpleado,
      usuario_modificacion: idEmpleado,
      fecha_creacion: new DateTime.now(),
      fecha_modificacion: new DateTime.now(),
    );
    final result = await MenuService.addCalPlatilloMenu(
      objCalificacion,
    );
    setState(
      () {
        this._getPlatilloFuture = this.getPlatillo();
        if (globals.isGerente) {
          Navigator.of(context).pushNamedAndRemoveUntil(
            "WorkTeam",
            (route) => false,
          );
        }
      },
    );
  }
}
