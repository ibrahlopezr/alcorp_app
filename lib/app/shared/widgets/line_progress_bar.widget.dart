import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:flutter/material.dart';

class LineProgressBar extends StatelessWidget {
  const LineProgressBar({
    Key key,
    @required this.padding,
    @required this.value,
    @required this.color,
  }) : super(key: key);

  final double value;
  final EdgeInsets padding;
  final MaterialColor color;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding ?? const EdgeInsets.all(0),
      child: ClipRRect(
        borderRadius: BorderRadius.all(Radius.circular(10)),
        child: LinearProgressIndicator(
          valueColor: AlwaysStoppedAnimation<Color>(
            color,
          ),
          minHeight: 10,
          value: value,
          backgroundColor: Colors.grey[200],
          color: color,
        ),
      ),
    );
  }
}
