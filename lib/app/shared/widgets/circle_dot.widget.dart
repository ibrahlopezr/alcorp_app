import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:flutter/material.dart';

class CircleDotWidget extends StatelessWidget {
  const CircleDotWidget({
    Key key,
    @required this.width,
    @required this.height,
    @required this.padding,
    @required this.color,
  }) : super(key: key);

  final double width;
  final double height;
  final EdgeInsets padding;
  final MaterialColor color;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Container(
        width: width,
        height: height,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
          color: color ?? CustomColors.primaryColor,
        ),
      ),
    );
  }
}
