import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:flutter/material.dart';

class CircularProgressBar extends StatelessWidget {
  const CircularProgressBar({
    Key key,
    @required this.padding,
    @required this.width,
    @required this.height,
    @required this.value,
  }) : super(key: key);

  final EdgeInsets padding;
  final double width;
  final double height;
  final double value;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: padding,
      child: Container(
        width: width,
        height: height,
        child: CircularProgressIndicator(
          strokeWidth: 20.0,
          valueColor: AlwaysStoppedAnimation<Color>(
            CustomColors.primaryColor,
          ),
          value: value,
          backgroundColor: CustomColors.primaryColorDark,
          color: CustomColors.primaryColor,
        ),
      ),
    );
  }
}
