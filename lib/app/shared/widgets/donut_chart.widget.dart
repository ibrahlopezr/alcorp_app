import 'dart:async';

import 'package:alcorp_app/app/core/models/data_item.model.dart';
import 'package:alcorp_app/app/shared/utils/donut_chart.painter.dart';
import 'package:flutter/material.dart';

class DonutChartWidget extends StatefulWidget {
  final List<DataItem> dataset;
  DonutChartWidget(this.dataset);
  @override
  State<DonutChartWidget> createState() => _DonutChartWidgetState();
}

class _DonutChartWidgetState extends State<DonutChartWidget> {
  Timer timer;
  double fullAngle = 0.0;
  double secondsToComplete = 3.0;

  @override
  void initState() {
    super.initState();
    timer = Timer.periodic(Duration(milliseconds: 1000 ~/ 60), (timer) {
      setState(() {
        fullAngle += 360.0 / (secondsToComplete * 1000 ~/ 60);
        if (fullAngle >= 360.0) {
          fullAngle = 360.0;
          timer.cancel();
        }
      });
    });
  }

  @override
  void dispose() {
    timer.cancel();
    super.dispose();
  }

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    throw UnimplementedError();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * .5,
      height: MediaQuery.of(context).size.height * .5,
      child: CustomPaint(
        painter: DonutChartPainter(
          widget.dataset,
          fullAngle,
        ),
      ),
    );
  }
}
