import 'package:alcorp_app/app/controller/news.controller.dart';
import 'package:alcorp_app/app/shared/widgets/tab_component.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class NewsPage extends GetView<NewsController> {
  const NewsPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: Center(
        child: Text('News Page Widget'),
      ),
    );
  }
}
