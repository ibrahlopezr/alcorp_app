import 'package:flutter/material.dart';

class CustomColors {
  static const MaterialColor primaryColor = MaterialColor(
    0xFFFF3A3A,
    <int, Color>{
      50: Color(0xFFFF3A3A0D),
      100: Color(0xFFFF3A3A1A),
      200: Color(0xFFFF3A3A33),
      300: Color(0xFFFF3A3A4D),
      400: Color(0xFFFF3A3A66),
      500: Color(0xFFFF3A3A80),
      600: Color(0xFFFF3A3A99),
      700: Color(0xD3FF3A3A),
      800: Color(0xFFFF3A3ACC),
      900: Color(0xFFFF3A3ABB),
    },
  );
  static const MaterialColor primaryColorDark = MaterialColor(
    // 0xFFBE2E2E,
    0xFF984747,
    <int, Color>{
      50: Color(0xFF9847470D),
      100: Color(0xFF9847471A),
      200: Color(0xFF98474733),
      300: Color(0xFF9847474D),
      400: Color(0xFF98474766),
      500: Color(0xFF98474780),
      600: Color(0xFF98474799),
      700: Color(0xFF984747),
      800: Color(0xFF984747CC),
      900: Color(0xFF984747BB),
    },
  );

  static const MaterialColor greyColor = MaterialColor(
    0xFF7A7A7A,
    <int, Color>{
      50: Color(0xFF7A7A7A0D),
      100: Color(0xFF7A7A7A1A),
      200: Color(0xFF7A7A7A33),
      300: Color(0xFF7A7A7A4D),
      400: Color(0xFF7A7A7A66),
      500: Color(0xFF7A7A7A80),
      600: Color(0xFF7A7A7A99),
      700: Color(0xFF7A7A7ADD),
      800: Color(0xFF7A7A7ACC),
      900: Color(0xFF7A7A7ABB),
    },
  );
}
