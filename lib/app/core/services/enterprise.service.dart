import 'package:alcorp_app/app/core/models/enterprise.model.dart';

import '../../shared/utils/globals.dart' as globals;
import 'dart:async' show Future;
import 'package:alcorp_app/app/core/models/dia.model.dart';
import 'package:alcorp_app/app/core/models/login_response.model.dart';
import 'package:alcorp_app/app/core/models/response.model.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as httpClient;
import 'package:jwt_decode/jwt_decode.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'base_url.dart';

class EnterpriseService {
  static Future<List<Enterprise>> getEnterprise(String appName) async {
    List<Enterprise> enterprises = null;
    try {
      var uri = enterpriseUri + "Enterprises/getEnterprise";
      print('URL: $uri');
      final response = await httpClient.post(
        Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(
          <String, dynamic>{
            "AppName": appName,
          },
        ),
      );
      if (response.statusCode == 200) {
        var jsonData = json.decode(response.body) as List;
        if (!jsonData.isEmpty) {
          enterprises = jsonData
              .map<Enterprise>((json) => Enterprise.fromJson(json))
              .toList();
        }
      }
      return enterprises;
    } catch (e) {
      return enterprises;
    }
  }
}
