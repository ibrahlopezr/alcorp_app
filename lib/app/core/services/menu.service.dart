import 'dart:async' show Future;
import 'package:alcorp_app/app/core/models/calificacion_platillo.model.dart';
import 'package:alcorp_app/app/core/models/dia.model.dart';
import 'package:alcorp_app/app/core/models/platillo.model.dart';
import 'package:alcorp_app/app/core/models/sugerencia.model.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as httpClient;
import 'base_url.dart';

class MenuService {
  static Future<List<Dia>> getPlatillosByGroupDay() async {
    // try {
    //   var jsonText = await rootBundle.loadString('assets/data/platillos.json');
    //   var data =
    //       (json.decode(jsonText) as List).map((g) => Dia.fromJson(g)).toList();
    //   return data;
    // } catch (e) {
    //   print('Ocurrio un error al obtener los platillos: $e');
    //   throw e;
    // }
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<Dia> dias;
    try {
      var uri = await endpoint() + "platillos/GetPlatilloSemana";
      var token = prefs.getString('token');
      // var jsonText = await rootBundle.loadString('assets/data/platillos.json');
      final response = await httpClient.get(
        Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token',
        },
      );

      if (response.statusCode == 200) {
        dias = jsonDecode(response.body)
            .map<Dia>((json) => Dia.fromJson(json))
            .toList();
        // dia = Dia.fromJson.fromJson(jsonDecode());
      }

      return dias;
    } catch (e) {
      print('Ocurrio un error al obtener los dias: $e');
      throw e;
    }
  }

  static Future<List<Platillo>> getCurrentPlatillosMenu() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<Platillo> platillo;
    try {
      var uri = await endpoint() + "platillos/GetCurrentPlatillosMenu";
      var token = prefs.getString('token');
      // var jsonText = await rootBundle.loadString('assets/data/platillos.json');
      final response = await httpClient.get(
        Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token',
        },
      );
      if (response.statusCode == 200) {
        platillo = (jsonDecode(response.body) as List)
            .map((json) => Platillo.fromJson(json))
            .toList();
        // platillo = Platillo.fromJson(jsonDecode(response.body));
      }

      return platillo;
    } catch (e) {
      print('Ocurrio un error al obtener el platillo del dia: $e');
      throw e;
    }
  }

  static Future<int> addCalPlatilloMenu(
    CalificacionPlatillo calPlatillo,
  ) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      var uri = await endpoint() + "platillos/AddCalPlatilloMenu";
      var token = prefs.getString('token');
      int result = 0;
      final jsonData = calPlatillo.toMap();
      final body = jsonEncode(
        jsonData,
      );
      // var jsonText = await rootBundle.loadString('assets/data/platillos.json');
      final response = await httpClient.post(
        Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token',
        },
        body: body,
      );
      print("body: ${response.body}");
      if (response.statusCode == 200) {
        result = int.parse(response.body);
      }

      return result;
    } catch (e) {
      print('Ocurrio un error al calificar el platillo del dia: $e');
      throw e;
    }
  }

  static Future<int> addSugerencia(Sugerencia sugerencia) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      var uri = await endpoint() + "platillos/AddSugerencia";
      var token = prefs.getString('token');
      int result = 0;
      final jsonData = sugerencia.toMap();
      final body = jsonEncode(
        jsonData,
      );
      // var jsonText = await rootBundle.loadString('assets/data/platillos.json');
      final response = await httpClient.post(
        Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token',
        },
        body: body,
      );
      print("body: ${response.body}");
      if (response.statusCode == 200) {
        result = int.parse(response.body);
      }

      return result;
    } catch (e) {
      print('Ocurrio un error al calificar el platillo del dia: $e');
      throw e;
    }
  }
}
