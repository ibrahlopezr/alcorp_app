import 'dart:async' show Future;
import 'package:alcorp_app/app/core/models/votacion.model.dart';
import 'package:alcorp_app/app/core/services/auth.service.dart';
import 'package:alcorp_app/app/pages/auth/login_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as httpClient;
import 'package:shared_preferences/shared_preferences.dart';
import 'base_url.dart';

class VotacionService {
  static Future<List<Votacion>> getVotacionesSemanal(int diaNumber) async {
    try {
      var jsonText = await rootBundle.loadString('assets/data/votaciones.json');
      var data = (json.decode(jsonText) as List)
          .map((json) => Votacion.fromJson(json))
          .toList();
      return data;
    } catch (e) {
      print('Ocurrio un error al obtener las votaciones: $e');
      throw e;
    }
  }

  static Future<List<Votacion>> getPlatillosParaVotacion({
    String dia = "-1",
    @required BuildContext context,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<Votacion> incidencias;
    try {
      var uri =
          await endpoint() + "platillos/GetPlatillosParaVotacion?dia=$dia";
      var token = prefs.getString('token');
      // var jsonText = await rootBundle.loadString('assets/data/platillos.json');
      final response = await httpClient.get(
        Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token',
        },
      );
      if (response.statusCode == 200) {
        var jsonData = json.decode(response.body) as List;
        incidencias =
            jsonData.map<Votacion>((json) => Votacion.fromJson(json)).toList();
      } else if (response.statusCode == 401) {
        await AuthService.logOut(context);
      }

      return incidencias;
    } catch (e) {
      print('Ocurrio un error al obtener los platillos para votar: $e');
      throw e;
    }
  }

  static Future<int> addVotoPlatillo(int id_platillo) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      var uri = await endpoint() +
          "platillos/AddVotoPlatillo?idPlatillo=$id_platillo";
      var token = prefs.getString('token');
      int result = 0;
      final response = await httpClient.post(
        Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token',
        },
      );
      print("body: ${response.body}");
      if (response.statusCode == 200) {
        result = int.parse(response.body);
      }

      return result;
    } catch (e) {
      print('Ocurrio un error votar por un platillo: $e');
      throw e;
    }
  }

  static Future<Map<String, dynamic>> getDashboard() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      var uri = await endpoint() + "platillos/GetDashboard";
      var token = prefs.getString('token');
      Map<String, dynamic> result;
      final response = await httpClient.get(
        Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token',
        },
      );
      // print("body: ${response.body}");
      if (response.statusCode == 200) {
        Map<String, dynamic> data = jsonDecode(response.body);
        print('data: ${data["data"]}');
        result = data['data'];
      }

      return result;
    } catch (e) {
      print('Ocurrio un error al obtener el tablero: $e');
      throw e;
    }
  }
}
