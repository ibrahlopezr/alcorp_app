import 'dart:async' show Future;
import 'package:alcorp_app/app/core/models/calificacion_grupo.model.dart';
import 'package:alcorp_app/app/core/models/calificacion_platillo.model.dart';
import 'package:alcorp_app/app/core/models/grupo.model.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as httpClient;
import 'base_url.dart';

class GrupoService {
  static Future<Grupo> getCurrentGrupo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    Grupo grupo;
    try {
      var uri = await endpoint() + "Grupos/GetCurrentGrupo";
      print('URL: $uri');
      var token = prefs.getString('token');
      // var jsonText = await rootBundle.loadString('assets/data/Grupos.json');
      final response = await httpClient.get(
        Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token',
        },
      );
      if (response.statusCode == 200) {
        grupo = Grupo.fromJson(jsonDecode(response.body));
      }

      return grupo;
    } catch (e) {
      print('Ocurrio un error al obtener el grupo del dia: $e');
      throw e;
    }
  }

  static Future<int> addCalGrupo(CalificacionGrupo calGrupo) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      var uri = await endpoint() + "Grupos/AddCalGrupo";
      var token = prefs.getString('token');
      int result = 0;
      final jsonData = calGrupo.toMap();
      final body = jsonEncode(
        jsonData,
      );
      // var jsonText = await rootBundle.loadString('assets/data/platillos.json');
      final response = await httpClient.post(
        Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token',
        },
        body: body,
      );
      print("body: ${response.body}");
      if (response.statusCode == 200) {
        result = int.parse(response.body);
      }

      return result;
    } catch (e) {
      print('Ocurrio un error al calificar el grupo del dia: $e');
      throw e;
    }
  }
}
