import 'package:alcorp_app/app/pages/auth/login_page.dart';
import 'package:flutter/material.dart';

import '../../shared/utils/globals.dart' as globals;
import 'dart:async' show Future;
import 'package:alcorp_app/app/core/models/dia.model.dart';
import 'package:alcorp_app/app/core/models/login_response.model.dart';
import 'package:alcorp_app/app/core/models/response.model.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'dart:convert';
import 'package:flutter/services.dart';
import 'package:http/http.dart' as httpClient;
import 'package:jwt_decode/jwt_decode.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'base_url.dart';

class AuthService {
  static Future<bool> login(String username, String password) async {
    try {
      var uri = await endpoint() + "auth/login";
      print('URL: $uri');
      final response = await httpClient.post(
        Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(
          <String, dynamic>{
            "username": username,
            "password": password,
          },
        ),
      );
      if (response.statusCode == 200) {
        SharedPreferences prefs = await SharedPreferences.getInstance();
        var objLoginResponse =
            LoginResponse.fromJson(jsonDecode(response.body));
        Map<String, dynamic> payload = Jwt.parseJwt(objLoginResponse.token);
        print("payload: ${jsonEncode(payload)}");
        prefs.setString('token', objLoginResponse.token);
        var jsonRoutes = jsonEncode(objLoginResponse.routes);
        prefs.setString('routes', jsonRoutes);
        globals.isLoggedIn = true;
        globals.isGerente = payload['gerente'] == "1";
        globals.voteGroup = payload['voteGroup'] == "1";

        return true;
      } else {
        globals.isLoggedIn = false;
        return false;
      }
    } catch (e) {
      print('Ocurrio un error al inciar sesion: $e');
      throw e;
    }
  }

  static Future<bool> register(
      String numeroEmpleado,
      String contrasena,
      String email,
      String numeroTelefono,
      String nombre,
      String apellidoPaterno,
      String apellidoMaterno) async {
    try {
      var uri = await endpoint() + "auth/register";
      dynamic objResponse = null;
      final response = await httpClient.post(
        Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(
          <String, dynamic>{
            "NumeroEmpleado": numeroEmpleado,
            "Contrasena": contrasena,
            "ConfirmarContrasena": contrasena,
            "CorreoElectronico": email,
            "NumeroTelefono": numeroTelefono,
            "Nombre": nombre,
            "ApellidoPaterno": apellidoPaterno,
            "ApellidoMaterno": apellidoMaterno
          },
        ),
      );

      if (response.statusCode == 200) {
        // var jsonData = json.decode(response.body) as List;
        // if (!jsonData.isEmpty) {
        //   objResponse = jsonData.map<Response>((json) => json).first;
        // }
        var idRegister = int.parse(response.body);

        return idRegister > 0;
      } else {
        var jsonData = json.decode(response.body) as dynamic;
        if (!jsonData.isEmpty) {
          String errorMsg = jsonData['errorMsg'].toString();
          if (errorMsg != null) {
            throw errorMsg;
          }
        }
        globals.isLoggedIn = false;
        return false;
      }
    } catch (e) {
      print('Ocurrio un error al registrar un usuario: $e');
      throw e;
    }
  }

  static Future<int> getCurrentUserId() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      final token = prefs.getString('token');
      if (token != null) {
        Map<String, dynamic> payload = Jwt.parseJwt(token);
        return int.parse(payload['id']);
      }
      return 0;
    } catch (e) {
      throw e;
    }
  }

  static Future<bool> isGerente() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      final token = prefs.getString('token');
      if (token != null) {
        Map<String, dynamic> payload = Jwt.parseJwt(token);
        return payload['voteGroup'] == "1";
      }
      throw Exception("No se encontro un token");
    } catch (e) {
      throw e;
    }
  }

  static Future<String> getCurrentUserName() async {
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      final token = prefs.getString('token');
      if (token != null) {
        Map<String, dynamic> payload = Jwt.parseJwt(token);
        return payload['name'].toString();
      }
      return '';
    } catch (e) {
      throw e;
    }
  }

  static Future<int> forgotPassword(String email) async {
    try {
      int result = 0;
      var uri = await endpoint() + "auth/ForgotPassword";
      print('URL: $uri');

      final response = await httpClient.post(
        Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(
          <String, dynamic>{
            "Email": email,
          },
        ),
      );

      if (response.statusCode == 200) {
        result = int.parse(response.body);
        return result;
      } else {
        return result;
      }
    } catch (e) {
      print('Ocurrio un error al enviar el codigo de recuperacion: $e');
      throw e;
    }
  }

  static Future<int> validarCodigo(String correo, String codigo) async {
    try {
      int result = 0;
      var uri = await endpoint() + "auth/ValidarCodigo";
      print('URL: $uri');

      final response = await httpClient.post(
        Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(
          <String, dynamic>{
            "Correo": correo,
            "Codigo": codigo,
          },
        ),
      );

      if (response.statusCode == 200) {
        result = int.parse(response.body);
        return result;
      } else {
        return result;
      }
    } catch (e) {
      print('Ocurrio un error al enviar el codigo de recuperacion: $e');
      throw e;
    }
  }

  static Future<int> changePassword(String correo, String contrasena) async {
    try {
      int result = 0;
      var uri = await endpoint() + "auth/ChangePassword";
      print('URL: $uri');

      final response = await httpClient.post(
        Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(
          <String, dynamic>{
            "Correo": correo,
            "Contrasena": contrasena,
          },
        ),
      );

      if (response.statusCode == 200) {
        result = int.parse(response.body);
        return result;
      } else {
        return result;
      }
    } catch (e) {
      print('Ocurrio un error al ChangePassword: $e');
      throw e;
    }
  }

  static Future<void> logOut(BuildContext context) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    prefs.setString('token', '');
    final result = await Navigator.pushAndRemoveUntil(
      context,
      MaterialPageRoute(
        builder: (context) => LoginPage(),
      ),
      (Route route) => false,
    ) as bool;
  }
}
