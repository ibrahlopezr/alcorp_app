import 'dart:async' show Future;

import 'dart:convert' show json, jsonDecode, jsonEncode;
import 'package:alcorp_app/app/core/models/form_fields.model.dart';
import 'package:alcorp_app/app/core/models/response.model.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as httpClient;

import 'package:shared_preferences/shared_preferences.dart'
    show SharedPreferences;

import 'base_url.dart' show endpoint;
import '../../shared/utils/globals.dart' as globals;

class FormService {
  static Future<dynamic> getDynamicForm() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String data;
    var uri = await endpoint() + "Form/GetDynamicForm";
    var token = prefs.getString('token') ?? '';
    print('URL: $uri');
    final response = await httpClient.get(
      Uri.parse(uri),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Authorization': 'Bearer $token',
      },
    );
    if (response.statusCode == 200) {
      var jsonData = json.decode(response.body) as List;
      data = response.body;
      return data;
    } else if (response.statusCode == 204) {
      // var jsonData = json.decode(response.body) as List;
      data = response.body;
      return data;
    } else {
      throw Exception(
        "Ocurrio un error al obtener el formulario: ${response.body.toString()}",
      );
    }
  }

  static Future<bool> addResponseDynamicForm(
    List<FormFields> data,
  ) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      var uri = await endpoint() + "Form/AddResponseDynamicForm";
      var token = prefs.getString('token') ?? '';
      // var aaaaa = jsonDecode(data.toString()) as List;
      var listMap = data.map((obj) => obj.toMap()).toList();
      final body = jsonEncode(listMap);
      // String body = '';
      // listMap.forEach((element) {});
      // for (var i = 0; i < listMap.length; i++) {
      //   if (i == listMap.length) {
      //     body = body + listMap[i];
      //   } else {
      //     body = body + listMap[i] + ",";
      //   }
      // }

      final response = await httpClient.post(
        Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token',
        },
        body: body,
      );
      if (response.statusCode == 200) {
        var idSave = int.parse(response.body);
        return idSave > 0;
      } else {
        // globals.isLoggedIn = false;
        return false;
      }
    } catch (e) {
      print('Ocurrio un error al responder el formulario: $e');
      throw e;
    }
  }
}
