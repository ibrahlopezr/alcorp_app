// PUBLIC SERVER
// const String endpoint = "http://164.68.109.183:88/api/";
import 'package:shared_preferences/shared_preferences.dart';

Future<String> endpoint() async {
  var prefs = await SharedPreferences.getInstance();
  final uri = prefs.get('apiUrl');
  return uri;
}

// const String endpoint = "https://192.168.0.11:45456/api/";
const String enterpriseUri = 'http://164.68.109.183:82/api/';
