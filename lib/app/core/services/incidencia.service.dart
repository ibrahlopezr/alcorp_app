import 'dart:async' show Future;
import 'package:alcorp_app/app/core/models/incidencia.modelo.dart';
import 'package:alcorp_app/app/core/models/respuesta.model.dart';
import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as httpClient;
import 'base_url.dart';

class IncidenciaService {
  static Future<List<Incidencia>> getIncidencias() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<Incidencia> incidencias;
    try {
      var uri = await endpoint() + "incidencias/GetIncidencias";
      var token = prefs.getString('token');
      // var jsonText = await rootBundle.loadString('assets/data/platillos.json');
      final response = await httpClient.get(
        Uri.parse(uri),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
          'Authorization': 'Bearer $token',
        },
      );
      if (response.statusCode == 200) {
        var jsonData = json.decode(response.body) as List;
        incidencias = jsonData
            .map<Incidencia>((json) => Incidencia.fromJson(json))
            .toList();
      }

      return incidencias;
    } catch (e) {
      print('Ocurrio un error al obtener las incidencias: $e');
      throw e;
    }
  }

  static Future<httpClient.Response> addRespuestaIncidencia(
      Respuesta respuesta) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    try {
      var uri = await endpoint() + "incidencias/AddRespuestaIncidencia";
      var token = prefs.getString('token');
      // var jsonText = await rootBundle.loadString('assets/data/platillos.json');
      final response = await httpClient.post(Uri.parse(uri),
          headers: <String, String>{
            'Content-Type': 'application/json; charset=UTF-8',
            'Authorization': 'Bearer $token',
          },
          body: jsonEncode(
            respuesta.toMap(),
          ));
      print("body:${response.body}");
      return response;
    } catch (e) {
      print('Ocurrio un error al obtener las incidencias: $e');
      throw e;
    }
  }
}
