import 'package:alcorp_app/app/core/models/platillo.model.dart';

class Dia {
  int id;
  String titulo;
  List<Platillo> platillos;

  Dia({
    this.id,
    this.titulo,
    this.platillos,
  });

  factory Dia.fromJson(Map<String, dynamic> json) {
    return Dia(
      id: json["id"],
      titulo: json["titulo"].toString(),
      platillos: json['platillos'] != null
          ? (json['platillos'] as List)
              .map((a) => Platillo.fromJson(a))
              .toList()
          : new List<Platillo>(),
    );
  }

  // Convert a Note object into a Map object
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['id'] = id;
    map['titulo'] = titulo;
    map['platillos'] = platillos;

    return map;
  }
}
