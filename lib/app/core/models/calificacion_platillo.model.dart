class CalificacionPlatillo {
  int id;
  int id_empleado;
  int id_platillo;
  int calificacion_cuantitativa;
  String calificacion_cualitativa;
  int usuario_creacion;
  int usuario_modificacion;
  DateTime fecha_creacion;
  DateTime fecha_modificacion;

  CalificacionPlatillo({
    this.id,
    this.id_empleado,
    this.id_platillo,
    this.calificacion_cuantitativa,
    this.calificacion_cualitativa,
    this.usuario_creacion,
    this.usuario_modificacion,
    this.fecha_creacion,
    this.fecha_modificacion,
  });

  factory CalificacionPlatillo.fromJson(Map<String, dynamic> json) {
    return CalificacionPlatillo(
      id: json['id'] as int,
      id_empleado: json['id_empleado'] as int,
      id_platillo: json['id_platillo'] as int,
      calificacion_cuantitativa: json['calificacion_cuantitativa'] as int,
      calificacion_cualitativa: json['calificacion_cualitativa'] as String,
      usuario_creacion: json['usuario_creacion'] as int,
      usuario_modificacion: json['usuario_modificacion'] as int,
      fecha_creacion: json['fecha_creacion'] as DateTime,
      fecha_modificacion: json['fecha_modificacion'] as DateTime,
    );
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['id'] = id;
    map['id_empleado'] = id_empleado;
    map['id_platillo'] = id_platillo;
    map['calificacion_cuantitativa'] = calificacion_cuantitativa;
    map['calificacion_cualitativa'] = calificacion_cualitativa;
    map['usuario_creacion'] = usuario_creacion;
    map['usuario_modificacion'] = usuario_modificacion;
    // map['fecha_creacion'] = fecha_creacion;
    // map['fecha_modificacion'] = fecha_modificacion;

    return map;
  }
}
