class Response {
  String requestId;
  String errorMsg;
  bool showRequestId;

  Response({this.requestId, this.errorMsg, this.showRequestId});

  factory Response.fromJson(Map<String, dynamic> json) {
    return Response(
      requestId: json['requestId'] as String,
      errorMsg: json['errorMsg'] as String,
      showRequestId: json['showRequestId'] as bool,
    );
  }
}
