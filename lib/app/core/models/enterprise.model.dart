
class Enterprise {
  int id;
  String keyCode;
  String name;
  String appName;
  String apiUrl;
  

  Enterprise({
    this.id,
    this.keyCode,
    this.name,
    this.appName,
    this.apiUrl,
  });

  factory Enterprise.fromJson(Map<String, dynamic> json) {
    return Enterprise(
      id: json["id"],
      keyCode: json["keyCode"].toString(),
      name: json["name"].toString(),
      appName: json["appName"].toString(),
      apiUrl: json["apiUrl"].toString(),
    );
  }

  // Convert a Note object into a Map object
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['id'] = id;
    map['keyCode'] = keyCode;
    map['name'] = name;
    map['appName'] = appName;
    map['apiUrl'] = apiUrl;

    return map;
  }
}
