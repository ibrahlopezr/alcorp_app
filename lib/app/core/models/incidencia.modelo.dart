import './respuesta.model.dart';

class Incidencia {
  int id;
  int usuarioId;
  String titulo;
  List<Respuesta> respuestas;

  Incidencia({this.id, this.usuarioId, this.titulo, this.respuestas});

  factory Incidencia.fromJson(Map<String, dynamic> json) {
    return Incidencia(
      id: json["id"],
      usuarioId: json["usuarioId"],
      titulo: json["titulo"].toString(),
      // respuestas: (json['respuestas'] as List)
      //     .map((itemWord) => Respuesta.fromJson(itemWord))
      //     .toList(),
    );
  }

  // Convert a Note object into a Map object
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['id'] = id;
    map['usuarioId'] = usuarioId;
    map['titulo'] = titulo;
    map['respuestas'] = respuestas;

    return map;
  }
}
