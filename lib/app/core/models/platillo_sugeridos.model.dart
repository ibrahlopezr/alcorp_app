class PlatillosSugeridos {
  String name;
  double value;
  double percentage;

  PlatillosSugeridos({
    this.name,
    this.value,
    this.percentage,
  });

  factory PlatillosSugeridos.fromJson(Map<String, dynamic> json) {
    return PlatillosSugeridos(
      name: json['name'] as String,
      value: json['value'] as double,
      percentage: json['percentage'] as double,
    );
  }

  // Convert a Note object into a Map object
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['name'] = name;
    map['value'] = value;
    map['percentage'] = percentage;

    return map;
  }
}
