class LoginResponse {
  String token;
  String image;
  List<dynamic> routes;
  LoginResponse({this.token, this.image, this.routes});

  factory LoginResponse.fromJson(Map<String, dynamic> json) {
    return LoginResponse(
      token: json['token'].toString(),
      image: json['image'].toString(),
      routes: json['routes'] as List<dynamic>,
    );
  }
}
