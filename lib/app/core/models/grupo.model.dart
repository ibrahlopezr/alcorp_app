class Grupo {
  int id;
  String titulo;
  String image;

  Grupo({
    this.id,
    this.titulo,
    this.image,
  });

  factory Grupo.fromJson(Map<String, dynamic> json) {
    return Grupo(
      id: json["id"],
      titulo: json["titulo"].toString(),
      image: json["image"].toString(),
    );
  }

  // Convert a Note object into a Map object
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['id'] = id;
    map['titulo'] = titulo;
    map['image'] = image;

    return map;
  }
}
