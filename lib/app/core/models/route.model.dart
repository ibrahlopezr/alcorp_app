class Routes {
  int id;
  String nombre;
  String ruta;

  Routes({
    this.id,
    this.nombre,
    this.ruta,
  });

  factory Routes.fromJson(Map<String, dynamic> json) {
    return Routes(
      id: json["id"],
      nombre: json["nombre"].toString(),
      ruta: json["ruta"].toString(),
    );
  }

  // Convert a Note object into a Map object
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['id'] = id;
    map['nombre'] = nombre;
    map['ruta'] = ruta;

    return map;
  }
}
