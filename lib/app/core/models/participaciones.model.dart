class Participaciones {
  int totalParticipantes;
  int totalParticipaciones;
  int totalRestantes;
  double totalParticipantesPorcentaje;
  double totalParticipacionesPorcentaje;
  double totalRestantesPorcentaje;

  Participaciones({
    this.totalParticipantes,
    this.totalParticipaciones,
    this.totalRestantes,
    this.totalParticipantesPorcentaje,
    this.totalParticipacionesPorcentaje,
    this.totalRestantesPorcentaje,
  });

  factory Participaciones.fromJson(Map<String, dynamic> json) {
    return Participaciones(
      totalParticipantes: json['totalParticipantes'] as int,
      totalParticipaciones: json['totalParticipaciones'] as int,
      totalRestantes: json['totalRestantes'] as int,
      totalParticipantesPorcentaje:
          json['totalParticipantesPorcentaje'] as double,
      totalParticipacionesPorcentaje:
          json['totalParticipacionesPorcentaje'] as double,
      totalRestantesPorcentaje: json['totalRestantesPorcentaje'] as double,
    );
  }

  // Convert a Note object into a Map object
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['totalParticipantes'] = totalParticipantes;
    map['totalParticipaciones'] = totalParticipaciones;
    map['totalRestantes'] = totalRestantes;
    map['totalParticipantesPorcentaje'] = totalParticipantesPorcentaje;
    map['totalParticipacionesPorcentaje'] = totalParticipacionesPorcentaje;
    map['totalRestantesPorcentaje'] = totalRestantesPorcentaje;

    return map;
  }
}
