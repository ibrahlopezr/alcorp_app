import 'package:alcorp_app/app/core/models/platillo.model.dart';

class Votacion {
  int id;
  String titulo;
  bool votado;
  List<Platillo> platillos;

  Votacion({
    this.id,
    this.titulo,
    this.votado,
    this.platillos,
  });

  factory Votacion.fromJson(Map<String, dynamic> json) {
    return Votacion(
      id: json["id"],
      titulo: json["titulo"].toString(),
      votado: json["votado"],
      platillos: (json['platillos'] as List)
          .map((platillo) => Platillo.fromJson(platillo))
          .toList(),
    );
  }

  // Convert a Note object into a Map object
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['id'] = id;
    map['titulo'] = titulo;
    map['votado'] = votado;
    map['platillos'] = platillos;

    return map;
  }
}
