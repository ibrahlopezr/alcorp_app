class Platillo {
  int id;
  String titulo;
  String image;
  String dia;
  bool votado;
  int porcentaje;

  Platillo({
    this.id,
    this.titulo,
    this.image,
    this.dia,
    this.votado,
    this.porcentaje,
  });

  factory Platillo.fromJson(Map<String, dynamic> json) {
    return Platillo(
      id: json["id"],
      titulo: json["titulo"].toString(),
      image: json["image"].toString(),
      dia: json["dia"].toString(),
      votado: json["votado"] == null ? false : json["votado"],
      porcentaje: json["porcentaje"] == null
          ? 0
          : int.parse(json["porcentaje"].toString()),
    );
  }

  // Convert a Note object into a Map object
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['id'] = id;
    map['titulo'] = titulo;
    map['image'] = image;
    map['dia'] = dia;
    map['votado'] = votado;
    map['porcenjate'] = porcentaje;

    return map;
  }
}
