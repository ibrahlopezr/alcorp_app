import 'dart:convert';

class FormFields {
  int id;
  String formName;
  String title;
  String validator;
  String type;
  String response;
  bool switchValue;
  String placeholder;

  FormFields({
    this.id,
    this.formName,
    this.title,
    this.validator,
    this.type,
    this.response,
    this.placeholder,
    this.switchValue,
  });

  Map<String, dynamic> toMap() {
    final responseData = response.isEmpty
        ? switchValue
            ? "1"
            : "0"
        : response;
    return {
      'id': id ?? 0,
      'formName': formName ?? '',
      'title': title ?? '',
      'validator': validator ?? '',
      'type': type ?? '',
      'response': responseData,
      'placeholder': placeholder ?? '',
      'switchValue': switchValue ?? '0',
    };
  }

  factory FormFields.fromMap(Map<String, dynamic> map) {
    final responseFinal = map['switchValue'] != null
        ? map['switchValue']
            ? '1'
            : '0'
        : '0' ?? map['response'] ?? '0';
    return FormFields(
      id: map['id'] ?? 0,
      formName: map['formName'] ?? '',
      title: map['title'] ?? '',
      validator: map['validator'] ?? '',
      type: map['type'] ?? '',
      response: responseFinal,
      placeholder: map['placeholder'] ?? '',
      switchValue: map['switchValue'] ?? false,
    );
  }

  String toJson() => json.encode(toMap());

  factory FormFields.fromJson(Map<String, dynamic> json) =>
      FormFields.fromMap(json);

  @override
  String toString() {
    return 'FormFields(id: $id, formName: $formName, title: $title, validator: $validator, type: $type, response: $response, placeholder: $placeholder, switchValue: $switchValue)';
  }

  @override
  bool operator ==(Object other) {
    if (identical(this, other)) return true;

    return other is FormFields &&
        other.id == id &&
        other.formName == formName &&
        other.title == title &&
        other.validator == validator &&
        other.type == type &&
        other.placeholder == placeholder &&
        other.response == response;
  }

  @override
  int get hashCode {
    return id.hashCode ^
        formName.hashCode ^
        title.hashCode ^
        validator.hashCode ^
        type.hashCode ^
        placeholder.hashCode ^
        response.hashCode;
  }

  FormFields copyWith({
    int id,
    String formName,
    String title,
    String validator,
    String type,
    String response,
    String placeholder,
  }) {
    return FormFields(
      id: id ?? this.id,
      formName: formName ?? this.formName,
      title: title ?? this.title,
      validator: validator ?? this.validator,
      type: type ?? this.type,
      placeholder: placeholder ?? this.placeholder,
      response: response ?? this.response,
      switchValue: switchValue ?? this.switchValue,
    );
  }
}
