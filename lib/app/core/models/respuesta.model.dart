class Respuesta {
  int id;
  int incidenciaId;
  int usuarioId;
  String descripcion;

  Respuesta({this.id, this.incidenciaId, this.descripcion, this.usuarioId});

  factory Respuesta.fromJson(Map<String, dynamic> json) {
    return Respuesta(
      id: int.parse(json["id"].toString()),
      incidenciaId: int.parse(json["incidenciaId"].toString()),
      descripcion: json["descripcion"].toString(),
      usuarioId: int.parse(json["usuarioId"].toString()),
    );
  }

  // Convert a Note object into a Map object
  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['id'] = id == null ? 0 : id;
    map['incidenciaId'] = incidenciaId == null ? 0 : incidenciaId;
    map['descripcion'] = descripcion == null ? '' : descripcion;
    map['usuarioId'] = usuarioId == null ? 0 : usuarioId;

    return map;
  }
}
