class Sugerencia {
  int id;
  String nombre;
  int usuarioCreacion;
  int usuarioModificacion;
  // DateTime fechaCreacion;
  // DateTime fechaModificacion;

  Sugerencia({
    this.id,
    this.nombre,
    this.usuarioCreacion,
    this.usuarioModificacion,
  });

  factory Sugerencia.fromJson(Map<String, dynamic> json) {
    return Sugerencia(
        id: json['id'] as int,
        nombre: json['nombre'] as String,
        usuarioCreacion: json['usuarioCreacion'] as int,
        usuarioModificacion: json['usuarioModificacion'] as int);
  }

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    map['id'] = id;
    map['nombre'] = nombre;
    map['usuarioCreacion'] = usuarioCreacion;
    map['usuarioModificacion'] = usuarioModificacion;
    return map;
  }
}
