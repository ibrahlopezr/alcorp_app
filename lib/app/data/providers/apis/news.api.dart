import 'dart:convert';

import 'package:alcorp_app/app/core/services/base_url.dart';
import 'package:flutter/material.dart';
import 'package:get/get_connect/http/src/status/http_status.dart';
import 'package:http/http.dart' as http;

class NewsClient {
  final http.Client httpClient;

  // Constructor.
  NewsClient({@required this.httpClient});

  getAll() async {
    throw UnimplementedError();
  }

  getId(id) async {
    throw UnimplementedError();
  }
}
