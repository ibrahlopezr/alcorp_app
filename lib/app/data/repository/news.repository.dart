import 'package:alcorp_app/app/data/providers/apis/news.api.dart';
import 'package:meta/meta.dart';

class NewsRepository {
  final NewsClient userClient;

  NewsRepository({
    @required this.userClient,
  }) : assert(userClient != null);

  getAll() {
    return userClient.getAll();
  }

  getId(id) {
    return userClient.getId(id);
  }
}
