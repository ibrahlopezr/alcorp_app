import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:alcorp_app/app/core/services/auth.service.dart';
import 'package:alcorp_app/app/shared/utils/backgroun.painter.dart';
import 'package:alcorp_app/app/shared/widgets/modal_rounded_progress.widget.dart';
import 'package:alcorp_app/app/shared/widgets/tab_component.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:email_validator/email_validator.dart';

class ForgotPasswordPage extends StatefulWidget {
  ForgotPasswordPage({Key key}) : super(key: key);

  @override
  _ForgotPasswordPageState createState() => _ForgotPasswordPageState();
}

class _ForgotPasswordPageState extends State<ForgotPasswordPage> {
  TextEditingController _txtEmail = new TextEditingController();
  TextEditingController _txtCodigo = new TextEditingController();
  TextEditingController _txtPassword = new TextEditingController();
  TextEditingController _txtConfirmPassword = new TextEditingController();
  bool sendCode = true;
  bool confirmCode = false;
  bool changePassword = false;
  bool done = false;
  bool _obscureText = true;
  ProgressBarHandler _handler;

  @override
  void initState() {
    super.initState();
    analyticsEvent();
  }

  Future<void> analyticsEvent() async {
    await FirebaseAnalytics.instance
        .setCurrentScreen(screenName: 'forgotPassword');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: CustomPaint(
              painter: BackgroundPainter(),
            ),
          ),
          SingleChildScrollView(
            child: Container(
              width: MediaQuery.of(context).size.width,
              // height: MediaQuery.of(context).size.height * .7,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  AppBar(
                    backgroundColor: CustomColors.primaryColor,
                    elevation: 0,
                    leading: IconButton(
                      icon: SvgPicture.asset(
                        'assets/icons/arrow-left.svg',
                        color: Colors.white,
                      ),
                      onPressed: () => {Navigator.of(context).pop()},
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 25.0),
                    child: Text(
                      'Restablecer contraseña',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 28.0,
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  Container(
                    height: MediaQuery.of(context).size.height * .5,
                    child: Card(
                      elevation: 8,
                      margin: const EdgeInsets.all(25.0),
                      color: Color(0xfffffdfd),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Visibility(
                            visible: sendCode,
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 12.0,
                                    left: 20.0,
                                    right: 20.0,
                                    bottom: 12.0,
                                  ),
                                  child: TextFormField(
                                    validator: (email) {
                                      if (email.isEmpty) {
                                        return "El correo electronico es obligatorio.";
                                      } else if (!EmailValidator.validate(
                                          email)) {
                                        return "Ingrese un correo electronico valido.";
                                      } else {
                                        return null;
                                      }
                                    },
                                    keyboardType: TextInputType.emailAddress,
                                    controller: _txtEmail,
                                    decoration: InputDecoration(
                                      suffixIcon: Icon(
                                        Icons.email,
                                        color: Color(0xffb2302a),
                                        size: 30,
                                      ),
                                      hintText: 'Correo electrónico',
                                      fillColor: Color(0xfffffdfd),
                                      hintStyle: TextStyle(
                                        color: Colors.redAccent,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                        borderSide: BorderSide.none,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 12.0,
                                    left: 20.0,
                                    right: 20.0,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Center(
                                        child: ButtonTheme(
                                          minWidth: 140.0,
                                          height: 45.0,
                                          child: RaisedButton(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                            color: CustomColors.primaryColor,
                                            onPressed: () async {
                                              await FirebaseAnalytics.instance
                                                  .logEvent(
                                                name: 'sendCodeForgotPassword',
                                                parameters: {
                                                  "email": _txtEmail.text
                                                },
                                              );
                                              try {
                                                _handler.show();
                                                final result = await AuthService
                                                    .forgotPassword(
                                                  _txtEmail.text,
                                                );
                                                if (result > 0) {
                                                  _handler.dismiss();
                                                  setState(() {
                                                    sendCode = false;
                                                    confirmCode = true;
                                                  });
                                                } else {
                                                  _handler.dismiss();
                                                  final snackBar = SnackBar(
                                                    content: Text(
                                                      'Ocurrio un error al enviar el codigo.',
                                                    ),
                                                  );
                                                  ScaffoldMessenger.of(context)
                                                      .showSnackBar(snackBar);
                                                }
                                              } catch (e) {
                                                _handler.dismiss();
                                                final snackBar = SnackBar(
                                                  content: Text(
                                                    'Ocurrio un error: $e',
                                                  ),
                                                );
                                                ScaffoldMessenger.of(context)
                                                    .showSnackBar(snackBar);
                                              }
                                            },
                                            child: Text(
                                              "Enviar código",
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white,
                                                fontSize: 14.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Visibility(
                            visible: confirmCode,
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 12.0,
                                    left: 20.0,
                                    right: 20.0,
                                    bottom: 12.0,
                                  ),
                                  child: TextFormField(
                                    validator: (codigo) {
                                      if (codigo.isEmpty) {
                                        return "El código es obligatorio.";
                                      } else {
                                        return null;
                                      }
                                    },
                                    keyboardType: TextInputType.phone,
                                    inputFormatters: <TextInputFormatter>[
                                      FilteringTextInputFormatter.allow(
                                        RegExp(r'[0-9]'),
                                      ),
                                    ],
                                    controller: _txtCodigo,
                                    decoration: InputDecoration(
                                      suffixIcon: Icon(
                                        Icons.aod,
                                        color: Color(0xffb2302a),
                                        size: 30,
                                      ),
                                      hintText: 'Código',
                                      fillColor: Color(0xfffffdfd),
                                      hintStyle: TextStyle(
                                        color: Colors.redAccent,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                        borderSide: BorderSide.none,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 12.0,
                                    left: 20.0,
                                    right: 20.0,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Center(
                                        child: ButtonTheme(
                                          minWidth: 140.0,
                                          height: 45.0,
                                          child: RaisedButton(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                            color: CustomColors.primaryColor,
                                            onPressed: () async {
                                              try {
                                                _handler.show();
                                                final result = await AuthService
                                                    .validarCodigo(
                                                  _txtEmail.text,
                                                  _txtCodigo.text,
                                                );
                                                if (result > 0) {
                                                  _handler.dismiss();
                                                  setState(() {
                                                    confirmCode = false;
                                                    changePassword = true;
                                                  });
                                                } else {
                                                  _handler.dismiss();
                                                  final snackBar = SnackBar(
                                                    content: Text(
                                                        'Ocurrio un error al validar el codigo.'),
                                                  );
                                                  ScaffoldMessenger.of(context)
                                                      .showSnackBar(snackBar);
                                                }
                                              } catch (e) {
                                                _handler.dismiss();
                                                final snackBar = SnackBar(
                                                  content: Text(
                                                    'Ocurrio un error: $e',
                                                  ),
                                                );
                                                ScaffoldMessenger.of(context)
                                                    .showSnackBar(snackBar);
                                              }
                                            },
                                            child: Text(
                                              "Confirmar código",
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white,
                                                fontSize: 14.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Visibility(
                            visible: changePassword,
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 12.0,
                                    left: 20.0,
                                    right: 20.0,
                                    bottom: 12.0,
                                  ),
                                  child: TextFormField(
                                    validator: (email) {
                                      if (email.isEmpty) {
                                        return "La contraseña es obligatoria.";
                                      } else {
                                        return null;
                                      }
                                    },
                                    keyboardType: TextInputType.visiblePassword,
                                    controller: _txtPassword,
                                    obscureText: _obscureText,
                                    decoration: InputDecoration(
                                      suffixIcon: GestureDetector(
                                        onTap: () {
                                          setState(() =>
                                              _obscureText = !_obscureText);
                                        },
                                        child: Icon(
                                          Icons.remove_red_eye_outlined,
                                          color: Color(0xffb2302a),
                                          size: 30,
                                        ),
                                      ),
                                      hintText: 'Contraseña',
                                      fillColor: Color(0xfffffdfd),
                                      hintStyle: TextStyle(
                                        color: Colors.redAccent,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                        borderSide: BorderSide.none,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 12.0,
                                    left: 20.0,
                                    right: 20.0,
                                    bottom: 12.0,
                                  ),
                                  child: TextFormField(
                                    validator: (value) {
                                      if (value.isEmpty) {
                                        return "Confirmar contraseña es obligatorio.";
                                      } else {
                                        return null;
                                      }
                                    },
                                    keyboardType: TextInputType.visiblePassword,
                                    controller: _txtConfirmPassword,
                                    obscureText: _obscureText,
                                    decoration: InputDecoration(
                                      suffixIcon: GestureDetector(
                                        onTap: () {
                                          setState(() =>
                                              _obscureText = !_obscureText);
                                        },
                                        child: Icon(
                                          Icons.remove_red_eye_outlined,
                                          color: Color(0xffb2302a),
                                          size: 30,
                                        ),
                                      ),
                                      hintText: 'Confirmar contraseña',
                                      fillColor: Color(0xfffffdfd),
                                      hintStyle: TextStyle(
                                        color: Colors.redAccent,
                                        fontWeight: FontWeight.bold,
                                      ),
                                      focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(15.0),
                                        borderSide: BorderSide.none,
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 12.0,
                                    left: 20.0,
                                    right: 20.0,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Center(
                                        child: ButtonTheme(
                                          minWidth: 140.0,
                                          height: 45.0,
                                          child: RaisedButton(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                            color: CustomColors.primaryColor,
                                            onPressed: () async {
                                              try {
                                                _handler.show();
                                                if (_txtPassword.text ==
                                                    _txtConfirmPassword.text) {
                                                  final result =
                                                      await AuthService
                                                          .changePassword(
                                                              _txtEmail.text,
                                                              _txtPassword
                                                                  .text);
                                                  if (result > 0) {
                                                    _handler.dismiss();
                                                    setState(() {
                                                      changePassword = false;
                                                      done = true;
                                                    });
                                                  } else {
                                                    _handler.dismiss();
                                                    final snackBar = SnackBar(
                                                      content: Text(
                                                          'Ocurrio un error al cambiar la contraseña'),
                                                    );
                                                    ScaffoldMessenger.of(
                                                            context)
                                                        .showSnackBar(snackBar);
                                                  }
                                                } else {
                                                  _handler.dismiss();
                                                  final snackBar = SnackBar(
                                                    content: Text(
                                                        'Las contraseñas no coinciden.'),
                                                  );
                                                  ScaffoldMessenger.of(context)
                                                      .showSnackBar(snackBar);
                                                }
                                              } catch (e) {
                                                _handler.dismiss();
                                                final snackBar = SnackBar(
                                                  content: Text(
                                                    'Ocurrio un error: $e',
                                                  ),
                                                );
                                                ScaffoldMessenger.of(context)
                                                    .showSnackBar(snackBar);
                                              }
                                            },
                                            child: Text(
                                              "Cambiar contraseña",
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white,
                                                fontSize: 14.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Visibility(
                            visible: done,
                            child: Column(
                              children: [
                                FittedBox(
                                  fit: BoxFit.fitWidth,
                                  child: Center(
                                    child: Padding(
                                      padding: const EdgeInsets.all(20.0),
                                      child: Text(
                                        "Su contraseña ha sido cambiada",
                                        style: TextStyle(
                                          color: CustomColors.greyColor,
                                          fontWeight: FontWeight.bold,
                                          fontSize: 18.0,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.only(
                                    top: 12.0,
                                    left: 20.0,
                                    right: 20.0,
                                  ),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Center(
                                        child: ButtonTheme(
                                          minWidth: 140.0,
                                          height: 45.0,
                                          child: RaisedButton(
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                                  BorderRadius.circular(10.0),
                                            ),
                                            color: CustomColors.primaryColor,
                                            onPressed: () async {
                                              await Navigator.of(context).pop();
                                            },
                                            child: Text(
                                              "Ir al Login",
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: Colors.white,
                                                fontSize: 14.0,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          ModalRoundedProgressBar(
            //getting the handler
            // ignore: missing_return
            handleCallback: (handler) {
              _handler = handler;
            },
          )
        ],
      ),
    );
    ;
  }
}
