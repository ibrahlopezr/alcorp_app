import 'dart:convert';
import 'dart:ui';
import 'dart:io' show Platform;

// my imports
import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:new_version/new_version.dart';
import 'package:upgrader/upgrader.dart';
import 'package:alcorp_app/app/core/models/roles.model.dart';
import 'package:alcorp_app/app/core/services/auth.service.dart';
import 'package:alcorp_app/app/core/services/enterprise.service.dart';
import 'package:alcorp_app/app/pages/auth/forgot_password_page.dart';
import 'package:alcorp_app/app/pages/auth/register_page.dart';
import 'package:alcorp_app/app/pages/home/home_page.dart';
import 'package:alcorp_app/app/shared/widgets/modal_rounded_progress.widget.dart';
import 'package:alcorp_app/app/shared/widgets/tab_component.dart';
import 'package:email_validator/email_validator.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:jwt_decode/jwt_decode.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../shared/utils/globals.dart' as globals;
import 'form_login.widget.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:package_info_plus/package_info_plus.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key}) : super(key: key);

  @override
  _LoginPage createState() => _LoginPage();
}

class _LoginPage extends State<LoginPage> {
  Future<bool> _isLoggedFuture;
  TextEditingController _txtCompania = new TextEditingController();
  TextEditingController _txtNumeroTelefono = new TextEditingController();
  TextEditingController _txtPassword = new TextEditingController();
  ProgressBarHandler _handler;
  bool _obscureText = true;
  bool _wrongNumber = false;
  String _wrongNumberText;
  String _password;
  bool _wrongPassword = false;
  String _wrongPasswordText;
  final _formGlobalKey = GlobalKey<FormState>();
  // String _password;
  FirebaseAnalytics analytics = FirebaseAnalytics.instance;
  String username = '';
  NewVersion newVersion;
  VersionStatus status;
  PackageInfo packageInfo;
  String versionNumber = "1.0.0";
  // Toggles the password show status

  @override
  void initState() {
    super.initState();
    _isLoggedFuture = this.isLogged();
    this.initAnalytics();
    statusVersion(context);
  }

  Future statusVersion(BuildContext context) async {
    newVersion = new NewVersion(context: context);
    status = await newVersion.getVersionStatus();
    // print('Status: ${jsonEncode(status)}');
    // newVersion.showUpdateDialog(status);
    packageInfo = await PackageInfo.fromPlatform();
    versionNumber = packageInfo.version;
    newVersion.showAlertIfNecessary();
    // return status.canUpdate;
    setState(() {});
  }

  void initAnalytics() async {
    try {
      print('initAnalytics');
      username = await AuthService.getCurrentUserName();

      await FirebaseAnalytics.instance.logEvent(
        name: 'login_page',
        parameters: {
          'user': username ?? 'usuario no loggeado',
        },
      );
      await FirebaseAnalytics.instance
          .setCurrentScreen(screenName: 'login_page');
    } catch (e) {
      print("Error Analytics: $e");
    }
  }

  @override
  Widget build(BuildContext context) {
    // Only call clearSavedSettings() during testing to reset internal values.
    Upgrader().clearSavedSettings(); // REMOVE this for release builds

    // On Android, setup the Appcast.
    // On iOS, the default behavior will be to use the App Store version of
    // the app, so update the Bundle Identifier in example/ios/Runner with a
    // valid identifier already in the App Store.

    return FutureBuilder<bool>(
        future: this._isLoggedFuture,
        builder: (_, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data != null && snapshot.data) {
              return TabBarComponent(currentPageIndex: 0);
            } else {
              return Scaffold(
                resizeToAvoidBottomInset: true,
                body: Stack(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: AssetImage("assets/images/bckg-login.png"),
                          //image: SvgPicture.asset("assets/images/bckg-login.svg"),
                          fit: BoxFit.fitHeight,
                          colorFilter: ColorFilter.mode(
                              Colors.black.withOpacity(0.30), BlendMode.darken),
                        ),
                      ),
                      //child: Image.asset('assets/images/login-bg.jpg', ),
                    ),
                    Column(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Center(
                          child: Container(
                            padding: EdgeInsets.only(
                                top: MediaQuery.of(context).size.width * .35),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  "¡BIENVENIDO!",
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 30.0,
                                    fontWeight: FontWeight.w700,
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                    // Card principal
                    SingleChildScrollView(
                      child: Container(
                        margin: EdgeInsets.only(
                          top: MediaQuery.of(context).size.height * .30,
                        ),
                        height: MediaQuery.of(context).size.height - 180.0,
                        decoration: BoxDecoration(
                          color: Color(0xfffafafa),
                          borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(50.0),
                            topRight: Radius.circular(50.0),
                          ),
                        ),
                        child: ListView(
                          primary: false,
                          padding: EdgeInsets.only(left: 25.0, right: 20.0),
                          children: [
                            Padding(
                              padding: EdgeInsets.only(top: 10.0),
                              child: Container(
                                height:
                                    MediaQuery.of(context).size.height * .80,
                                child: Container(
                                  child: ListView(
                                    children: [
                                      Center(
                                        child: Text(
                                          "LOGIN",
                                          style: TextStyle(
                                              color: Colors.redAccent,
                                              fontSize: 28,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                      //Underline login title
                                      UnderlineLoginTitle(),
                                      // Form login
                                      Form(
                                        key: _formGlobalKey,
                                        autovalidateMode:
                                            AutovalidateMode.always,
                                        child: Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                top: 12.0,
                                                left: 20.0,
                                                right: 20.0,
                                                bottom: 12.0,
                                              ),
                                              child: Material(
                                                borderRadius:
                                                    BorderRadius.circular(15.0),
                                                elevation: 8.0,
                                                //shadowColor: Colors.white,
                                                child: TextFormField(
                                                  validator: (value) {
                                                    return validateNumber(
                                                        value);
                                                  },
                                                  keyboardType:
                                                      TextInputType.text,
                                                  controller: _txtCompania,
                                                  decoration: InputDecoration(
                                                    suffixIcon: Icon(
                                                      Icons.business_rounded,
                                                      color: Color(0xffb2302a),
                                                      size: 30,
                                                    ),
                                                    hintText: 'Compañia',
                                                    fillColor:
                                                        Color(0xfffffdfd),
                                                    hintStyle: TextStyle(
                                                      color: Colors.redAccent,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                    focusedBorder:
                                                        OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              15.0),
                                                      borderSide:
                                                          BorderSide.none,
                                                    ),
                                                    enabledBorder:
                                                        OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                        15.0,
                                                      ),
                                                      borderSide: BorderSide(
                                                        color: Colors.white,
                                                        width: 2.0,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                top: 12.0,
                                                left: 20.0,
                                                right: 20.0,
                                                bottom: 12.0,
                                              ),
                                              child: Material(
                                                borderRadius:
                                                    BorderRadius.circular(15.0),
                                                elevation: 8.0,
                                                //shadowColor: Colors.white,
                                                child: TextFormField(
                                                  validator: (value) {
                                                    return validateNumber(
                                                        value);
                                                  },
                                                  keyboardType: TextInputType
                                                      .numberWithOptions(
                                                          signed: true,
                                                          decimal: true),
                                                  inputFormatters: <
                                                      TextInputFormatter>[
                                                    FilteringTextInputFormatter
                                                        .allow(
                                                      RegExp(r'[0-9]'),
                                                    ),
                                                  ],
                                                  controller:
                                                      _txtNumeroTelefono,
                                                  decoration: InputDecoration(
                                                    suffixIcon: Icon(
                                                      Icons.people_outlined,
                                                      color: Color(0xffb2302a),
                                                      size: 30,
                                                    ),
                                                    hintText:
                                                        'Numero de Telefono',
                                                    fillColor:
                                                        Color(0xfffffdfd),
                                                    hintStyle: TextStyle(
                                                      color: Colors.redAccent,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                    focusedBorder:
                                                        OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                              15.0),
                                                      borderSide:
                                                          BorderSide.none,
                                                    ),
                                                    enabledBorder:
                                                        OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                        15.0,
                                                      ),
                                                      borderSide: BorderSide(
                                                        color: Colors.white,
                                                        width: 2.0,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Visibility(
                                              visible: _wrongNumber,
                                              child: Center(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(5.0),
                                                  child: Text(
                                                    '$_wrongNumberText',
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontSize: 14.0,
                                                      color: Colors.red,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                top: 6.0,
                                                left: 20.0,
                                                right: 20.0,
                                                bottom: 12.0,
                                              ),
                                              child: Material(
                                                borderRadius:
                                                    BorderRadius.circular(15.0),
                                                elevation: 8.0,
                                                //shadowColor: Colors.white,
                                                child: TextFormField(
                                                  validator: (val) {
                                                    return validatePassword(
                                                        val);
                                                  },
                                                  onSaved: (val) =>
                                                      this._password = val,
                                                  keyboardType:
                                                      TextInputType.text,
                                                  controller: _txtPassword,
                                                  obscureText: _obscureText,
                                                  decoration: InputDecoration(
                                                    suffixIcon: GestureDetector(
                                                      onTap: () {
                                                        setState(() =>
                                                            _obscureText =
                                                                !_obscureText);
                                                      },
                                                      child: Icon(
                                                        Icons
                                                            .remove_red_eye_outlined,
                                                        color:
                                                            Color(0xffb2302a),
                                                        size: 30,
                                                      ),
                                                    ),
                                                    hintText: 'Contraseña',
                                                    fillColor:
                                                        Color(0xfffffdfd),
                                                    hintStyle: TextStyle(
                                                      color: Colors.redAccent,
                                                      fontWeight:
                                                          FontWeight.bold,
                                                    ),
                                                    focusedBorder:
                                                        OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                        15.0,
                                                      ),
                                                      borderSide: BorderSide(
                                                        color: Colors.white,
                                                      ),
                                                    ),
                                                    enabledBorder:
                                                        OutlineInputBorder(
                                                      borderRadius:
                                                          BorderRadius.circular(
                                                        15.0,
                                                      ),
                                                      borderSide: BorderSide(
                                                        color: Colors.white,
                                                        width: 2.0,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Visibility(
                                              visible: _wrongPassword,
                                              child: Center(
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(5.0),
                                                  child: Text(
                                                    '$_wrongPasswordText',
                                                    textAlign: TextAlign.left,
                                                    style: TextStyle(
                                                      fontSize: 14.0,
                                                      color: Colors.red,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                top: 6.0,
                                                left: 20.0,
                                                right: 20.0,
                                                bottom: 6.0,
                                              ),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                children: [
                                                  Text(
                                                    "¿No tienes cuenta? ",
                                                    style: TextStyle(
                                                      color: Color(0xffb7b7b7),
                                                      fontWeight:
                                                          FontWeight.normal,
                                                    ),
                                                  ),
                                                  GestureDetector(
                                                    onTap: () async {
                                                      // Add Event forgotPassword.
                                                      await FirebaseAnalytics
                                                          .instance
                                                          .logEvent(
                                                        name: 'registrate',
                                                        parameters: {
                                                          "username": username
                                                        },
                                                      );

                                                      Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                          builder: (context) =>
                                                              RegisterPage(),
                                                        ),
                                                      );
                                                      // if (result) {
                                                      //   print(
                                                      //       'Termino de hacer la navegacion a RegisterPage');
                                                      // }
                                                    },
                                                    child: Text(
                                                      "Registrate",
                                                      style: TextStyle(
                                                        color: CustomColors
                                                            .primaryColor,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                top: 6.0,
                                                left: 20.0,
                                                right: 20.0,
                                                bottom: 6.0,
                                              ),
                                              child: Row(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                children: [
                                                  GestureDetector(
                                                    onTap: () async {
                                                      // Add Event forgotPassword.
                                                      await FirebaseAnalytics
                                                          .instance
                                                          .logEvent(
                                                        name: 'forgotPassword',
                                                        parameters: {
                                                          "username": username
                                                        },
                                                      );

                                                      Navigator.push(
                                                        context,
                                                        MaterialPageRoute(
                                                          builder: (context) =>
                                                              ForgotPasswordPage(),
                                                        ),
                                                      );
                                                    },
                                                    child: Text(
                                                      "¿Olvidaste tu contraseña?",
                                                      style: TextStyle(
                                                        color: CustomColors
                                                            .primaryColor,
                                                        fontWeight:
                                                            FontWeight.bold,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding: const EdgeInsets.only(
                                                top: 12.0,
                                                left: 20.0,
                                                right: 20.0,
                                              ),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: [
                                                  Center(
                                                    child: ButtonTheme(
                                                      minWidth: 170.0,
                                                      height: 45.0,
                                                      child: RaisedButton(
                                                        shape:
                                                            RoundedRectangleBorder(
                                                          borderRadius:
                                                              BorderRadius
                                                                  .circular(
                                                                      10.0),
                                                        ),
                                                        color: CustomColors
                                                            .primaryColor,
                                                        onPressed: () async {
                                                          validateNumber(
                                                            _txtNumeroTelefono
                                                                .text,
                                                          );
                                                          validatePassword(
                                                            _txtPassword.text,
                                                          );
                                                          if (_txtNumeroTelefono
                                                                      .text !=
                                                                  '' &&
                                                              _txtPassword
                                                                      .text !=
                                                                  '') {
                                                            _handler.show();
                                                            await login();
                                                          }
                                                          setState(() {});
                                                        },
                                                        child: Text(
                                                          "LOGIN",
                                                          style: TextStyle(
                                                            fontWeight:
                                                                FontWeight.bold,
                                                            color: Colors.white,
                                                            fontSize: 20.0,
                                                          ),
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                            Padding(
                                              padding:
                                                  const EdgeInsets.all(18.0),
                                              child: Row(
                                                textDirection:
                                                    TextDirection.ltr,
                                                mainAxisAlignment:
                                                    MainAxisAlignment.end,
                                                crossAxisAlignment:
                                                    CrossAxisAlignment.end,
                                                children: [
                                                  Text(
                                                    'v${versionNumber}',
                                                    textAlign: TextAlign.right,
                                                    style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      color: Colors.black,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),

                    ModalRoundedProgressBar(
                      //getting the handler
                      // ignore: missing_return
                      handleCallback: (handler) {
                        _handler = handler;
                      },
                    )
                  ],
                ),
              );
            }
          } else if (snapshot.connectionState == ConnectionState.waiting &&
              !snapshot.hasError) {
            return Scaffold(
              body: Container(
                color: Colors.white,
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Logo(),
                  ],
                ),
              ),
            );
          } else {
            return Scaffold(
              body: Center(
                child: Text(
                  'Ocurrio un error al cargar la aplicacion\n Comuniquese con soporte.',
                  style: TextStyle(fontSize: 16.0),
                ),
              ),
            );
          }
        });
  }

  Null validatePassword(String val) {
    if (val.length < 1) {
      this._wrongPassword = true;
      this._wrongPasswordText = 'La contraseña es muy corta.';
      return null;
    } else {
      this._wrongPassword = false;
      this._wrongPasswordText = '';
      return null;
    }
  }

  Null validateNumber(String value) {
    if (value.isNotEmpty) {
      _wrongNumber = false;
      _wrongNumberText = '';
      return null;
    } else {
      _wrongNumber = true;
      _wrongNumberText = "El número de telefono es obligatorio";
      return null;
    }
  }

  Future<bool> isLogged() async {
    final prefs = await SharedPreferences.getInstance();
    String token = prefs.getString('token');
    var result = false;
    if (token != null && token != '') {
      Map<String, dynamic> payload = Jwt.parseJwt(token);
      print("payload: ${jsonEncode(payload)}");
      globals.isGerente = payload['gerente'] == "1";
      globals.voteGroup = payload['voteGroup'] == "1";
      globals.role = int.parse(payload['role']);
      var dateExpirationToken =
          DateTime.fromMillisecondsSinceEpoch(payload['exp'] * 1000);
      var currentDate = DateTime.now();
      var tokenAvailable = dateExpirationToken.compareTo(currentDate) >= 0;
      if (tokenAvailable) {
        result = true;
        globals.isLoggedIn = result;
        return result;
      } else {
        result = false;
        globals.isLoggedIn = result;
        return result;
      }
    } else {
      result = false;
      globals.isLoggedIn = result;
      return result;
    }
  }

  void login() async {
    try {
      final String appName = _txtCompania.text;
      final enterprisesList = await EnterpriseService.getEnterprise(appName);
      if (enterprisesList != null) {
        final apiUrl = enterprisesList
            .where((element) => element.appName.toUpperCase() == 'METRICS')
            .single
            .apiUrl;
        if (!apiUrl.isEmpty) {
          try {
            final prefs = await SharedPreferences.getInstance();
            prefs.setString('apiUrl', apiUrl);
            var response = await AuthService.login(
                _txtNumeroTelefono.text, _txtPassword.text);
            _handler.dismiss();
            if (response) {
              await isLogged();
              // Registro login FirebaseAnalytics.
              await FirebaseAnalytics.instance
                  .setCurrentScreen(screenName: 'login_page');
              await FirebaseAnalytics.instance
                  .logSignUp(signUpMethod: _txtCompania.text);
              if (globals.role == Roles.gerente ||
                  globals.role == Roles.supervisor) {
                final result = await Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (context) => TabBarComponent(
                      currentPageIndex: 5,
                    ),
                  ),
                  (Route route) => false,
                ) as bool;
                if (result) {
                  print('Termino de hacer la navegacion a TabBarComponent');
                }
              } else {
                final result = await Navigator.pushAndRemoveUntil(
                  context,
                  MaterialPageRoute(
                    builder: (context) => TabBarComponent(
                      currentPageIndex: 0,
                    ),
                  ),
                  (Route route) => false,
                ) as bool;
                if (result) {
                  print('Termino de hacer la navegacion a TabBarComponent');
                }
              }
            } else {
              await FirebaseAnalytics.instance.logEvent(
                name: 'failedLogin',
                parameters: {
                  "company": _txtCompania.text,
                  'phoneNumber': _txtNumeroTelefono.text,
                },
              );
              final snackBar = SnackBar(
                content:
                    Text('Numero de Telefono o Contraseña son incorrectos'),
              );
              ScaffoldMessenger.of(context).showSnackBar(snackBar);
            }
          } catch (e) {
            _handler.dismiss();
            final snackBar = SnackBar(
              content: Text('Ocurrio un error al iniciar sesion'),
            );
            print(e);
            ScaffoldMessenger.of(context).showSnackBar(snackBar);
          }
        } else {
          _handler.dismiss();
          final snackBar = SnackBar(
            content: Text('La compañia es incorrecta.'),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }
      } else {
        _handler.dismiss();
        final snackBar = SnackBar(
          content: Text('La compañia es incorrecta.'),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
      }
    } catch (e) {
      print('Ocurrio un error al iniciar sesion $e');
      _handler.dismiss();
      final snackBar = SnackBar(
        content: Text('Ocurrio un error al iniciar sesion'),
      );
      print(e);
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }
}

class UnderlineLoginTitle extends StatelessWidget {
  const UnderlineLoginTitle({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
        top: 12.0,
        bottom: 25.0,
      ),
      child: Container(
        width: double.infinity,
        height: 2,
        color: Colors.redAccent,
        margin: const EdgeInsets.only(right: 4),
      ),
    );
  }
}

class Logo extends StatelessWidget {
  const Logo({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width * .2,
      height: MediaQuery.of(context).size.height * .15,
      // decoration: BoxDecoration(
      //   image: DecorationImage(
      //     image: AssetImage("assets/images/logo.png"),
      //     fit: BoxFit.cover,
      //     colorFilter: ColorFilter.mode(
      //       Colors.white.withOpacity(0.30),
      //       BlendMode.darken,
      //     ),
      //   ),
      // ),
      child: Image.asset(
        'assets/images/logo.png',
        width: 100.0,
        //height: 50,
        fit: BoxFit.fitHeight,
      ),
    );
  }
}

class RegisterPageRoute extends CupertinoPageRoute {
  RegisterPageRoute()
      : super(builder: (BuildContext context) => new RegisterPage());

  // OPTIONAL IF YOU WISH TO HAVE SOME EXTRA ANIMATION WHILE ROUTING
  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    return new FadeTransition(opacity: animation, child: new RegisterPage());
  }
}
