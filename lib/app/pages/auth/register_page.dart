import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:alcorp_app/app/core/services/auth.service.dart';
import 'package:alcorp_app/app/core/services/enterprise.service.dart';
import 'package:alcorp_app/app/shared/widgets/button.widget.dart';
import 'package:alcorp_app/app/shared/widgets/modal_rounded_progress.widget.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'login_page.dart';

class RegisterPage extends StatefulWidget {
  RegisterPage({Key key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  TextEditingController _txtNumeroEmpleado = new TextEditingController();
  TextEditingController _txtContrasena = new TextEditingController();
  TextEditingController _txtCompania = new TextEditingController();
  TextEditingController _txtConfirmarContrasena = new TextEditingController();
  TextEditingController _txtEmail = new TextEditingController();
  TextEditingController _txtTelefono = new TextEditingController();
  TextEditingController _txtNombre = new TextEditingController();
  TextEditingController _txtApellidoPaterno = new TextEditingController();
  TextEditingController _txtApellidoMaterno = new TextEditingController();
  ProgressBarHandler _handler;
  bool _obscureText = true;
  final _formGlobalKey = GlobalKey<FormState>();
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  // Toggles the password show status
  void _toggle() {
    setState(() {
      _obscureText = !_obscureText;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Stack(
        children: [
          //SizedBox(height: 25.0),
          Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/bckg-login.png"),
                //image: SvgPicture.asset("assets/images/bckg-login.svg"),
                fit: BoxFit.fitHeight,
                colorFilter: ColorFilter.mode(
                  Colors.black.withOpacity(0.30),
                  BlendMode.darken,
                ),
              ),
            ),
            //child: Image.asset('assets/images/login-bg.jpg', ),
          ),
          Column(
            mainAxisSize: MainAxisSize.min,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: Container(
                  padding: EdgeInsets.only(
                      top: MediaQuery.of(context).size.width * .20),
                  child: Text(
                    "¡BIENVENIDO!",
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 25.0,
                      fontWeight: FontWeight.w700,
                    ),
                  ),
                ),
              ),
            ],
          ),
          // Card principal
          Container(
            margin: EdgeInsets.only(
              top: MediaQuery.of(context).size.height * .2,
              bottom: 0,
            ),
            decoration: BoxDecoration(
              color: Color(0xfffafafa),
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(50.0),
                topRight: Radius.circular(50.0),
              ),
            ),
            child: Padding(
              padding: EdgeInsets.only(left: 25.0, right: 20.0),
              child: Container(
                height: MediaQuery.of(context).size.height,
                child: Container(
                  child: ListView(
                    children: [
                      Center(
                        child: Text(
                          "REGISTRO",
                          style: TextStyle(
                            color: Colors.redAccent,
                            fontSize: 28,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                      //Underline login title
                      UnderlineLoginTitle(),
                      // Form login
                      Form(
                        key: _formGlobalKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 12.0,
                                left: 20.0,
                                right: 20.0,
                                bottom: 12.0,
                              ),
                              child: Material(
                                borderRadius: BorderRadius.circular(15.0),
                                elevation: 8.0,
                                //shadowColor: Colors.white,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'La compañia es obligatoria.';
                                    }
                                    return null;
                                  },
                                  keyboardType: TextInputType.text,
                                  controller: _txtCompania,
                                  decoration: InputDecoration(
                                    suffixIcon: Icon(
                                      Icons.business_rounded,
                                      color: Color(0xffb2302a),
                                      size: 30,
                                    ),
                                    hintText: 'Compañia',
                                    fillColor: Color(0xfffffdfd),
                                    hintStyle: TextStyle(
                                      color: Colors.redAccent,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                        width: 2.0,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 12.0,
                                left: 20.0,
                                right: 20.0,
                                bottom: 12.0,
                              ),
                              child: Material(
                                borderRadius: BorderRadius.circular(15.0),
                                elevation: 8.0,
                                //shadowColor: Colors.white,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'El nombre es obligatorio.';
                                    }
                                    return null;
                                  },
                                  keyboardType: TextInputType.text,
                                  controller: _txtNombre,
                                  decoration: InputDecoration(
                                    suffixIcon: Icon(
                                      Icons.person_outline_rounded,
                                      color: Color(0xffb2302a),
                                      size: 30,
                                    ),
                                    hintText: 'Nombre',
                                    fillColor: Color(0xfffffdfd),
                                    hintStyle: TextStyle(
                                      color: Colors.redAccent,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                        width: 2.0,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 12.0,
                                left: 20.0,
                                right: 20.0,
                                bottom: 12.0,
                              ),
                              child: Material(
                                borderRadius: BorderRadius.circular(15.0),
                                elevation: 8.0,
                                //shadowColor: Colors.white,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'El Apellido Paterno es obligatorio.';
                                    }
                                    return null;
                                  },
                                  keyboardType: TextInputType.text,
                                  controller: _txtApellidoPaterno,
                                  decoration: InputDecoration(
                                    suffixIcon: Icon(
                                      Icons.person_outline_rounded,
                                      color: Color(0xffb2302a),
                                      size: 30,
                                    ),
                                    hintText: 'Apellido Paterno',
                                    fillColor: Color(0xfffffdfd),
                                    hintStyle: TextStyle(
                                      color: Colors.redAccent,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                        width: 2.0,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 12.0,
                                left: 20.0,
                                right: 20.0,
                                bottom: 12.0,
                              ),
                              child: Material(
                                borderRadius: BorderRadius.circular(15.0),
                                elevation: 8.0,
                                //shadowColor: Colors.white,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'El Apellido Materno es obligatorio.';
                                    }
                                    return null;
                                  },
                                  keyboardType: TextInputType.text,
                                  controller: _txtApellidoMaterno,
                                  decoration: InputDecoration(
                                    suffixIcon: Icon(
                                      Icons.person_outline_rounded,
                                      color: Color(0xffb2302a),
                                      size: 30,
                                    ),
                                    hintText: 'Apellido Materno',
                                    fillColor: Color(0xfffffdfd),
                                    hintStyle: TextStyle(
                                      color: Colors.redAccent,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                        width: 2.0,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 12.0,
                                left: 20.0,
                                right: 20.0,
                                bottom: 12.0,
                              ),
                              child: Material(
                                borderRadius: BorderRadius.circular(15.0),
                                elevation: 8.0,
                                //shadowColor: Colors.white,
                                child: TextFormField(
                                  validator: (value) {
                                    if (value.isEmpty) {
                                      return 'El numero de empleado es obligatorio.';
                                    }
                                    return null;
                                  },
                                  keyboardType: TextInputType.numberWithOptions(
                                      signed: true, decimal: true),
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.allow(
                                      RegExp(r'[0-9]'),
                                    ),
                                  ],
                                  controller: _txtNumeroEmpleado,
                                  decoration: InputDecoration(
                                    suffixIcon: Icon(
                                      Icons.person_outline_rounded,
                                      color: Color(0xffb2302a),
                                      size: 30,
                                    ),
                                    hintText: 'Numero de empleado',
                                    fillColor: Color(0xfffffdfd),
                                    hintStyle: TextStyle(
                                      color: Colors.redAccent,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(15.0),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                        width: 2.0,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 6.0,
                                left: 20.0,
                                right: 20.0,
                                bottom: 12.0,
                              ),
                              child: Material(
                                borderRadius: BorderRadius.circular(15.0),
                                elevation: 8.0,
                                //shadowColor: Colors.white,
                                child: TextFormField(
                                  // validator: (value) {
                                  //   if (value.isEmpty) {
                                  //     return 'La Contraseña es obligatoria.';
                                  //   }
                                  //   return null;
                                  // },
                                  validator: (val) => val.length < 6
                                      ? 'La contraseña es demasiada corta.'
                                      : null,
                                  onSaved: (val) => val = val,
                                  keyboardType: TextInputType.text,
                                  controller: _txtContrasena,
                                  obscureText: _obscureText,
                                  decoration: InputDecoration(
                                    suffixIcon: GestureDetector(
                                      onTap: () {
                                        setState(
                                            () => _obscureText = !_obscureText);
                                      },
                                      child: Icon(
                                        Icons.remove_red_eye_outlined,
                                        color: Color(0xffb2302a),
                                        size: 30,
                                      ),
                                    ),
                                    hintText: 'Contraseña',
                                    fillColor: Color(0xfffffdfd),
                                    hintStyle: TextStyle(
                                      color: Colors.redAccent,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(
                                        15.0,
                                      ),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(
                                        15.0,
                                      ),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                        width: 2.0,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 6.0,
                                left: 20.0,
                                right: 20.0,
                                bottom: 12.0,
                              ),
                              child: Material(
                                borderRadius: BorderRadius.circular(15.0),
                                elevation: 8.0,
                                //shadowColor: Colors.white,
                                child: TextFormField(
                                  // validator: (value) {
                                  //   if (value.isEmpty) {
                                  //     return 'La Contraseña es obligatoria.';
                                  //   }
                                  //   return null;
                                  // },
                                  validator: (val) => val.length < 6
                                      ? 'Confirmar contrasena es demasiada corta'
                                      : null,
                                  onSaved: (val) => val = val,
                                  keyboardType: TextInputType.text,
                                  controller: _txtConfirmarContrasena,
                                  obscureText: _obscureText,
                                  decoration: InputDecoration(
                                    suffixIcon: GestureDetector(
                                      onTap: () {
                                        setState(
                                            () => _obscureText = !_obscureText);
                                      },
                                      child: Icon(
                                        Icons.remove_red_eye_outlined,
                                        color: Color(0xffb2302a),
                                        size: 30,
                                      ),
                                    ),
                                    hintText: 'Confirmar Contraseña',
                                    fillColor: Color(0xfffffdfd),
                                    hintStyle: TextStyle(
                                      color: Colors.redAccent,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(
                                        15.0,
                                      ),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(
                                        15.0,
                                      ),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                        width: 2.0,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 6.0,
                                left: 20.0,
                                right: 20.0,
                                bottom: 12.0,
                              ),
                              child: Material(
                                borderRadius: BorderRadius.circular(15.0),
                                elevation: 8.0,
                                //shadowColor: Colors.white,
                                child: TextFormField(
                                  // validator: (value) {
                                  //   if (value.isEmpty) {
                                  //     return 'La Contraseña es obligatoria.';
                                  //   }
                                  //   return null;
                                  // },
                                  validator: (val) => val.length < 6
                                      ? 'El correo es demasiado corto'
                                      : null,
                                  onSaved: (val) => val = val,
                                  keyboardType: TextInputType.text,
                                  controller: _txtEmail,
                                  decoration: InputDecoration(
                                    suffixIcon: GestureDetector(
                                      onTap: () {},
                                      child: Icon(
                                        Icons.email_outlined,
                                        color: Color(0xffb2302a),
                                        size: 30,
                                      ),
                                    ),
                                    hintText: 'Correo electrónico',
                                    fillColor: Color(0xfffffdfd),
                                    hintStyle: TextStyle(
                                      color: Colors.redAccent,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(
                                        15.0,
                                      ),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(
                                        15.0,
                                      ),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                        width: 2.0,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 6.0,
                                left: 20.0,
                                right: 20.0,
                                bottom: 12.0,
                              ),
                              child: Material(
                                borderRadius: BorderRadius.circular(15.0),
                                elevation: 8.0,
                                //shadowColor: Colors.white,
                                child: TextFormField(
                                  validator: (val) => val.length < 10
                                      ? 'El número de telefono es demasiado corto'
                                      : null,
                                  onSaved: (val) => val = val,
                                  keyboardType: TextInputType.numberWithOptions(
                                      signed: true, decimal: true),
                                  inputFormatters: <TextInputFormatter>[
                                    FilteringTextInputFormatter.allow(
                                      RegExp(r'[0-9]'),
                                    ),
                                  ],
                                  controller: _txtTelefono,
                                  decoration: InputDecoration(
                                    suffixIcon: Icon(
                                      Icons.phone,
                                      color: Color(0xffb2302a),
                                      size: 30,
                                    ),
                                    hintText: 'Número de Telefono',
                                    fillColor: Color(0xfffffdfd),
                                    hintStyle: TextStyle(
                                      color: Colors.redAccent,
                                      fontWeight: FontWeight.bold,
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(
                                        15.0,
                                      ),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                      ),
                                    ),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(
                                        15.0,
                                      ),
                                      borderSide: BorderSide(
                                        color: Colors.white,
                                        width: 2.0,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                top: 6.0,
                                left: 20.0,
                                right: 20.0,
                                bottom: 16.0,
                              ),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    "¿Ya tienes cuenta?",
                                    style: TextStyle(
                                      color: Color(0xffb7b7b7),
                                      fontWeight: FontWeight.normal,
                                    ),
                                  ),
                                  GestureDetector(
                                    onTap: () async {
                                      Navigator.pop(context);
                                      // if (result) {
                                      //   print(
                                      //       'Termino de hacer la navegacion a LoginPage');
                                      // }
                                    },
                                    child: Text(
                                      " Inicia sesión",
                                      style: TextStyle(
                                        color: CustomColors.primaryColor,
                                        fontWeight: FontWeight.normal,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Center(
                              child: ButtonTheme(
                                minWidth: 170.0,
                                height: 45.0,
                                child: RaisedButton(
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  color: CustomColors.primaryColor,
                                  onPressed: () async {
                                    if (_formGlobalKey.currentState
                                        .validate()) {
                                      // use the information provided
                                      _handler.show();
                                      await register();
                                    }
                                  },
                                  child: Text(
                                    "Registrarse",
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: Colors.white,
                                      fontSize: 20.0,
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          ModalRoundedProgressBar(
            //getting the handler
            // ignore: missing_return
            handleCallback: (handler) {
              _handler = handler;
            },
          )
        ],
      ),
    );
  }

  showAlertDialog(BuildContext context) {
    // show the dialog

    // show the dialog
    showDialog(
      context: _scaffoldKey.currentContext,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.transparent,
          insetPadding: EdgeInsets.all(10.0),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * .2,
                child: SvgPicture.asset('assets/icons/check.svg'),
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(
                    "Usuario registrado\ncorrectamente",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(15.0),
                child: PrimaryButton(
                  onPressed: () async {
                    try {
// Navigator.of(context).pop();
                      final result = await Navigator.pushAndRemoveUntil(
                        context,
                        MaterialPageRoute(
                          builder: (context) => LoginPage(),
                        ),
                        (Route route) => false,
                      ) as bool;
                      // if (result) {
                      // Add Event forgotPassword.
                      await FirebaseAnalytics.instance.logEvent(
                        name: 'register',
                        parameters: {
                          "email": _txtEmail.text,
                          "employeeNumber": _txtNumeroEmpleado.text,
                          "phoneNumber": _txtTelefono.text,
                        },
                      );

                      print('Termino de hacer la navegacion a TabBarComponent');
                      // }

                    } catch (e) {
                      print(e);
                      throw e;
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Continuar",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  minWidth: MediaQuery.of(context).size.width * .4,
                  height: 35,
                ),
              )
            ],
          ),
        );
      },
    ).then(
      (value) => (exit) {
        if (exit == null) return;

        if (exit) {
          // user pressed Yes button
        } else {
          // user pressed No button
        }
      },
    );
  }

  void register() async {
    final String appName = _txtCompania.text;
    final enterprisesList = await EnterpriseService.getEnterprise(appName);
    if (enterprisesList != null) {
      final apiUrl = enterprisesList
          .where((element) => element.appName.toUpperCase() == 'METRICS')
          .single
          .apiUrl;
      if (!apiUrl.isEmpty) {
        try {
          final prefs = await SharedPreferences.getInstance();
          prefs.setString('apiUrl', apiUrl);
        } catch (e) {}
      }
    } else {
      _handler.dismiss();
      final snackBar = SnackBar(
        content: Text('La compañia es incorrecta'),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
      return;
    }

    try {
      if (_txtContrasena.text != _txtConfirmarContrasena.text) {
        _handler.dismiss();
        final snackBar = SnackBar(
          content: Text('Las contraseñas no coinciden'),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        return;
      } else {
        var response = await AuthService.register(
          _txtNumeroEmpleado.text,
          _txtContrasena.text,
          _txtEmail.text,
          _txtTelefono.text,
          _txtNombre.text,
          _txtApellidoPaterno.text,
          _txtApellidoMaterno.text,
        );

        _handler.dismiss();

        if (response) {
          showAlertDialog(context);
        } else {
          final snackBar = SnackBar(
            content: Text('Codigo de empleado incorrecto'),
          );
          ScaffoldMessenger.of(context).showSnackBar(snackBar);
        }
      }
    } catch (e) {
      _handler.dismiss();
      final snackBar = SnackBar(
        content: Text(e),
      );
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }
}

// return Scaffold(
//     resizeToAvoidBottomInset: true,
//     body: Stack(
//       children: [
//         Container(
//           height: MediaQuery.of(context).size.height * .45,
//           decoration: BoxDecoration(
//             borderRadius: BorderRadius.only(
//               bottomLeft: Radius.elliptical(40, 30),
//               bottomRight: Radius.elliptical(40, 30),
//             ),
//             image: DecorationImage(
//               alignment: Alignment.center,
//               image: AssetImage("assets/images/bckg-login.png"),
//               fit: BoxFit.fitHeight,
//               colorFilter: ColorFilter.mode(
//                 Colors.black.withOpacity(0.30),
//                 BlendMode.darken,
//               ),
//             ),
//           ),
//           //child: Image.asset('assets/images/login-bg.jpg', ),
//         ),
//         Center(
//           child: Column(
//             mainAxisAlignment: MainAxisAlignment.center,
//             crossAxisAlignment: CrossAxisAlignment.center,
//             children: [
//               Container(
//                 height: MediaQuery.of(context).size.height * .45,
//                 width: MediaQuery.of(context).size.width * .75,
//                 child: Card(
//                   elevation: 3,
//                   shape: RoundedRectangleBorder(
//                     borderRadius: BorderRadius.circular(20.0),
//                   ),
//                   child: Column(
//                     children: [
//                       Center(
//                         child: Padding(
//                           padding:
//                               const EdgeInsets.only(top: 16.0, bottom: 5),
//                           child: Text(
//                             'Registro',
//                             style: TextStyle(
//                               fontSize: 22.0,
//                               color: CustomColors.greyColor,
//                               fontWeight: FontWeight.bold,
//                             ),
//                           ),
//                         ),
//                       ),
//                       Padding(
//                         padding: const EdgeInsets.only(
//                           top: 5.0,
//                           bottom: 25.0,
//                           left: 35,
//                           right: 35,
//                         ),
//                         child: Container(
//                           width: double.infinity,
//                           height: 2,
//                           color: Colors.redAccent,
//                           //margin: const EdgeInsets.only(right: 15, left: 15),
//                         ),
//                       ),
//                       Form(
//                         child: Column(
//                           crossAxisAlignment: CrossAxisAlignment.start,
//                           children: [
//                             Padding(
//                               padding: const EdgeInsets.all(15.0),
//                               child: Theme(
//                                 data: ThemeData(
//                                   primaryColor: Colors.red,
//                                   accentColor: Colors.orange,
//                                   hintColor: Colors.green,
//                                 ),
//                                 child: TextField(
//                                   decoration: InputDecoration(
//                                     border: UnderlineInputBorder(
//                                       borderSide:
//                                           BorderSide(color: Colors.red),
//                                     ),
//                                     labelText: "Número de empleado",
//                                     labelStyle: TextStyle(
//                                       color: CustomColors.greyColor,
//                                     ),
//                                     prefixIcon:
//                                         Icon(Icons.person_outline_rounded),
//                                   ),
//                                 ),
//                               ),
//                             )
//                           ],
//                         ),
//                       ),
//                     ],
//                   ),
//                 ),
//               ),
//             ],
//           ),
//         )
//       ],
//     ),
//   );
