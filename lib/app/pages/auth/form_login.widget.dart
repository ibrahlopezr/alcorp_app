// import 'package:alcorp_app/app/core/services/auth.service.dart';
// import 'package:alcorp_app/app/core/custom/colors.dart';
// import 'package:alcorp_app/app/pages/menu/menu_page.dart';
// import 'package:alcorp_app/app/shared/widgets/tab_component.dart';
// import 'package:flutter/material.dart';
// import 'package:jwt_decode/jwt_decode.dart';
// import 'package:shared_preferences/shared_preferences.dart';

// class FormLogin extends StatefulWidget {
//   FormLogin({Key key}) : super(key: key);

//   @override
//   _FormLogin createState() => _FormLogin();
// }

// class _FormLogin extends State<FormLogin> {
//   TextEditingController _txtUsername = new TextEditingController();
//   TextEditingController _txtPassword = new TextEditingController();

//   @override
//   void initState() {
//     super.initState();
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Form(
//       child: Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: [
//           Padding(
//             padding: const EdgeInsets.only(
//               top: 12.0,
//               left: 20.0,
//               right: 20.0,
//               bottom: 12.0,
//             ),
//             child: Material(
//               borderRadius: BorderRadius.circular(15.0),
//               elevation: 8.0,
//               //shadowColor: Colors.white,
//               child: TextFormField(
//                 validator: (value) {
//                   if (value.isEmpty) {
//                     return 'El Usuario es obligatorio.';
//                   }
//                   return null;
//                 },
//                 keyboardType: TextInputType.text,
//                 controller: _txtUsername,
//                 decoration: InputDecoration(
//                   suffixIcon: Icon(
//                     Icons.person_outline_rounded,
//                     color: Color(0xffb2302a),
//                     size: 30,
//                   ),
//                   hintText: 'Usuario',
//                   fillColor: Color(0xfffffdfd),
//                   hintStyle: TextStyle(
//                     color: Colors.redAccent,
//                     fontWeight: FontWeight.bold,
//                   ),
//                   focusedBorder: OutlineInputBorder(
//                     borderRadius: BorderRadius.circular(15.0),
//                     borderSide: BorderSide(
//                       color: Colors.white,
//                     ),
//                   ),
//                   enabledBorder: OutlineInputBorder(
//                     borderRadius: BorderRadius.circular(15.0),
//                     borderSide: BorderSide(
//                       color: Colors.white,
//                       width: 2.0,
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//           ),
//           Padding(
//             padding: const EdgeInsets.only(
//               top: 6.0,
//               left: 20.0,
//               right: 20.0,
//               bottom: 12.0,
//             ),
//             child: Material(
//               borderRadius: BorderRadius.circular(15.0),
//               elevation: 8.0,
//               //shadowColor: Colors.white,
//               child: TextFormField(
//                 validator: (value) {
//                   if (value.isEmpty) {
//                     return 'La Contraseña es obligatoria.';
//                   }
//                   return null;
//                 },
//                 keyboardType: TextInputType.text,
//                 controller: _txtPassword,
//                 decoration: InputDecoration(
//                   suffixIcon: Icon(
//                     Icons.remove_red_eye_outlined,
//                     color: Color(0xffb2302a),
//                     size: 30,
//                   ),
//                   hintText: 'Contraseña',
//                   fillColor: Color(0xfffffdfd),
//                   hintStyle: TextStyle(
//                     color: Colors.redAccent,
//                     fontWeight: FontWeight.bold,
//                   ),
//                   focusedBorder: OutlineInputBorder(
//                     borderRadius: BorderRadius.circular(15.0),
//                     borderSide: BorderSide(
//                       color: Colors.white,
//                     ),
//                   ),
//                   enabledBorder: OutlineInputBorder(
//                     borderRadius: BorderRadius.circular(15.0),
//                     borderSide: BorderSide(
//                       color: Colors.white,
//                       width: 2.0,
//                     ),
//                   ),
//                 ),
//               ),
//             ),
//           ),
//           Padding(
//             padding: const EdgeInsets.only(
//               top: 6.0,
//               left: 20.0,
//               right: 20.0,
//               bottom: 6.0,
//             ),
//             child: Row(
//               mainAxisAlignment: MainAxisAlignment.end,
//               children: [
//                 Text(
//                   "¿Olvidaste tu contraseña?",
//                   style: TextStyle(
//                       color: Color(0xffb7b7b7), fontWeight: FontWeight.normal),
//                 ),
//               ],
//             ),
//           ),
//           Padding(
//             padding: const EdgeInsets.only(
//               top: 12.0,
//               left: 20.0,
//               right: 20.0,
//             ),
//             child: Column(
//               mainAxisAlignment: MainAxisAlignment.center,
//               children: [
//                 Center(
//                   child: ButtonTheme(
//                     minWidth: 170.0,
//                     height: 45.0,
//                     child: RaisedButton(
//                       shape: RoundedRectangleBorder(
//                         borderRadius: BorderRadius.circular(10.0),
//                       ),
//                       color: CustomColors.primaryColor,
//                       onPressed: () async {
//                         await login();
//                       },
//                       child: Text(
//                         "LOGIN",
//                         style: TextStyle(
//                           fontWeight: FontWeight.bold,
//                           color: Colors.white,
//                           fontSize: 20.0,
//                         ),
//                       ),
//                     ),
//                   ),
//                 ),
//               ],
//             ),
//           ),
//         ],
//       ),
//     );
//   }

//   void login() async {
//     var response =
//         await AuthService.login(_txtUsername.text, _txtPassword.text);
//     if (response) {
//       final result = await Navigator.pushAndRemoveUntil(
//         context,
//         MaterialPageRoute(
//           builder: (context) => TabBarComponent(
//             currentPageIndex: 0,
//           ),
//         ),
//         (Route route) => false,
//       ) as bool;
//       if (result) {
//         print('Termino de hacer la navegacion a TabBarComponent');
//       }
//     }
//   }


// }
