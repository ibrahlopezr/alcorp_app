import 'dart:convert';

import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:alcorp_app/app/core/models/route.model.dart';
import 'package:alcorp_app/app/pages/auth/login_page.dart';
import 'package:alcorp_app/app/shared/utils/global.utils.dart';
import 'package:alcorp_app/app/shared/widgets/button.widget.dart';
import 'package:alcorp_app/app/shared/widgets/tab_component.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ModulesPage extends StatefulWidget {
  ModulesPage({Key key}) : super(key: key);

  @override
  State<ModulesPage> createState() => _ModulesPageState();
}

class _ModulesPageState extends State<ModulesPage> {
  Future<List<Routes>> _getRoutesFuture;

  @override
  void initState() {
    super.initState();
    this._getRoutesFuture = this.getRoutes();
  }

  Future<List<Routes>> getRoutes() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<Routes> routes = jsonDecode(prefs.getString('routes'))
        .map<Routes>((json) => Routes.fromJson(json))
        .toList();
    // List<Routes> routes = (jsonEncode(prefs.getString('routes')) as List<dynamic>).map((e) => e.toString());
    return routes;
  }

  static Route<void> _myRouteBuilder(BuildContext context, Object arguments) {
    return MaterialPageRoute<void>(
      builder: (BuildContext context) => LoginPage(),
    );
  }

  Container loading(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 50.0,
                width: 50.0,
                child: CircularProgressIndicator(
                  color: CustomColors.primaryColor,
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          alignment: Alignment.center,
          children: [
            ListView(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(14.0),
                  child: ListTile(
                    title: Text(
                      'Modulos',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 24.0,
                      ),
                    ),
                  ),
                ),
                // ListTile(
                //   leading: Icon(Icons.notifications),
                //   title: Text(
                //     "Notificaciones",
                //     style: TextStyle(
                //       color: CustomColors.greyColor,
                //       fontWeight: FontWeight.bold,
                //     ),
                //   ),
                // ),
                FutureBuilder<List<Routes>>(
                    future: this._getRoutesFuture,
                    // initialData: true,
                    builder: (_, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done) {
                        if (snapshot.data != null) {
                          // globals.voteGroup = false;
                          return Column(
                            children: snapshot.data.map((e) {
                              return ListTile(
                                leading: Icon(Icons.folder_shared),
                                title: Text(
                                  e.nombre,
                                  style: TextStyle(
                                    color: CustomColors.greyColor,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                                onTap: () async {
                                  await Navigator.pushNamed(
                                    context,
                                    e.ruta,
                                  );
                                },
                              );
                            }).toList(),
                          );
                        } else {
                          //globals.voteGroup = true;
                          return Container();
                        }
                      } else if (snapshot.connectionState ==
                              ConnectionState.waiting &&
                          !snapshot.hasError) {
                        return loading(context);
                      } else {
                        return Center(
                          child: Text(
                            'Ocurrio un error al obtener la informacion\nIntente de nuevo mas tarde.',
                            style: TextStyle(fontSize: 16.0),
                          ),
                        );
                      }
                    }),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
