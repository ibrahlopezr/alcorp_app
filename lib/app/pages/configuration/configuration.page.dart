import 'dart:convert';

import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:alcorp_app/app/core/models/route.model.dart';
import 'package:alcorp_app/app/core/services/auth.service.dart';
import 'package:alcorp_app/app/pages/auth/login_page.dart';
import 'package:alcorp_app/app/pages/configuration/configuration_builder.page.dart';
import 'package:alcorp_app/app/pages/configuration/default.page.dart';
import 'package:alcorp_app/app/routes/app_pages.dart' as AppRoutes;
import 'package:alcorp_app/app/shared/widgets/button.widget.dart';
import 'package:alcorp_app/app/shared/widgets/tab_component.dart';
import 'package:alcorp_app/app/ui/android/news/news.page.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ConfigurationPage extends StatefulWidget {
  ConfigurationPage({Key key}) : super(key: key);
  // final void Function(int) onAddButtonTapped;

  @override
  _ConfigurationPageState createState() => _ConfigurationPageState();
}

class _ConfigurationPageState extends State<ConfigurationPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: Container(
        color: Colors.white,
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Stack(
          alignment: Alignment.center,
          children: [
            ListView(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(14.0),
                  child: ListTile(
                    title: Center(
                      child: Text(
                        'Configuraciones',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 24.0,
                        ),
                      ),
                    ),
                  ),
                ),
                ListTile(
                  leading: Icon(Icons.notifications),
                  title: Text(
                    "Notificaciones",
                    style: TextStyle(
                      color: CustomColors.greyColor,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                ListTile(
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => NewsPage(),
                      ),
                    );
                  },
                  leading: Icon(Icons.notifications),
                  title: Text(
                    "Noticias",
                    style: TextStyle(
                      color: CustomColors.greyColor,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
              ],
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.only(
                    bottom: MediaQuery.of(context).size.width * .05,
                  ),
                  child: Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: LogOutButton(context),
                  ),
                )
              ],
            )
          ],
        ),
      ),
    );
  }

  PrimaryButton LogOutButton(BuildContext context) {
    return PrimaryButton(
      minWidth: 50,
      height: 45,
      onPressed: () async {
        await AuthService.logOut(context);
      },
      child: Text(
        'Cerrar sesion',
        style: TextStyle(color: Colors.white),
      ),
    );
  }
}
