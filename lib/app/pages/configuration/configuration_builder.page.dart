import 'dart:convert';

import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:alcorp_app/app/core/models/route.model.dart';
import 'package:alcorp_app/app/pages/Configuration/Configuration.page.dart';
import 'package:alcorp_app/app/pages/auth/login_page.dart';
import 'package:alcorp_app/app/pages/configuration/modules.page.dart';
import 'package:alcorp_app/app/pages/configuration/default.page.dart';
import 'package:alcorp_app/app/pages/home/home_page.dart';
import 'package:alcorp_app/app/pages/home/weekly_menu.page.dart';
import 'package:alcorp_app/app/shared/widgets/button.widget.dart';
import 'package:alcorp_app/app/shared/widgets/tab_component.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ConfigurationPageBuilder extends StatefulWidget {
  ConfigurationPageBuilder({Key key}) : super(key: key);

  @override
  _ConfigurationPageBuilderState createState() =>
      _ConfigurationPageBuilderState();
}

class _ConfigurationPageBuilderState extends State<ConfigurationPageBuilder> {
  @override
  Widget build(BuildContext context) {
    return Navigator(
      initialRoute: 'Modules',
      onGenerateRoute: (RouteSettings settings) {
        var route = settings.name;
        switch (route) {
          case '/Modules':
            return MaterialPageRoute(
              builder: (context) => ModulesPage(),
              settings: settings,
            );
            break;
          case 'Modules':
            return MaterialPageRoute(
              builder: (context) => ModulesPage(),
              settings: settings,
            );
            break;
          default:
            return MaterialPageRoute(
              builder: (context) => DefaultPage(),
              settings: settings,
            );
        }
      },
    );
  }
}
