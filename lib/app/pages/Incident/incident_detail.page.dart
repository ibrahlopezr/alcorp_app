import 'dart:math';

import 'package:alcorp_app/app/shared/widgets/modal_rounded_progress.widget.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:alcorp_app/app/core/models/incidencia.modelo.dart';
import 'package:alcorp_app/app/core/models/respuesta.model.dart';
import 'package:alcorp_app/app/core/services/auth.service.dart';
import 'package:alcorp_app/app/core/services/incidencia.service.dart';
import 'package:alcorp_app/app/shared/utils/backgroun.painter.dart';
import 'package:alcorp_app/app/shared/widgets/button.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
    FlutterLocalNotificationsPlugin();

class IncidentDetailPage extends StatefulWidget {
  IncidentDetailPage({Key key, this.incidencia}) : super(key: key);
  final Incidencia incidencia;
  @override
  _IncidentDetailPageState createState() =>
      _IncidentDetailPageState(incidencia: this.incidencia);
}

class _IncidentDetailPageState extends State<IncidentDetailPage> {
  Incidencia incidencia;
  TextEditingController txtRespuesta = TextEditingController();
  _IncidentDetailPageState({@required this.incidencia});
  ProgressBarHandler _handler = new ProgressBarHandler();

  @override
  void initState() {
    super.initState();
    tz.initializeTimeZones();
    tz.setLocalLocation(tz.getLocation('America/Phoenix'));
    var initializationSettingsAndroid =
        AndroidInitializationSettings('@mipmap/ic_launcher');
    var initializationSettingsIOS = IOSInitializationSettings();
    var initializationSettings = InitializationSettings(
      android: initializationSettingsAndroid,
      iOS: initializationSettingsIOS,
    );
    flutterLocalNotificationsPlugin.initialize(
      initializationSettings,
    );
  }

  double width = 0;
  double height = 0;

  @override
  Widget build(BuildContext context) {
    this.incidencia = ModalRoute.of(context).settings.arguments as Incidencia;
    width = MediaQuery.of(context).size.width;
    height = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: SvgPicture.asset(
            'assets/icons/arrow-left.svg',
            color: Colors.white,
          ),
          onPressed: () => Navigator.of(context).pop(),
        ),
      ),
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: CustomPaint(
              painter: BackgroundPainter(),
            ),
          ),
          SingleChildScrollView(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 25.0),
                      child: Center(
                        child: Text(
                          'Comunica tu problema\no incidencia',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 28.0,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      margin: EdgeInsets.only(
                        top: MediaQuery.of(context).size.height * .10,
                        bottom: MediaQuery.of(context).size.height * .10,
                      ),
                      width: MediaQuery.of(context).size.width * .8,
                      height: MediaQuery.of(context).size.height * .3,
                      child: Card(
                        color: Colors.grey[800],
                        elevation: 16,
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10.0),
                        ),
                        child: Column(
                          children: [
                            Expanded(
                              flex: 1,
                              child: Container(
                                height: MediaQuery.of(context).size.height * .1,
                                color: Colors.grey[800],
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    Flexible(
                                      child: Container(
                                        padding:
                                            const EdgeInsets.only(left: 15.0),
                                        child: Center(
                                          child: Text(
                                            incidencia?.titulo != null
                                                ? incidencia?.titulo
                                                : 'Otro',
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                              fontSize: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .045,
                                              fontWeight: FontWeight.bold,
                                              color: Colors.white,
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Expanded(
                              flex: 3,
                              child: Container(
                                height: MediaQuery.of(context).size.height * .1,
                                color: Colors.white,
                                child: Padding(
                                  padding: const EdgeInsets.all(10.0),
                                  child: TextFormField(
                                    controller: txtRespuesta,
                                    validator: (value) {
                                      if (value == null || value.isEmpty) {
                                        return "Ingrese primero un mensaje";
                                      }
                                      return null;
                                    },
                                    maxLines: 8,
                                    decoration: new InputDecoration(
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                            color: CustomColors.greyColor,
                                            width: 1.0),
                                      ),
                                      enabledBorder: OutlineInputBorder(
                                        borderSide: BorderSide(
                                          color: CustomColors.greyColor,
                                          width: 1.0,
                                        ),
                                      ),
                                      hintText: 'Escribe tu mensaje...',
                                      hintStyle: TextStyle(
                                        fontSize: 12.0,
                                        color: CustomColors.greyColor,
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    PrimaryButton(
                      onPressed: () async {
                        if (txtRespuesta.text == null ||
                            txtRespuesta.text.isEmpty) {
                          return;
                        }
                        _handler.show();
                        await agregarIncidencia(context);
                        _handler.dismiss();
                      },
                      child: Text(
                        'Enviar',
                        style: TextStyle(
                          color: Colors.white,
                        ),
                      ),
                      minWidth: MediaQuery.of(context).size.width * .4,
                      height: 50,
                    ),
                  ],
                )
              ],
            ),
          ),
          SizedBox(
            width: width,
            height: height,
            child: ModalRoundedProgressBar(
              //getting the handler
              // ignore: missing_return
              handleCallback: (handler) {
                _handler = handler;
              },
              message: "Enviando incidencia...",
            ),
          )
        ],
      ),
    );
  }

  Future<void> agregarIncidencia(BuildContext context) async {
    int userId = await AuthService.getCurrentUserId();
    final respuesta = new Respuesta(
      descripcion: txtRespuesta.text,
      incidenciaId: incidencia.id,
      usuarioId: userId,
    );
    final response = await IncidenciaService.addRespuestaIncidencia(
      respuesta,
    );

    if (response.statusCode == 200) {
      print("Incidencia enviada correctamente.");

      //Programación de una notificación
      scheduleNotification();

      txtRespuesta.text = "";
      final snackBar =
          SnackBar(content: Text('Incidencia enviada correctamente!'));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    } else {
      print("Ocurrio un error al enviar la incidencia.");
      final snackBar =
          SnackBar(content: Text('Ocurrio un error al enviar la incidencia!'));
      ScaffoldMessenger.of(context).showSnackBar(snackBar);
    }
  }

  void scheduleNotification() async {
    var rng = new Random();
    final minutes = rng.nextInt(100);
    final username = await AuthService.getCurrentUserName();
    await flutterLocalNotificationsPlugin.zonedSchedule(
      0,
      'Alcorp Metrics',
      '¡Hola $username! tu incidencia esta siendo atendida.',
      tz.TZDateTime.now(tz.local).add(Duration(minutes: minutes)),
      const NotificationDetails(
        iOS: IOSNotificationDetails(),
        android: AndroidNotificationDetails(
          'alcorp_metrics',
          'Alcorp Metrics',
          'incidencias',
        ),
      ),
      androidAllowWhileIdle: true,
      uiLocalNotificationDateInterpretation:
          UILocalNotificationDateInterpretation.absoluteTime,
      payload: 'This is notification detail Text...',
    );
  }
}
