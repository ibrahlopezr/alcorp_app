import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:alcorp_app/app/core/models/incidencia.modelo.dart';
import 'package:alcorp_app/app/core/models/respuesta.model.dart';
import 'package:alcorp_app/app/core/services/incidencia.service.dart';
import 'package:alcorp_app/app/pages/Incident/incident_detail.page.dart';
import 'package:alcorp_app/app/shared/utils/backgroun.painter.dart';
import 'package:alcorp_app/app/shared/widgets/tab_component.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class IncidentPage extends StatefulWidget {
  IncidentPage({Key key}) : super(key: key);
  // final void Function(int) onAddButtonTapped;

  @override
  _IncidentPageState createState() => _IncidentPageState();
}

class _IncidentPageState extends State<IncidentPage> {
  List<Incidencia> lstIncidencia = new List<Incidencia>()
    ..add(Incidencia(id: 1, titulo: "Problemas en como recibi mi comida"))
    ..add(Incidencia(id: 2, titulo: "No me gusta el trato del comedor"))
    ..add(Incidencia(id: 3, titulo: "¡Felicidades muy buen comedor!"))
    ..add(Incidencia(id: 4, titulo: "Otro"));

  Future<List<Incidencia>> _getIncidenciasFuture;
  List<Incidencia> _incidencias;

  @override
  void initState() {
    super.initState();
    this._getIncidenciasFuture = this.getIncidencias();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: CustomColors.primaryColor,
        elevation: 0,
        leading: IconButton(
          icon: SvgPicture.asset(
            'assets/icons/x.svg',
            color: Colors.white,
          ),
          onPressed: () => {},
        ),
      ),
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: CustomPaint(
              painter: BackgroundPainter(),
            ),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 25.0),
                      child: Center(
                        child: Text(
                          'Comunica tu problema\no incidencia',
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 28.0,
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(
                    top: MediaQuery.of(context).size.height * .1,
                  ),
                  child: FutureBuilder<List<Incidencia>>(
                    future: _getIncidenciasFuture,
                    builder: (_, snapshot) {
                      if (snapshot.connectionState == ConnectionState.done &&
                          snapshot.data != null) {
                        return _buildCardsForIncidents(
                          context,
                          snapshot.data,
                        );
                      } else if (snapshot.connectionState ==
                              ConnectionState.waiting &&
                          !snapshot.hasError) {
                        return loading(context);
                      } else {
                        return Center(
                          child: Text(
                            'Ocurrio un error al obtener las Incidencias\nIntente de nuevo mas tarde.',
                            style: TextStyle(fontSize: 16.0),
                          ),
                        );
                      }
                    },
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget _buildCardsForIncidents(
      BuildContext context, List<Incidencia> incidencias) {
    if (incidencias.length > 0) {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          GestureDetector(
            onTap: () async {
              if (incidencias[0] != null) {
                await goToDetails(context, incidencias[0]);
              }
            },
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 5.0),
              decoration: BoxDecoration(boxShadow: <BoxShadow>[
                BoxShadow(
                  color: CustomColors.greyColor.withOpacity(0.5),
                  blurRadius: 25,
                  offset: Offset(0, 2),
                ),
              ]),
              width: MediaQuery.of(context).size.width * .8,
              height: MediaQuery.of(context).size.height * .1,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                color: Colors.white,
                elevation: 16,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 9,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Center(
                              child: Text(
                                incidencias[0].titulo,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: CustomColors.greyColor,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 15.0),
                            child: SvgPicture.asset(
                              'assets/icons/arrow-right.svg',
                              width: 20.0,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () async {
              if (incidencias[1] != null) {
                await goToDetails(context, incidencias[1]);
              }
            },
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 5.0),
              decoration: BoxDecoration(boxShadow: <BoxShadow>[
                BoxShadow(
                  color: CustomColors.greyColor.withOpacity(0.5),
                  blurRadius: 25,
                  offset: Offset(0, 2),
                ),
              ]),
              width: MediaQuery.of(context).size.width * .8,
              height: MediaQuery.of(context).size.height * .1,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                color: Colors.white,
                elevation: 16,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 9,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Center(
                              child: Text(
                                incidencias[1].titulo,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: CustomColors.greyColor,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 15.0),
                            child: SvgPicture.asset(
                              'assets/icons/arrow-right.svg',
                              width: 20.0,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () async {
              if (incidencias[2] != null) {
                await goToDetails(context, incidencias[2]);
              }
            },
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 5.0),
              decoration: BoxDecoration(boxShadow: <BoxShadow>[
                BoxShadow(
                  color: CustomColors.greyColor.withOpacity(0.5),
                  blurRadius: 25,
                  offset: Offset(0, 2),
                ),
              ]),
              width: MediaQuery.of(context).size.width * .8,
              height: MediaQuery.of(context).size.height * .1,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                color: Colors.white,
                elevation: 16,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 9,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Center(
                              child: Text(
                                incidencias[2].titulo,
                                textAlign: TextAlign.center,
                                style: TextStyle(
                                  color: CustomColors.greyColor,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 15.0),
                            child: SvgPicture.asset(
                              'assets/icons/arrow-right.svg',
                              width: 20.0,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          GestureDetector(
            onTap: () async {
              if (incidencias[3] != null) {
                await goToDetails(context, incidencias[3]);
              }
            },
            child: Container(
              margin: EdgeInsets.symmetric(vertical: 5.0),
              decoration: BoxDecoration(boxShadow: <BoxShadow>[
                BoxShadow(
                  color: CustomColors.greyColor.withOpacity(0.5),
                  blurRadius: 25,
                  offset: Offset(0, 2),
                ),
              ]),
              width: MediaQuery.of(context).size.width * .8,
              height: MediaQuery.of(context).size.height * .1,
              child: Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10.0),
                ),
                color: Colors.white,
                elevation: 16,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Expanded(
                      flex: 9,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              incidencias[3].titulo,
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                color: CustomColors.greyColor,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      flex: 1,
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(right: 15.0),
                            child: SvgPicture.asset(
                              'assets/icons/arrow-right.svg',
                              width: 20.0,
                            ),
                          ),
                        ],
                      ),
                    )
                  ],
                ),
              ),
            ),
          )
        ],
      );
    } else {
      return Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 50.0,
              width: 50.0,
              child: CircularProgressIndicator(
                color: CustomColors.primaryColor,
              ),
            )
          ],
        ),
      );
    }
  }

  Container loading(BuildContext context) {
    return Container(
      height: MediaQuery.of(context).size.height * .5,
      width: MediaQuery.of(context).size.width * .5,
      child: Center(
        child: Container(
          width: 50,
          height: 50,
          child: CircularProgressIndicator(
            color: CustomColors.primaryColor,
          ),
        ),
      ),
    );
  }

  Future<void> goToDetails(BuildContext context, Incidencia incidencia) async {
    final result = await Navigator.pushNamed(
      context,
      'IncidentsDetail',
      arguments: incidencia,
    ) as bool;
    if (result == true) {
      print('Termino de hacer la navegacion a IncidentsDetailPage');
    }
  }

  Future<List<Incidencia>> getIncidencias() async {
    //if (ltsDiasGroup.length == 0) {
    final data = await IncidenciaService.getIncidencias();
    // setState(() => );
    return this._incidencias = data;
    //}
  }
}
