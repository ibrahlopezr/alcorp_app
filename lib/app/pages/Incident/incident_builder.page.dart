import 'package:alcorp_app/app/pages/Incident/incident.page.dart';
import 'package:alcorp_app/app/pages/Incident/incident_detail.page.dart';
import 'package:alcorp_app/app/pages/home/home_page.dart';
import 'package:alcorp_app/app/pages/home/weekly_menu.page.dart';
import 'package:flutter/material.dart';

class IncidentPageBuilder extends StatefulWidget {
  IncidentPageBuilder({Key key}) : super(key: key);

  @override
  _IncidentPageBuilderState createState() => _IncidentPageBuilderState();
}

class _IncidentPageBuilderState extends State<IncidentPageBuilder> {
  @override
  Widget build(BuildContext context) {
    return Navigator(
      initialRoute: 'Incidents',
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case 'Incidents':
            return MaterialPageRoute(
                builder: (context) => IncidentPage(), settings: settings);
            break;
          case 'IncidentsDetail':
            return MaterialPageRoute(
                builder: (context) => IncidentDetailPage(), settings: settings);
            break;

          default:
            throw Exception("Invalid route");
        }
      },
    );
  }
}
