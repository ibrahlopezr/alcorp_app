import 'dart:convert';
import 'dart:ui';

import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:alcorp_app/app/core/models/data_item.model.dart';
import 'package:alcorp_app/app/core/models/participaciones.model.dart';
import 'package:alcorp_app/app/core/models/platillo.model.dart';
import 'package:alcorp_app/app/core/models/platillo_sugeridos.model.dart';
import 'package:alcorp_app/app/core/models/sugerencia.model.dart';
import 'package:alcorp_app/app/core/models/votacion.model.dart';
import 'package:alcorp_app/app/core/services/menu.service.dart';
import 'package:alcorp_app/app/core/services/votacion.service.dart';
import 'package:alcorp_app/app/shared/utils/global.utils.dart';
import 'package:alcorp_app/app/shared/widgets/button.widget.dart';
import 'package:alcorp_app/app/shared/widgets/circle_dot.widget.dart';
import 'package:alcorp_app/app/shared/widgets/circular_progress_bar.widget.dart';
import 'package:alcorp_app/app/shared/widgets/donut_chart.widget.dart';
import 'package:alcorp_app/app/shared/widgets/info_title.widget.dart';
import 'package:alcorp_app/app/shared/widgets/line_progress_bar.widget.dart';
import 'package:alcorp_app/app/shared/widgets/participant_chart.widget.dart';
import 'package:alcorp_app/app/shared/widgets/suggested_menu_progress_line.widget.dart';
import 'package:alcorp_app/app/shared/widgets/tab_component.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

const pal = [0xFFF2387C, 0xFF05C7F2, 0xFF04D9C4, 0xFFF2B705, 0xFFF26241];

class WeeklyMenuPage extends StatefulWidget {
  WeeklyMenuPage({Key key}) : super(key: key);

  @override
  _WeeklyMenuPageState createState() => _WeeklyMenuPageState();
}

class _WeeklyMenuPageState extends State<WeeklyMenuPage> {
  Future<List<Platillo>> _getVotingFuture;
  Future<Map<String, dynamic>> _getDashboardFuture;
  List<Platillo> platillosList;
  int _currentDay = 0;
  int _diasTotales = 0;
  Votacion _votacionActual;
  bool _inVoting = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final List<DataItem> dataset = [
    DataItem(0.2, "Comedy", Color(pal[0])),
    DataItem(0.25, "Action", Color(pal[1])),
    DataItem(0.3, "Romance", Color(pal[2])),
    DataItem(0.05, "Drama", Color(pal[3])),
    DataItem(0.2, "SciFi", Color(pal[4])),
  ];

  TextEditingController txtSugerencia = new TextEditingController();

  double _currentWidth = 0;
  double _currentHeight = 0;

  List<PlatillosSugeridos> mostRecommended;

  List<PlatillosSugeridos> mostSuggested;

  Participaciones participantes;

  @override
  void initState() {
    super.initState();
    this._getVotingFuture = this.getVoting();
    this._getDashboardFuture = this.getDashboard();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _currentWidth = MediaQuery.of(context).size.width;
    _currentHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      key: _scaffoldKey,
      appBar: CustomAppBar(),
      body: LayoutBuilder(
        builder: (BuildContext context, BoxConstraints constraints) {
          return FutureBuilder(
            future: Future.wait([
              this._getVotingFuture,
              this._getDashboardFuture,
            ]),
            builder: (context, snapshot) {
              print('maxHeight: ${constraints.maxHeight}');
              final imageSize = constraints.maxHeight >= 480 ? .30 : .15;
              if (snapshot.connectionState == ConnectionState.done &&
                  platillosList != null &&
                  platillosList.length > 0) {
                return _buildCards(context, platillosList, imageSize);
              } else if (snapshot.connectionState == ConnectionState.waiting &&
                  !snapshot.hasError) {
                return Center(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Container(
                        height: 50.0,
                        width: 50.0,
                        child: CircularProgressIndicator(
                          color: CustomColors.primaryColor,
                        ),
                      ),
                      Text(
                        _currentDay > 0 ? "Cargando la siguiente votacion" : "",
                        style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 18.0,
                        ),
                      ),
                    ],
                  ),
                );
              } else if (snapshot.connectionState == ConnectionState.done &&
                  snapshot.hasError) {
                return Center(
                  child: Text(
                    'Ocurrio un error al obtener la informacion\nIntente de nuevo mas tarde.',
                    style: TextStyle(fontSize: 16.0),
                  ),
                );
              } else if (snapshot.connectionState == ConnectionState.done &&
                  participantes != null &&
                  mostRecommended != null &&
                  mostSuggested != null) {
                return graphics(context);
              } else {
                return Center(
                  child: Text(
                    'Ocurrio un error al obtener la informacion\nIntente de nuevo mas tarde.',
                    style: TextStyle(fontSize: 16.0),
                  ),
                );
              }
            },
          );
        },
      ),
    );
  }

  Widget graphics(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Expanded(
          flex: 1,
          child: Center(
            child: SizedBox(
              width: _currentWidth * .9,
              height: _currentHeight * .38,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25),
                  ),
                  color: Colors.white,
                  elevation: 0.5,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Padding(
                        padding: const EdgeInsets.all(25.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    'Total de participantes',
                                    style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 14.0,
                                      color: Colors.black54,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    participantes.totalParticipantes
                                            .toString() ??
                                        "0",
                                    style: TextStyle(
                                      fontWeight: FontWeight.w800,
                                      fontSize: 28.0,
                                      color: Colors.black87,
                                    ),
                                  ),
                                )
                              ],
                            ),
                            CircularProgressBar(
                              width: MediaQuery.of(context).size.width * .2,
                              height: MediaQuery.of(context).size.width * .2,
                              padding: EdgeInsets.all(5.0),
                              value: participantes.totalRestantesPorcentaje,
                            ),
                          ],
                        ),
                      ),
                      ParticipantChart(
                        text: "Participantes pendientes: ",
                        participants:
                            participantes.totalRestantes.toString() ?? "",
                        value: participantes.totalRestantesPorcentaje ?? 0,
                        color: CustomColors.primaryColor,
                      ),
                      ParticipantChart(
                        text: "Participantes que sugirieron: ",
                        participants:
                            participantes.totalParticipaciones.toString() ?? "",
                        value:
                            participantes.totalParticipacionesPorcentaje ?? 0,
                        color: CustomColors.primaryColorDark,
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ),
        Expanded(
          flex: 1,
          child: Center(
            child: SizedBox(
              width: _currentWidth * .9,
              height: _currentHeight * .38,
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Card(
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(25),
                  ),
                  color: Colors.white,
                  elevation: 0.5,
                  child: Column(
                    children: [
                      InfoTitle(title: 'Platillos mas sugeridos'),
                      ...mostRecommended.map(
                        (item) => SuggestedMenuProgressLine(
                          porcentaje: "${item.percentage}%",
                          text: item.name,
                          value: item.value,
                        ),
                      ),
                      InfoTitle(title: 'Platillos mas recomendados'),
                      ...mostSuggested.map(
                        (item) => SuggestedMenuProgressLine(
                          porcentaje: "${item.percentage}%",
                          text: item.name,
                          value: item.value,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        )
      ],
    );
  }

  Column _buildProgressBar(BuildContext context, String dia) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Text(
            dia.toUpperCase(),
            style: TextStyle(
              color: CustomColors.greyColor.withOpacity(.75),
              fontWeight: FontWeight.w900,
              fontSize: 18.0,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(5.0),
          child: Container(
            width: MediaQuery.of(context).size.width * .8,
            height: 10.0,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              child: LinearProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                  CustomColors.primaryColor,
                ),
                value: (_currentDay / _diasTotales),
                backgroundColor: CustomColors.primaryColorDark,
                color: CustomColors.primaryColor,
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildCards(
    BuildContext context,
    List<Platillo> platillos,
    double imageSize,
  ) {
    return Stack(
      children: [
        Column(
          children: [
            _buildProgressBar(context, platillos[0].dia),
            _renderCards(context, imageSize, platillos),
            _finalButton()
          ],
        ),
        Visibility(
          visible: _inVoting,
          child: Container(
            color: Colors.black38,
            height: double.infinity,
            width: double.infinity,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 50.0,
                    width: 50.0,
                    child: CircularProgressIndicator(
                      color: CustomColors.primaryColor,
                    ),
                  ),
                  Text(
                    "Cargando los resultados",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Widget _finalButton() {
    return Container(
      decoration: BoxDecoration(
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: CustomColors.greyColor.withOpacity(0.15),
            blurRadius: 55,
            offset: Offset(5, 5),
          ),
        ],
      ),
      child: ButtonTheme(
        child: RaisedButton(
          elevation: 0,
          colorBrightness: Brightness.light,
          highlightColor: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(45.0),
          ),
          color: Colors.white,
          onPressed: () {
            return Dialog(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(15.0),
              ),
              backgroundColor: Colors.black38,
              child: _dialogContent(),
            );
          },
          child: Text(
            "Ninguno de los anteriores",
            style: TextStyle(
              color: CustomColors.greyColor,
              fontSize: 12.0,
            ),
          ),
        ),
      ),
    );
  }

  Widget _dialogContent() {
    // Create button

    // show the dialog
    showDialog(
      context: _scaffoldKey.currentContext,
      builder: (BuildContext context) {
        return AlertDialog(
          backgroundColor: Colors.transparent,
          insetPadding: EdgeInsets.all(10.0),
          content: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Center(
                child: Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(
                    "¿Algún platillo a sugerir?",
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Colors.white,
                      fontSize: 20.0,
                    ),
                  ),
                ),
              ),
              ClipRRect(
                borderRadius: BorderRadius.all(Radius.circular(7.0)),
                child: Material(
                  elevation: 0,
                  shape: BeveledRectangleBorder(
                    borderRadius: BorderRadius.all(Radius.circular(7.0)),
                  ),
                  child: Container(
                    width: MediaQuery.of(context).size.width * .65,
                    color: Colors.white,
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: TextFormField(
                        controller: txtSugerencia,
                        validator: (value) {
                          if (value == null || value.isEmpty) {
                            return "Ingrese primero una sugerencia";
                          }
                          return null;
                        },
                        maxLines: 1,
                        decoration: new InputDecoration(
                          focusedBorder: OutlineInputBorder(
                            borderSide:
                                BorderSide(color: Colors.grey[200], width: 0.2),
                          ),
                          enabledBorder: OutlineInputBorder(
                            borderSide: BorderSide(
                              color: CustomColors.greyColor,
                              width: 1.0,
                            ),
                          ),
                          hintText: 'Escriba aquí...',
                          hintStyle: TextStyle(
                            fontSize: 12.0,
                            color: CustomColors.greyColor,
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ),
              Container(
                padding: EdgeInsets.all(10.0),
                child: PrimaryButton(
                  onPressed: () async {
                    if (txtSugerencia.text.isNotEmpty) {
                      Navigator.pop(context, false);
                      try {
                        var model = new Sugerencia(
                          nombre: txtSugerencia.text,
                          id: 0,
                          usuarioCreacion: 0,
                          usuarioModificacion: 0,
                        );
                        int result = await MenuService.addSugerencia(model);
                        txtSugerencia.text = '';
                        if (result > 0) {
                          final snackBar = SnackBar(
                            content: Text(
                              'Sugerencia agregada correctamente.',
                            ),
                          );
                          ScaffoldMessenger.of(_scaffoldKey.currentContext)
                              .showSnackBar(snackBar);
                        } else {
                          final snackBar = SnackBar(
                            content: Text(
                              'Ocurrio un error al agregar la sugerencia.',
                            ),
                          );
                          ScaffoldMessenger.of(_scaffoldKey.currentContext)
                              .showSnackBar(snackBar);
                        }
                      } catch (e) {
                        final snackBar = SnackBar(
                          content: Text(
                            'Ocurrio un error al agregar la sugerencia.',
                          ),
                        );
                        ScaffoldMessenger.of(_scaffoldKey.currentContext)
                            .showSnackBar(snackBar);
                      }
                    }
                  },
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Enviar",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                  minWidth: MediaQuery.of(context).size.width * .30,
                  height: 35,
                ),
              )
            ],
          ),
        );
      },
    ).then(
      (value) => (exit) {
        if (exit == null) return;

        if (exit) {
          // user pressed Yes button
        } else {
          // user pressed No button
        }
      },
    );
  } //Your dialog view

  Widget _renderCards(
    BuildContext context,
    double imageSize,
    List<Platillo> platillos,
  ) {
    var size = MediaQuery.of(context).size;
    final double itemHeight = (size.height - kToolbarHeight - 24) * .38;
    final double itemWidth = size.width * .5;
    List<Widget> list = platillos
        .map(
          (e) => Padding(
            padding: const EdgeInsets.only(
              left: 15.0,
              right: 15.0,
              top: 5.0,
              bottom: 10.0,
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              child: Material(
                elevation: 0,
                shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                child: Container(
                  color: CustomColors.primaryColor,
                  child: Column(
                    children: [
                      Expanded(
                          flex: 4,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Center(
                                    child: SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                          imageSize,
                                      child: e.image != null && e.image != ''
                                          ? Image.network(
                                              e.image,
                                              loadingBuilder:
                                                  (BuildContext context,
                                                      Widget child,
                                                      ImageChunkEvent
                                                          loadingProgress) {
                                                if (loadingProgress == null)
                                                  return child;
                                                return Center(
                                                  child:
                                                      CircularProgressIndicator(
                                                    color:
                                                        CustomColors.greyColor,
                                                    value: loadingProgress
                                                                .expectedTotalBytes !=
                                                            null
                                                        ? loadingProgress
                                                                .cumulativeBytesLoaded /
                                                            loadingProgress
                                                                .expectedTotalBytes
                                                        : null,
                                                  ),
                                                );
                                              },
                                            )
                                          : SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .1,
                                              child: Image.asset(
                                                'assets/images/hamburguesa.png',
                                              ),
                                            ),
                                    ),
                                  ),
                                  Center(
                                    child: FittedBox(
                                      fit: BoxFit.fitWidth,
                                      child: Text(
                                        e.titulo.split(' ').join('\n'),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .045,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )
                            ],
                          )),
                      Visibility(
                        visible: e.votado,
                        child: Expanded(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(5),
                                  ),
                                  child: Container(
                                    color: CustomColors.primaryColorDark
                                        .withOpacity(0.35),
                                    child: Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child: Text(
                                        e.porcentaje.toString() + "%",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .025,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * .25,
                                // width: MediaQuery.of(context).size.width * imageSize,
                                //    height:
                                //         MediaQuery.of(context).size.width *
                                //             .1,
                                child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                  child: LinearProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.white),
                                    value: e.porcentaje / 100,
                                    backgroundColor:
                                        CustomColors.primaryColorDark,
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Visibility(
                        visible: !e.votado,
                        child: Expanded(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              GestureDetector(
                                onTap: () async {
                                  print('Votar por cuarto platillo');
                                  final idPlatillo = e.id;
                                  final dia = e.dia;
                                  final idResult = await realizarVotacion(
                                    idPlatillo,
                                    dia: dia,
                                  );
                                  sleepPageAfterReload();
                                },
                                child: ClipRRect(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                  ),
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width * .22,
                                    height:
                                        MediaQuery.of(context).size.width * .1,
                                    color: Colors.white,
                                    child: Center(
                                        child: Text(
                                      'Sugerir',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: CustomColors.greyColor,
                                      ),
                                    )),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ),
        )
        .toList();
    // list.insert(list.length - 1, _finalButton());
    return Expanded(
      flex: 1,
      child: GridView.count(
        crossAxisCount: 2,
        childAspectRatio: (itemWidth / itemHeight),
        children: list,
      ),
    );
  }

  Widget RedCard(
      BuildContext context, double imageSize, List<Platillo> platillos) {
    return Expanded(
      flex: 1,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.only(
              left: 5.0,
              right: 15.0,
              top: 15.0,
              bottom: 15.0,
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              child: Material(
                elevation: 10.0,
                shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                child: Container(
                  color: CustomColors.primaryColor,
                  width: MediaQuery.of(context).size.width * .4,
                  height: MediaQuery.of(context).size.height * .4,
                  child: Column(
                    children: [
                      Expanded(
                          flex: 4,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Center(
                                    child: SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                          imageSize,
                                      child: platillos[0].image != null &&
                                              platillos[0].image != ''
                                          ? Image.network(
                                              platillos[0].image,
                                              loadingBuilder:
                                                  (BuildContext context,
                                                      Widget child,
                                                      ImageChunkEvent
                                                          loadingProgress) {
                                                if (loadingProgress == null)
                                                  return child;
                                                return Center(
                                                  child:
                                                      CircularProgressIndicator(
                                                    color:
                                                        CustomColors.greyColor,
                                                    value: loadingProgress
                                                                .expectedTotalBytes !=
                                                            null
                                                        ? loadingProgress
                                                                .cumulativeBytesLoaded /
                                                            loadingProgress
                                                                .expectedTotalBytes
                                                        : null,
                                                  ),
                                                );
                                              },
                                            )
                                          : SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .1,
                                              child: Image.asset(
                                                'assets/images/hamburguesa.png',
                                              ),
                                            ),
                                    ),
                                  ),
                                  Center(
                                    child: FittedBox(
                                      fit: BoxFit.fitWidth,
                                      child: Text(
                                        platillos[0]
                                            .titulo
                                            .split(' ')
                                            .join('\n'),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )
                            ],
                          )),
                      Visibility(
                        visible: platillos[0].votado,
                        child: Expanded(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(5),
                                  ),
                                  child: Container(
                                    color: CustomColors.primaryColorDark
                                        .withOpacity(0.35),
                                    child: Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child: Text(
                                        platillos[0].porcentaje.toString() +
                                            "%",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .025,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * .25,
                                // width: MediaQuery.of(context).size.width * imageSize,
                                //    height:
                                //         MediaQuery.of(context).size.width *
                                //             .1,
                                child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                  child: LinearProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.white),
                                    value: platillos[0].porcentaje / 100,
                                    backgroundColor:
                                        CustomColors.primaryColorDark,
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Visibility(
                        visible: !platillos[0].votado,
                        child: Expanded(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              GestureDetector(
                                onTap: () async {
                                  print('Votar por primer platillo');
                                  final idPlatillo = platillos[0].id;
                                  final dia = platillos[0].dia;
                                  await realizarVotacion(idPlatillo, dia: dia);
                                  sleepPageAfterReload();
                                },
                                child: ClipRRect(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                  ),
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width * .22,
                                    height:
                                        MediaQuery.of(context).size.width * .1,
                                    color: Colors.white,
                                    child: Center(
                                      child: Text(
                                        'Sugerir',
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: CustomColors.greyColor,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 15.0,
              right: 5.0,
              top: 15.0,
              bottom: 15.0,
            ),
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(20)),
              child: Material(
                elevation: 10.0,
                shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.all(Radius.circular(10.0)),
                ),
                child: Container(
                  color: CustomColors.primaryColor,
                  width: MediaQuery.of(context).size.width * .4,
                  height: MediaQuery.of(context).size.height * .4,
                  child: Column(
                    children: [
                      Expanded(
                          flex: 4,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  Center(
                                    child: SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                          imageSize,
                                      child: platillos[1]?.image != null &&
                                              platillos[1].image != ''
                                          ? Image.network(
                                              platillos[1]?.image,
                                              loadingBuilder:
                                                  (BuildContext context,
                                                      Widget child,
                                                      ImageChunkEvent
                                                          loadingProgress) {
                                                if (loadingProgress == null)
                                                  return child;
                                                return Center(
                                                  child:
                                                      CircularProgressIndicator(
                                                    color:
                                                        CustomColors.greyColor,
                                                    backgroundColor:
                                                        CustomColors
                                                            .primaryColor,
                                                    value: loadingProgress
                                                                .expectedTotalBytes !=
                                                            null
                                                        ? loadingProgress
                                                                .cumulativeBytesLoaded /
                                                            loadingProgress
                                                                .expectedTotalBytes
                                                        : null,
                                                  ),
                                                );
                                              },
                                            )
                                          : SizedBox(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width *
                                                  .1,
                                              child: Image.asset(
                                                'assets/images/hamburguesa.png',
                                              ),
                                            ),
                                    ),
                                  ),
                                  Center(
                                    child: FittedBox(
                                      fit: BoxFit.fitWidth,
                                      child: Text(
                                        platillos[1]
                                            .titulo
                                            .split(' ')
                                            .join('\n'),
                                        textAlign: TextAlign.center,
                                        style: TextStyle(
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .045,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold,
                                        ),
                                      ),
                                    ),
                                  )
                                ],
                              )
                            ],
                          )),
                      Visibility(
                        visible: platillos[1].votado,
                        child: Expanded(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.start,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: ClipRRect(
                                  borderRadius: BorderRadius.all(
                                    Radius.circular(5),
                                  ),
                                  child: Container(
                                    color: CustomColors.primaryColorDark
                                        .withOpacity(0.35),
                                    child: Padding(
                                      padding: const EdgeInsets.all(4.0),
                                      child: Text(
                                        platillos[1].porcentaje.toString() +
                                            "%",
                                        style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .025,
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width * .25,
                                child: ClipRRect(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(10)),
                                  child: LinearProgressIndicator(
                                    valueColor: AlwaysStoppedAnimation<Color>(
                                        Colors.white),
                                    value: platillos[1].porcentaje / 100,
                                    backgroundColor:
                                        CustomColors.primaryColorDark,
                                    color: Colors.white,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                      Visibility(
                        visible: !platillos[1].votado,
                        child: Expanded(
                          flex: 1,
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: [
                              GestureDetector(
                                onTap: () async {
                                  print('Votar por segundo platillo');
                                  final idPlatillo = platillos[1].id;
                                  final dia = platillos[1].dia;
                                  await realizarVotacion(idPlatillo, dia: dia);
                                  sleepPageAfterReload();
                                },
                                child: ClipRRect(
                                  borderRadius: BorderRadius.only(
                                    topLeft: Radius.circular(20),
                                  ),
                                  child: Container(
                                    width:
                                        MediaQuery.of(context).size.width * .22,
                                    height:
                                        MediaQuery.of(context).size.width * .1,
                                    color: Colors.white,
                                    child: Center(
                                        child: Text(
                                      'Sugerir',
                                      style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: CustomColors.greyColor,
                                      ),
                                    )),
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Future<void> sleepPageAfterReload() async {
    ///Mandamos a llamar el set state para que refresque
    ///y se traiga el otro platillo.

    _inVoting = false;

    this.platillosList = await this.getVoting();

    final resulta = await Future<void>.delayed(
      const Duration(seconds: 5),
      () => setState(
        () {
          _inVoting = false;
        },
      ),
    );
  }

  Future<int> realizarVotacion(int idPlatillo, {String dia = "-1"}) async {
    setState(() {
      _inVoting = true;
    });
    var result = await VotacionService.addVotoPlatillo(idPlatillo);
    //Actualizamos los platillos.
    this.platillosList = await this.getVoting(dia: dia, votaciones: false);
    setState(() {});
    return result;
  }

  Future<List<Platillo>> getPorcentaje({String dia = "-1"}) async {
    try {
      final dias = await VotacionService.getPlatillosParaVotacion(dia: dia);
      final platillos = dias.first.platillos;

      return platillos;
    } catch (e) {
      print('Ocurrio un error: $e');
      throw e;
    }
  }

  Future<List<Platillo>> getVoting({
    String dia = "-1",
    bool votaciones = true,
  }) async {
    if (votaciones) {
      try {
        var dias = await VotacionService.getPlatillosParaVotacion(
            dia: dia, context: context);
        // Obtenemos el dia actual mediante el index.
        final index = dias.indexWhere((element) => !element.votado);
        _currentDay = index + 1;

        /// Validamos que no esten votados todos los dias.
        if (index == -1) {
          // ignore: deprecated_member_use
          final dataEmpty = new List<Platillo>();
          return dataEmpty;
        }
        //Obtenemos los dias totales
        this._diasTotales = dias.length;

        _votacionActual = dias[_currentDay - 1];
        dias[_currentDay - 1].platillos.sort((a, b) => a.id.compareTo(b.id));
        platillosList = dias[_currentDay - 1].platillos;
        _inVoting = false;
        await FirebaseAnalytics.instance
            .logScreenView(screenName: 'Votaciones');
        return dias[_currentDay - 1].platillos;
      } catch (e) {
        print('Ocurrio un error: $e');
        throw e;
      }
    } else {
      try {
        List<Platillo> platillos = await getPorcentaje(dia: dia);
        platillos.forEach((element) {
          element.votado = true;
        });
        platillos.sort((a, b) => a.id.compareTo(b.id));
        platillosList = platillos;
        _inVoting = false;
        await FirebaseAnalytics.instance
            .logScreenView(screenName: 'Votaciones');
        return platillos;
      } catch (e) {
        print('Ocurrio un error al obtener el porcentaje: $e');
        throw e;
      }
    }
  }

  Future<Map<String, dynamic>> getDashboard() async {
    var dashboard = await VotacionService.getDashboard();
    participantes = Participaciones.fromJson(dashboard['participants']);
    mostRecommended = (dashboard['mostRecommended'] as List)
        .map((json) => PlatillosSugeridos.fromJson(json))
        .toList();
    mostSuggested = (dashboard['mostSuggested'] as List)
        .map((json) => PlatillosSugeridos.fromJson(json))
        .toList();

    return dashboard;
  }
}
