import 'package:alcorp_app/app/core/models/roles.model.dart';
import 'package:alcorp_app/app/shared/widgets/other_metrics_button.dart';
import 'package:alcorp_app/app/shared/widgets/weekly_menu_button.dart';

import '../../shared/utils/globals.dart' as globals;
import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:alcorp_app/app/core/models/calificacion_grupo.model.dart';
import 'package:alcorp_app/app/core/models/grupo.model.dart';
import 'package:alcorp_app/app/core/services/auth.service.dart';
import 'package:alcorp_app/app/core/services/grupo.service.dart';
import 'package:alcorp_app/app/core/services/menu.service.dart';
import 'package:alcorp_app/app/shared/widgets/button.widget.dart';
import 'package:alcorp_app/app/shared/widgets/tab_component.dart';
import 'package:flutter/material.dart';

import 'home_page.dart';

class WokTeamPage extends StatefulWidget {
  WokTeamPage({Key key}) : super(key: key);

  @override
  _WokTeamPageState createState() => _WokTeamPageState();
}

class _WokTeamPageState extends State<WokTeamPage> {
  Future<Grupo> _getGrupoFuture;
  Grupo _grupo;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this._getGrupoFuture = this.getGrupo();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: FutureBuilder<Grupo>(
        future: this._getGrupoFuture,
        // initialData: true,
        builder: (_, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.data != null) {
              globals.voteGroup = false;
              return successLoadData(context, snapshot.data);
            } else {
              globals.voteGroup = true;
              return doneData(context);
            }
          } else if (snapshot.connectionState == ConnectionState.waiting &&
              !snapshot.hasError) {
            return loading(context);
          } else {
            return Center(
              child: Text(
                'Ocurrio un error al obtener la informacion\nIntente de nuevo mas tarde.',
                style: TextStyle(fontSize: 16.0),
              ),
            );
          }
        },
      ),
    );
  }

  Container successLoadData(BuildContext context, Grupo grupo) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Column(
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/images/obreros.png"),
                      //image: SvgPicture.asset("assets/images/bckg-login.svg"),
                      fit: BoxFit.fitHeight,
                      colorFilter: ColorFilter.mode(
                          Colors.black.withOpacity(0.30), BlendMode.darken),
                    ),
                  ),
                  //child: Image.asset('assets/images/login-bg.jpg', ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width * .85,
                height: MediaQuery.of(context).size.height * .40,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        child: Opacity(
                          opacity: 0.9,
                          child: Card(
                            elevation: 6,
                            borderOnForeground: true,
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.white, width: 1),
                              borderRadius: BorderRadius.circular(25),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceEvenly,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  FittedBox(
                                    fit: BoxFit.fitHeight,
                                    child: Text(
                                      '¿Cómo calificas a tu equipo de trabajo hoy?',
                                      style: TextStyle(
                                        fontSize: 28,
                                        fontWeight: FontWeight.bold,
                                        color: CustomColors.greyColor,
                                      ),
                                    ),
                                  ),
                                  FittedBox(
                                    fit: BoxFit.fitWidth,
                                    child: Text(
                                      grupo.titulo != null
                                          ? grupo.titulo
                                          : "Equipo",
                                      style: TextStyle(
                                        fontSize: 50,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black87,
                                      ),
                                    ),
                                  ),
                                  Row(
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: GestureDetector(
                                          onTap: () async {
                                            final idEmpleado = await AuthService
                                                .getCurrentUserId();
                                            const value = "bien";
                                            await realizarVotacion(
                                                value, grupo, idEmpleado);
                                          },
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              SizedBox(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    .2,
                                                child: Image.asset(
                                                  'assets/icons/emoji_1.png',
                                                  fit: BoxFit.fitHeight,
                                                ),
                                              ),
                                              Text(
                                                'Bien',
                                                style: TextStyle(
                                                  color: CustomColors.greyColor,
                                                  fontSize: 12.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: GestureDetector(
                                          onTap: () async {
                                            final idEmpleado = await AuthService
                                                .getCurrentUserId();
                                            const value = "muy bien";
                                            await realizarVotacion(
                                                value, grupo, idEmpleado);
                                          },
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              SizedBox(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    .2,
                                                child: Image.asset(
                                                  'assets/icons/emoji_2.png',
                                                  fit: BoxFit.fitHeight,
                                                ),
                                              ),
                                              Text(
                                                'Muy Bien',
                                                style: TextStyle(
                                                  color: CustomColors.greyColor,
                                                  fontSize: 12.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                        flex: 1,
                                        child: GestureDetector(
                                          onTap: () async {
                                            final idEmpleado = await AuthService
                                                .getCurrentUserId();
                                            const value = "excelente";
                                            await realizarVotacion(
                                              value,
                                              grupo,
                                              idEmpleado,
                                            );
                                          },
                                          child: Column(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              SizedBox(
                                                width: MediaQuery.of(context)
                                                        .size
                                                        .width *
                                                    .2,
                                                child: Image.asset(
                                                  'assets/icons/emoji_3.png',
                                                  fit: BoxFit.fitHeight,
                                                ),
                                              ),
                                              Text(
                                                'Excelente',
                                                style: TextStyle(
                                                  color: CustomColors.greyColor,
                                                  fontSize: 12.0,
                                                  fontWeight: FontWeight.bold,
                                                ),
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  )
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Visibility(
                visible:
                    globals.role == Roles.gerente || globals.role == Roles.rh,
                child: OtherMetricsButton(),
              ),
              DayMenuButton(),
            ],
          )
        ],
      ),
    );
  }

  Future<void> realizarVotacion(
      String value, Grupo grupo, int idEmpleado) async {
    var objCalificacion = new CalificacionGrupo(
      id: 0,
      calificacion_cualitativa: value,
      calificacion_cuantitativa: 0,
      id_grupo: grupo.id,
      id_empleado: idEmpleado,
      usuario_creacion: idEmpleado,
      usuario_modificacion: idEmpleado,
      fecha_creacion: new DateTime.now(),
      fecha_modificacion: new DateTime.now(),
    );
    final result = await GrupoService.addCalGrupo(
      objCalificacion,
    );
    if (result > 0) {
      globals.voteGroup = true;
    }
    setState(() {
      this._getGrupoFuture = this.getGrupo();
    });
  }

  Container doneData(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Stack(
        alignment: Alignment.center,
        children: [
          // Background Calificacion de metricas
          //TODO Cambiar el fondo dependiendo del rol del usuario.
          Column(
            children: [
              Expanded(
                flex: 1,
                child: Container(
                  decoration: BoxDecoration(
                    image: DecorationImage(
                      image: AssetImage("assets/images/obreros.png"),
                      //image: SvgPicture.asset("assets/images/bckg-login.svg"),
                      fit: BoxFit.fitHeight,
                      colorFilter: ColorFilter.mode(
                          Colors.black.withOpacity(0.30), BlendMode.darken),
                    ),
                  ),
                  //child: Image.asset('assets/images/login-bg.jpg', ),
                ),
              ),
              Expanded(
                flex: 1,
                child: Container(),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              SizedBox(
                width: MediaQuery.of(context).size.width * .85,
                height: MediaQuery.of(context).size.height * .40,
                child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      flex: 1,
                      child: Container(
                        child: Opacity(
                          opacity: 0.9,
                          child: Card(
                            elevation: 6,
                            borderOnForeground: true,
                            color: Colors.white,
                            shape: RoundedRectangleBorder(
                              side: BorderSide(color: Colors.white, width: 1),
                              borderRadius: BorderRadius.circular(25),
                            ),
                            child: Padding(
                              padding: const EdgeInsets.all(20.0),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: [
                                  FittedBox(
                                    fit: BoxFit.fitHeight,
                                    child: Text(
                                      'Ya has calificado a tu equipo de trabajo\nVuelve mañana',
                                      textAlign: TextAlign.center,
                                      style: TextStyle(
                                        fontSize: 50,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black87,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              Visibility(
                visible:
                    globals.role == Roles.gerente || globals.role == Roles.rh,
                child: OtherMetricsButton(),
              ),
              DayMenuButton(),
            ],
          )
        ],
      ),
    );
  }

  Container loading(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 50.0,
                width: 50.0,
                child: CircularProgressIndicator(
                  color: CustomColors.primaryColor,
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  Future<Grupo> getGrupo() async {
    final data = await GrupoService.getCurrentGrupo();
    return this._grupo = data;
  }
}
