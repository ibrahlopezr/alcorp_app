import 'dart:convert';

import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:alcorp_app/app/core/models/form_fields.model.dart';
import 'package:alcorp_app/app/core/services/forms.service.dart';
import 'package:alcorp_app/app/shared/widgets/button.widget.dart';
import 'package:alcorp_app/app/shared/widgets/modal_rounded_progress.widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart' show rootBundle;
import 'package:json_to_form/json_schema.dart';

import 'package:alcorp_app/app/shared/widgets/tab_component.dart';
import 'package:json_to_form/json_to_form.dart';

class OtherMetricsPage extends StatefulWidget {
  OtherMetricsPage({Key key}) : super(key: key);

  @override
  _OtherMetricsPageState createState() => _OtherMetricsPageState();
}

class _OtherMetricsPageState extends State<OtherMetricsPage> {
  // Feature to load form.
  Future<String> _loadJsonFuture;
  dynamic response;
  ProgressBarHandler _handler;
  bool done = false;

  @override
  void initState() {
    super.initState();
    _loadJsonFuture = loadJson();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: CustomAppBar(),
      body: FutureBuilder(
        future: _loadJsonFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            if (snapshot.hasData && snapshot.data != null) {
              return body(snapshot.data);
            } else {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Icon(
                        Icons.info_outline,
                        size: MediaQuery.of(context).size.width * .25,
                        color: CustomColors.primaryColor,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(25.0),
                      child: FittedBox(
                        fit: BoxFit.fitWidth,
                        child: Text(
                          "Actualmente no hay otras metricas disponibles.",
                          textAlign: TextAlign.center,
                          style: TextStyle(
                            fontSize: 24.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              );
            }
          } else if (snapshot.hasError &&
              snapshot.connectionState == ConnectionState.done) {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Icon(
                      Icons.info_outline,
                      size: MediaQuery.of(context).size.width * .25,
                      color: CustomColors.primaryColor,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(25.0),
                    child: FittedBox(
                      fit: BoxFit.fitWidth,
                      child: Text(
                        "Ocurrio un error al cargar las metricas\nContacte a un administrador.",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 24.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            );
          } else {
            return Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 50.0,
                    width: 50.0,
                    child: CircularProgressIndicator(
                      color: CustomColors.primaryColor,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 25.0),
                    child: Text(
                      "Cargando las metricas",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0,
                      ),
                    ),
                  ),
                ],
              ),
            );
          }
        },
      ),
    );
  }

  /// Methods.
  Widget body(String formString) {
    if (formString == null) {
      return Center(child: Text('No Contents!'));
    }

    return Stack(
      children: [
        Visibility(
          visible: !done,
          child: SingleChildScrollView(
            child: Padding(
              padding: const EdgeInsets.all(15.0),
              child: Column(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(15.0),
                    child: Center(
                      child: Text(
                        'Otras Metricas',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 28.0,
                          color: CustomColors.greyColor,
                        ),
                      ),
                    ),
                  ),
                  CoreForm(
                    form: formString,
                    onChanged: (dynamic data) {
                      this.response = data;
                    },
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 50.0),
                    child: PrimaryButton(
                      onPressed: () async {
                        showAlertDialog(context);
                      },
                      child: Padding(
                        padding: const EdgeInsets.symmetric(vertical: 15.0),
                        child: Text(
                          'Guardar',
                          style: TextStyle(
                            color: Colors.white.withOpacity(0.85),
                            fontFamily: 'Roboto',
                          ),
                        ),
                      ),
                      minWidth: 125,
                      height: 50,
                    ),
                  )
                ],
              ),
            ),
          ),
        ),
        ModalRoundedProgressBar(
          message: "Guardando respuestas...",
          handleCallback: (handler) {
            this._handler = handler;
          },
        ),
        Visibility(
          visible: done,
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Icon(
                    Icons.info_outline,
                    size: MediaQuery.of(context).size.width * .25,
                    color: CustomColors.primaryColor,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    'Ya has contestado un formulario',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Text(
                    'Vuelve de nuevo mas tarde.',
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontSize: 16.0,
                      fontWeight: FontWeight.bold,
                      color: CustomColors.greyColor,
                    ),
                  ),
                ),
              ],
            ),
          ),
        )
      ],
    );
  }

  showAlertDialog(BuildContext context) {
    Widget cancelButton = FlatButton(
      child: Text("Cancelar"),
      onPressed: () {
        Navigator.of(context, rootNavigator: true).pop();
      },
    );

    Widget okButton = FlatButton(
      child: Text("Guardar"),
      onPressed: () async {
        try {
          if (this.response == null) {
            return;
          }
          Navigator.of(context, rootNavigator: true).pop();
          this._handler.show();
          print(this.response.toString());
          final fields = json.encode(this.response).toString();
          var jsonData = json.decode(fields) as List;
          final data = jsonData
              .map<FormFields>((json) => FormFields.fromJson(json))
              .toList();

          final result = await FormService.addResponseDynamicForm(data);
          print('$result');
          this._handler.dismiss();
          setState(() {
            done = result;
          });
        } catch (e) {
          print(e);
          throw e;
        }
      },
    );
    // Create AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Otras metricas"),
      content: Text("¿Esta seguro que desea guardar la informacion?"),
      actions: [cancelButton, okButton],
    );

    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }

  Future<String> loadJson() async {
    try {
      // final jsonString =
      //     await rootBundle.loadString('assets/data/sample_forms.json');
      String jsonString = await FormService.getDynamicForm();
      if (!jsonString.isEmpty) {
        List<dynamic> jsonObj = json.decode(jsonString) ?? '';
        List<Map<String, dynamic>> rawJson = listOfDynamicToMap(jsonObj) ?? [];
        final formString = json.encode(rawJson);
        return formString;
      } else {
        return null;
      }
    } catch (e) {
      print(e);
      throw e;
    }
  }

  List<Map<String, dynamic>> listOfDynamicToMap(List<dynamic> list) {
    List<Map<String, dynamic>> listOfMap = [];
    list.forEach((element) {
      if (element is Map<String, dynamic>) {
        listOfMap.add(element);
      }
    });
    return listOfMap;
  }

  // Future<String> getDynamicForm() async {
  //   final jsonString = await  FormService.getDynamicForm();

  // }

  /// End Methods
}
