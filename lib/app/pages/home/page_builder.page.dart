import 'dart:convert';

import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:alcorp_app/app/core/models/roles.model.dart';
import 'package:alcorp_app/app/pages/home/home_page.dart';
import 'package:alcorp_app/app/pages/home/weekly_menu.page.dart';
import 'package:alcorp_app/app/pages/home/work_team.page.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../shared/utils/globals.dart' as globals;
import 'othe_metrics.page.dart';

class PageBuilder extends StatefulWidget {
  final String route;
  PageBuilder({Key key, this.route}) : super(key: key);

  @override
  _PageBuilderState createState() => _PageBuilderState(route: this.route);
}

class _PageBuilderState extends State<PageBuilder> {
  String route;
  List<String> _routes;
  _PageBuilderState({this.route});
  Future<List<String>> _getRoutesFuture;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    this._getRoutesFuture = this.getRoutes();
  }

  Future<List<String>> getRoutes() async {
    try {
      var prefs = await SharedPreferences.getInstance();
      var jsonRoutes = prefs.getString('routes');
      List<dynamic> routes = jsonDecode(jsonRoutes) as List<dynamic>;
      _routes = routes.map((e) => e.toString()).toList();
      ;
      return _routes;
    } catch (e) {
      print('Error al obtener las rutas: $e');
    }
  }

  Container loading(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height,
      child: Stack(
        alignment: Alignment.center,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 50.0,
                width: 50.0,
                child: CircularProgressIndicator(
                  color: CustomColors.primaryColor,
                ),
              )
            ],
          )
        ],
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return renderPageBuilder(this._routes);
  }

  Navigator renderPageBuilder(List<String> routes) {
    return Navigator(
      initialRoute: route,
      onGenerateRoute: (RouteSettings settings) {
        //TODO: Buscar si tiene permiso para la ruta.
        print('routes: $routes');
        // route = routes.where((route) => route == settings.name).first;
        // List<String> routes = prefs.getString('routes');

        String currentRoute = route == 'WorkTeam' && settings.name == 'home'
            ? route
            : settings.name;
        switch (settings.name) {
          case 'home':
            return MaterialPageRoute(
              builder: (context) => HomePage(),
              settings: settings,
            );
            // if (globals.role == Roles.gerente ||
            //     globals.role == Roles.supervisor) {
            // } else {
            //   return MaterialPageRoute(
            //     builder: (context) => HomePage(),
            //     settings: settings,
            //   );
            // }
            break;
          case 'WorkTeam':
            if (globals.role == Roles.gerente ||
                globals.role == Roles.supervisor) {
              return MaterialPageRoute(
                builder: (context) => WokTeamPage(),
                settings: settings,
              );
            } else {
              return MaterialPageRoute(
                builder: (context) => HomePage(),
                settings: settings,
              );
            }
            break;
          case 'WeeklyMenu':
            return MaterialPageRoute(
              builder: (context) => WeeklyMenuPage(),
              settings: settings,
            );
            break;
          case 'Incidents':
            return MaterialPageRoute(
              builder: (context) => WeeklyMenuPage(),
              settings: settings,
            );
            break;
          case 'IncidentsDetail':
            return MaterialPageRoute(
              builder: (context) => WeeklyMenuPage(),
              settings: settings,
            );
            break;
          case 'OtherMetrics':
            if (globals.role == Roles.gerente || globals.role == Roles.rh) {
              return MaterialPageRoute(
                builder: (context) => OtherMetricsPage(),
                settings: settings,
              );
            } else {
              return MaterialPageRoute(
                builder: (context) => HomePage(),
                settings: settings,
              );
            }
            break;

          default:
            throw Exception("Invalid route");
        }
      },
    );
  }
}
