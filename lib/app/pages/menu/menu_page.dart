import 'package:alcorp_app/app/core/models/data_item.model.dart';
import 'package:alcorp_app/app/core/models/dia.model.dart';
import 'package:alcorp_app/app/core/models/platillo.model.dart';
import 'package:alcorp_app/app/core/services/menu.service.dart';
import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:alcorp_app/app/shared/widgets/donut_chart.widget.dart';
import 'package:alcorp_app/app/shared/widgets/tab_component.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:carousel_slider/carousel_slider.dart';

class MenuPage extends StatefulWidget {
  MenuPage({Key key}) : super(key: key);
  // final void Function(int) onAddButtonTapped;

  @override
  _MenuPageState createState() => _MenuPageState();
}

class _MenuPageState extends State<MenuPage> {
  int _current = 0;
  List<Dia> ltsDiasGroup = [];
  List<Platillo> platillosLunes = [];
  List<Platillo> platillosMartes = [];
  List<Platillo> platillosMiercoles = [];
  List<Platillo> platillosJueves = [];
  List<Platillo> platillosViernes = [];
  Future<List<Dia>> _getMenuFuture;

  @override
  void initState() {
    super.initState();
    this._getMenuFuture = this.getMenu();
  }

  Future<void> initAnalyticsEvents() async {
    await FirebaseAnalytics.instance.setCurrentScreen(screenName: 'Menu');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: FutureBuilder<List<Dia>>(
        future: this._getMenuFuture,
        // initialData: true,
        builder: (_, snapshot) {
          if (snapshot.connectionState == ConnectionState.done &&
              snapshot.data != null) {
            if (snapshot.data.length > 0) {
              return customTabController(context, snapshot.data);
            } else {
              // return DonutChartWidget(dataset);
              return graphics(context);
            }
          } else if (snapshot.connectionState == ConnectionState.waiting &&
              !snapshot.hasError) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Center(
                  child: Container(
                    height: 50.0,
                    width: 50.0,
                    child: CircularProgressIndicator(
                      color: CustomColors.primaryColor,
                    ),
                  ),
                )
              ],
            );
          } else {
            return Center(
              child: Text(
                'Ocurrio un error al obtener la informacion\nIntente de nuevo mas tarde.',
                style: TextStyle(fontSize: 16.0),
              ),
            );
          }
        },
      ),
    );
  }

  Center graphics(BuildContext context) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Icon(
              Icons.info_outline,
              size: MediaQuery.of(context).size.width * .25,
              color: CustomColors.primaryColor,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              'Actualmente no se encuentra registrado un menu semanal',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(
              'Intente de nuevo mas tarde.',
              textAlign: TextAlign.center,
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
                color: CustomColors.greyColor,
              ),
            ),
          ),
        ],
      ),
    );
  }

  DefaultTabController customTabController(
      BuildContext context, List<Dia> currentDays) {
    return DefaultTabController(
      initialIndex: 0,
      length: currentDays.isEmpty ? 0 : currentDays.length,
      child: Padding(
        padding: const EdgeInsets.all(1.0),
        child: Scaffold(
          backgroundColor: Colors.white,
          appBar: AppBar(
            // centerTitle: true,
            backgroundColor: Colors.white,
            elevation: 0,
            bottom: TabBar(
              onTap: (int index) {
                _current = 0;
                setState(() {});
              },
              indicator: UnderlineTabIndicator(
                borderSide:
                    BorderSide(color: CustomColors.primaryColor, width: 1.5),
                insets: EdgeInsets.fromLTRB(68.0, 12, 68.0, 12),
              ),
              indicatorColor: CustomColors.primaryColor,
              labelColor: CustomColors.greyColor,
              unselectedLabelColor: CustomColors.greyColor,
              isScrollable: true,
              tabs: currentDays.isEmpty
                  ? <Widget>[]
                  :

                  //Dias
                  currentDays
                      .map(
                        (e) => FittedBox(
                          fit: BoxFit.fitWidth,
                          child: Tab(
                            child: Text(
                              e.titulo,
                              style: TextStyle(
                                fontSize: 16.0,
                                // color: Colors.black87,
                              ),
                            ),
                          ),
                        ),
                      )
                      .toList(),
            ),
          ),
          body: TabBarView(
            children: currentDays.isEmpty
                ? <Widget>[]
                : currentDays
                    .map((e) => getCardsByDay(context, e.platillos))
                    .toList(),
          ),
        ),
      ),
    );
  }

  Future<List<Dia>> getMenu() async {
    //if (ltsDiasGroup.length == 0) {
    final data = await MenuService.getPlatillosByGroupDay();
    await FirebaseAnalytics.instance.logScreenView(screenName: 'Menu');
    // setState(() => );
    return this.ltsDiasGroup = data;
    //}
  }

  Column getCardsByDay(BuildContext context, List<Platillo> platillos) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        //Text title platillos.
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.only(top: 20.0, bottom: 20.0),
              child: Text(
                'Platillos',
                style: TextStyle(
                  color: CustomColors.greyColor,
                  fontSize: MediaQuery.of(context).size.width * .08,
                  fontWeight: FontWeight.w600,
                ),
              ),
            ),
          ],
        ),
        //Cards
        Column(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            SizedBox(
              width: MediaQuery.of(context).size.width * .85,
              height: MediaQuery.of(context).size.height * .45,
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
                  Expanded(
                    flex: 1,
                    child: SvgPicture.asset('assets/icons/arrow-left.svg'),
                  ),
                  Expanded(
                    flex: 12,
                    child: CarouselSlider(
                      options: CarouselOptions(
                        aspectRatio: 1,
                        enableInfiniteScroll: false,
                        height: MediaQuery.of(context).size.height,
                        enlargeCenterPage: true,
                        onPageChanged: (index, reason) async {
                          await FirebaseAnalytics.instance.logSelectContent(
                            contentType: 'menu',
                            itemId: index.toString(),
                          );
                          setState(() {
                            _current = index;
                          });
                        },
                      ),
                      items: platillos.map((platillo) {
                        return Builder(
                          builder: (BuildContext ctx) {
                            return generateCards(ctx, platillo);
                          },
                        );
                      }).toList(),
                    ),
                  ),
                  Expanded(
                    flex: 1,
                    child: RotationTransition(
                      turns: AlwaysStoppedAnimation(180 / 360),
                      child: SvgPicture.asset(
                        'assets/icons/arrow-left.svg',
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
        //Paginador.
        getPagination(platillos)
      ],
    );
  }

  //Widget de pagination.
  Row getPagination(List<Platillo> platillos) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: platillos.map((i) {
        int index = platillos.indexOf(i);
        if (_current == index) {
          return Container(
            width: 12.0,
            height: 12.0,
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            decoration: BoxDecoration(
              border: Border.all(
                color: CustomColors.greyColor.withOpacity(0.5),
              ),
              shape: BoxShape.circle,
              color: CustomColors.greyColor[500],
            ),
          );
        } else {
          return Container(
            width: 12.0,
            height: 12.0,
            margin: EdgeInsets.symmetric(horizontal: 4.0),
            decoration: BoxDecoration(
              border: Border.all(
                color: CustomColors.greyColor.withOpacity(0.5),
              ),
              shape: BoxShape.circle,
              color: Colors.white,
            ),
          );
        }
      }).toList(),
    );
  }

  Widget generateCards(BuildContext context, Platillo platillo) {
    return Container(
      margin: EdgeInsets.all(15),
      child: Card(
        shape: RoundedRectangleBorder(
          side: BorderSide(color: CustomColors.primaryColor, width: 1),
          borderRadius: BorderRadius.circular(15),
        ),
        borderOnForeground: true,
        color: CustomColors.primaryColor,
        child: Column(
          children: [
            Expanded(flex: 1, child: Container()),
            Expanded(
              flex: 3,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Center(
                    child: SizedBox(
                      width: MediaQuery.of(context).size.width * .35,
                      //width: 180,
                      child: Center(
                        child: platillo.image == null
                            ? Image.asset(
                                'assets/images/hamburguesa.png',
                                fit: BoxFit.fill,
                              )
                            : Image.network(
                                platillo.image,
                                width: MediaQuery.of(context).size.width * .35,
                                fit: BoxFit.fitWidth,
                                loadingBuilder: (BuildContext context,
                                    Widget child,
                                    ImageChunkEvent loadingProgress) {
                                  if (loadingProgress == null) return child;
                                  return Center(
                                    child: CircularProgressIndicator(
                                      color: CustomColors.greyColor,
                                      value: loadingProgress
                                                  .expectedTotalBytes !=
                                              null
                                          ? loadingProgress
                                                  .cumulativeBytesLoaded /
                                              loadingProgress.expectedTotalBytes
                                          : null,
                                    ),
                                  );
                                },
                              ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              flex: 2,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Flexible(
                    child: Padding(
                      padding: const EdgeInsets.all(5.0),
                      child: Text(
                        platillo.titulo,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          color: Colors.white,
                          fontSize: MediaQuery.of(context).size.width * .06,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  void loadList() {
    //Lunes
    platillosLunes.add(
      Platillo(
          id: 1,
          titulo: 'Chilaquiles',
          image: 'assets/images/chilaquiles.png',
          dia: 'Lunes'),
    );
    platillosLunes.add(
      Platillo(
          id: 2,
          titulo: 'Ensalada',
          image: 'assets/images/ensalada.png',
          dia: 'Lunes'),
    );
    ltsDiasGroup.add(Dia(id: 1, titulo: 'Lunes', platillos: platillosLunes));
    //Martes
    platillosMartes.add(
      Platillo(
          id: 1,
          titulo: 'Hamburguesa',
          image: 'assets/images/hamburguesa.png',
          dia: 'Martes'),
    );
    platillosMartes.add(
      Platillo(
          id: 2,
          titulo: 'Ensalada',
          image: 'assets/images/ensalada.png',
          dia: 'Martes'),
    );
    ltsDiasGroup.add(Dia(id: 2, titulo: 'Martes', platillos: platillosMartes));
    //Miercoles
    platillosMiercoles.add(
      Platillo(
          id: 1,
          titulo: 'Chilaquiles',
          image: 'assets/images/chilaquiles.png',
          dia: 'Miercoles'),
    );
    platillosMiercoles.add(
      Platillo(
          id: 2,
          titulo: 'Ensalada',
          image: 'assets/images/ensalada.png',
          dia: 'Miercoles'),
    );
    ltsDiasGroup
        .add(Dia(id: 3, titulo: 'Miercoles', platillos: platillosMiercoles));
    //Jueves
    platillosJueves.add(
      Platillo(
          id: 1,
          titulo: 'Hamburguesa',
          image: 'assets/images/hamburguesa.png',
          dia: 'Jueves'),
    );
    platillosJueves.add(
      Platillo(
          id: 2,
          titulo: 'Ensalada',
          image: 'assets/images/ensalada.png',
          dia: 'Jueves'),
    );
    ltsDiasGroup.add(Dia(id: 4, titulo: 'Jueves', platillos: platillosJueves));
    platillosViernes.add(
      Platillo(
          id: 1,
          titulo: 'Spaguetti',
          image: 'assets/images/spaguetti.png',
          dia: 'Viernes'),
    );
    platillosViernes.add(
      Platillo(
          id: 2,
          titulo: 'Hamburguesa',
          image: 'assets/images/hamburguesa.png',
          dia: 'Viernes'),
    );
    ltsDiasGroup
        .add(Dia(id: 4, titulo: 'Viernes', platillos: platillosViernes));
  }
}
