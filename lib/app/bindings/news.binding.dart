import 'package:alcorp_app/app/controller/news.controller.dart';
import 'package:alcorp_app/app/data/providers/apis/news.api.dart';
import 'package:alcorp_app/app/data/repository/news.repository.dart';
import 'package:get/instance_manager.dart';
import 'package:http/http.dart' as http;

class NewsBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<NewsController>(
      () => NewsController(
        repository: NewsRepository(
          userClient: NewsClient(httpClient: http.Client()),
        ),
      ),
    );
  }
}
