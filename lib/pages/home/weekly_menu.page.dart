import 'package:alcorp_app/core/custom/colors.dart';
import 'package:alcorp_app/core/models/platillo.model.dart';
import 'package:alcorp_app/core/models/votacion.model.dart';
import 'package:alcorp_app/core/services/votacion.service.dart';
import 'package:alcorp_app/shared/utils/global.utils.dart';
import 'package:alcorp_app/shared/widgets/tab_component.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class WeeklyMenuPage extends StatefulWidget {
  WeeklyMenuPage({Key key}) : super(key: key);

  @override
  _WeeklyMenuPageState createState() => _WeeklyMenuPageState();
}

class _WeeklyMenuPageState extends State<WeeklyMenuPage> {
  Future<List<Platillo>> _getVotingFuture;
  List<Platillo> platillosList;
  int _currentDay = 0;
  int _diasTotales = 0;
  Votacion _votacionActual;
  bool _inVoting = false;

  @override
  void initState() {
    super.initState();
    this._getVotingFuture = this.getVoting();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        leading: IconButton(
          icon: SvgPicture.asset(
            'assets/icons/arrow-left.svg',
            color: CustomColors.greyColor,
          ),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
      ),
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        child: FutureBuilder<List<Platillo>>(
          future: this._getVotingFuture,
          builder: (context, snapshot) {
            if (snapshot.connectionState == ConnectionState.done &&
                platillosList != null &&
                platillosList.length > 0) {
              return _buildCards(context, platillosList);
            } else if (snapshot.connectionState == ConnectionState.waiting &&
                !snapshot.hasError) {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(
                      height: 50.0,
                      width: 50.0,
                      child: CircularProgressIndicator(
                        color: CustomColors.primaryColor,
                      ),
                    ),
                    Text(
                      _currentDay > 0 ? "Cargando la siguiente votacion" : "",
                      style: TextStyle(
                        color: Colors.black,
                        fontWeight: FontWeight.bold,
                        fontSize: 18.0,
                      ),
                    ),
                  ],
                ),
              );
            } else if (snapshot.connectionState == ConnectionState.done &&
                snapshot.hasError) {
              return Center(
                child: Text(
                  'Ocurrio un error al obtener la informacion\nIntente de nuevo mas tarde.',
                  style: TextStyle(fontSize: 16.0),
                ),
              );
            } else {
              return Center(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Icon(
                        Icons.info_outline,
                        size: MediaQuery.of(context).size.width * .25,
                        color: CustomColors.primaryColor,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        'Ya realizaste las votaciones de la semana',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 24.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Text(
                        'Te avisaremos cuando haya nuevas votaciones.',
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontSize: 16.0,
                          fontWeight: FontWeight.bold,
                          color: CustomColors.greyColor,
                        ),
                      ),
                    ),
                  ],
                ),
              );
            }
          },
        ),
      ),
    );
  }

  Column _buildProgressBar(BuildContext context, String dia) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Text(
            dia.toUpperCase(),
            style: TextStyle(
              color: CustomColors.greyColor.withOpacity(.75),
              fontWeight: FontWeight.w900,
              fontSize: 18.0,
            ),
          ),
        ),
        Padding(
          padding: const EdgeInsets.all(12.0),
          child: Container(
            width: MediaQuery.of(context).size.width * .8,
            height: 10.0,
            child: ClipRRect(
              borderRadius: BorderRadius.all(Radius.circular(10)),
              child: LinearProgressIndicator(
                valueColor: AlwaysStoppedAnimation<Color>(
                  CustomColors.primaryColor,
                ),
                value: (_currentDay / _diasTotales),
                backgroundColor: CustomColors.primaryColorDark,
                color: CustomColors.primaryColor,
              ),
            ),
          ),
        )
      ],
    );
  }

  Widget _buildCards(BuildContext context, List<Platillo> platillos) {
    return Stack(
      children: [
        Column(
          children: [
            _buildProgressBar(context, platillos[0].dia),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    left: 5.0,
                    right: 15.0,
                    top: 15.0,
                    bottom: 15.0,
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    child: Material(
                      elevation: 10.0,
                      shape: BeveledRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                      child: Container(
                        color: CustomColors.primaryColor,
                        width: MediaQuery.of(context).size.width * .4,
                        height: MediaQuery.of(context).size.width * .6,
                        child: Column(
                          children: [
                            Expanded(
                                flex: 4,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Center(
                                          child: SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                .35,
                                            child: platillos[0].image != null &&
                                                    platillos[0].image != ''
                                                ? Image.network(
                                                    platillos[0]?.image,
                                                    loadingBuilder: (BuildContext
                                                            context,
                                                        Widget child,
                                                        ImageChunkEvent
                                                            loadingProgress) {
                                                      if (loadingProgress ==
                                                          null) return child;
                                                      return Center(
                                                        child:
                                                            CircularProgressIndicator(
                                                          color: CustomColors
                                                              .greyColor,
                                                          value: loadingProgress
                                                                      .expectedTotalBytes !=
                                                                  null
                                                              ? loadingProgress
                                                                      .cumulativeBytesLoaded /
                                                                  loadingProgress
                                                                      .expectedTotalBytes
                                                              : null,
                                                        ),
                                                      );
                                                    },
                                                  )
                                                : Image.asset(
                                                    'assets/images/hamburguesa.png',
                                                  ),
                                          ),
                                        ),
                                        Center(
                                          child: Text(
                                            platillos[0].titulo,
                                            style: TextStyle(
                                              fontSize: 22.0,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                )),
                            Visibility(
                              visible: platillos[0].votado,
                              child: Expanded(
                                flex: 1,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(5),
                                        ),
                                        child: Container(
                                          color: CustomColors.primaryColorDark
                                              .withOpacity(0.35),
                                          child: Padding(
                                            padding: const EdgeInsets.all(4.0),
                                            child: Text(
                                              platillos[0]
                                                      .porcentaje
                                                      .toString() +
                                                  "%",
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12.0,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      width: MediaQuery.of(context).size.width *
                                          .25,
                                      // width: MediaQuery.of(context).size.width * .2,
                                      //    height:
                                      //         MediaQuery.of(context).size.width *
                                      //             .1,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)),
                                        child: LinearProgressIndicator(
                                          valueColor:
                                              AlwaysStoppedAnimation<Color>(
                                                  Colors.white),
                                          value: platillos[0].porcentaje / 100,
                                          backgroundColor:
                                              CustomColors.primaryColorDark,
                                          color: Colors.white,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Visibility(
                              visible: !platillos[0].votado,
                              child: Expanded(
                                flex: 1,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    GestureDetector(
                                      onTap: () async {
                                        print('Votar por primer platillo');
                                        final idPlatillo = platillos[0].id;
                                        final dia = platillos[0].dia;
                                        await realizarVotacion(idPlatillo,
                                            dia: dia);
                                        sleepPageAfterReload();
                                      },
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(20),
                                        ),
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .22,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .1,
                                          color: Colors.white,
                                          child: Center(
                                            child: Text(
                                              'Votar',
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: CustomColors.greyColor,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 15.0,
                    right: 5.0,
                    top: 15.0,
                    bottom: 15.0,
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    child: Material(
                      elevation: 10.0,
                      shape: BeveledRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                      child: Container(
                        color: CustomColors.primaryColor,
                        width: MediaQuery.of(context).size.width * .4,
                        height: MediaQuery.of(context).size.width * .6,
                        child: Column(
                          children: [
                            Expanded(
                                flex: 4,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Center(
                                          child: SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                .35,
                                            child: platillos[1]?.image !=
                                                        null &&
                                                    platillos[1].image != ''
                                                ? Image.network(
                                                    platillos[1]?.image,
                                                    loadingBuilder: (BuildContext
                                                            context,
                                                        Widget child,
                                                        ImageChunkEvent
                                                            loadingProgress) {
                                                      if (loadingProgress ==
                                                          null) return child;
                                                      return Center(
                                                        child:
                                                            CircularProgressIndicator(
                                                          color: CustomColors
                                                              .greyColor,
                                                          value: loadingProgress
                                                                      .expectedTotalBytes !=
                                                                  null
                                                              ? loadingProgress
                                                                      .cumulativeBytesLoaded /
                                                                  loadingProgress
                                                                      .expectedTotalBytes
                                                              : null,
                                                        ),
                                                      );
                                                    },
                                                  )
                                                : Image.asset(
                                                    'assets/images/hamburguesa.png',
                                                  ),
                                          ),
                                        ),
                                        Center(
                                          child: Text(
                                            platillos[1].titulo,
                                            style: TextStyle(
                                              fontSize: 22.0,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                )),
                            Visibility(
                              visible: platillos[1].votado,
                              child: Expanded(
                                flex: 1,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(5),
                                        ),
                                        child: Container(
                                          color: CustomColors.primaryColorDark
                                              .withOpacity(0.35),
                                          child: Padding(
                                            padding: const EdgeInsets.all(4.0),
                                            child: Text(
                                              platillos[1]
                                                      .porcentaje
                                                      .toString() +
                                                  "%",
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12.0,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      width: MediaQuery.of(context).size.width *
                                          .25,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)),
                                        child: LinearProgressIndicator(
                                          valueColor:
                                              AlwaysStoppedAnimation<Color>(
                                                  Colors.white),
                                          value: platillos[1].porcentaje / 100,
                                          backgroundColor:
                                              CustomColors.primaryColorDark,
                                          color: Colors.white,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Visibility(
                              visible: !platillos[1].votado,
                              child: Expanded(
                                flex: 1,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    GestureDetector(
                                      onTap: () async {
                                        print('Votar por segundo platillo');
                                        final idPlatillo = platillos[1].id;
                                        final dia = platillos[1].dia;
                                        await realizarVotacion(idPlatillo,
                                            dia: dia);
                                        sleepPageAfterReload();
                                      },
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(20),
                                        ),
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .22,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .1,
                                          color: Colors.white,
                                          child: Center(
                                              child: Text(
                                            'Votar',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: CustomColors.greyColor,
                                            ),
                                          )),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(
                    left: 5.0,
                    right: 15.0,
                    top: 15.0,
                    bottom: 15.0,
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    child: Material(
                      elevation: 10.0,
                      shape: BeveledRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                      child: Container(
                        color: CustomColors.primaryColor,
                        width: MediaQuery.of(context).size.width * .4,
                        height: MediaQuery.of(context).size.width * .6,
                        child: Column(
                          children: [
                            Expanded(
                                flex: 4,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Center(
                                          child: SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                .35,
                                            child: platillos[2].image != null &&
                                                    platillos[2].image != ''
                                                ? Image.network(
                                                    platillos[2]?.image,
                                                    loadingBuilder: (BuildContext
                                                            context,
                                                        Widget child,
                                                        ImageChunkEvent
                                                            loadingProgress) {
                                                      if (loadingProgress ==
                                                          null) return child;
                                                      return Center(
                                                        child:
                                                            CircularProgressIndicator(
                                                          color: CustomColors
                                                              .greyColor,
                                                          value: loadingProgress
                                                                      .expectedTotalBytes !=
                                                                  null
                                                              ? loadingProgress
                                                                      .cumulativeBytesLoaded /
                                                                  loadingProgress
                                                                      .expectedTotalBytes
                                                              : null,
                                                        ),
                                                      );
                                                    },
                                                  )
                                                : Image.asset(
                                                    'assets/images/hamburguesa.png',
                                                  ),
                                          ),
                                        ),
                                        Center(
                                          child: Text(
                                            platillos[2].titulo,
                                            style: TextStyle(
                                              fontSize: 22.0,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                )),
                            Visibility(
                              visible: platillos[2].votado,
                              child: Expanded(
                                flex: 1,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(5),
                                        ),
                                        child: Container(
                                          color: CustomColors.primaryColorDark
                                              .withOpacity(0.35),
                                          child: Padding(
                                            padding: const EdgeInsets.all(4.0),
                                            child: Text(
                                              platillos[2]
                                                      .porcentaje
                                                      .toString() +
                                                  "%",
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12.0,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      width: MediaQuery.of(context).size.width *
                                          .25,
                                      // width: MediaQuery.of(context).size.width * .2,
                                      //    height:
                                      //         MediaQuery.of(context).size.width *
                                      //             .1,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)),
                                        child: LinearProgressIndicator(
                                          valueColor:
                                              AlwaysStoppedAnimation<Color>(
                                                  Colors.white),
                                          value: platillos[2].porcentaje / 100,
                                          backgroundColor:
                                              CustomColors.primaryColorDark,
                                          color: Colors.white,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Visibility(
                              visible: !platillos[2].votado,
                              child: Expanded(
                                flex: 1,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    GestureDetector(
                                      onTap: () async {
                                        print('Votar por tercer platillo');
                                        final idPlatillo = platillos[2].id;
                                        final dia = platillos[2].dia;
                                        await realizarVotacion(idPlatillo,
                                            dia: dia);
                                        sleepPageAfterReload();
                                      },
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(20),
                                        ),
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .22,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .1,
                                          color: Colors.white,
                                          child: Center(
                                              child: Text(
                                            'Votar',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: CustomColors.greyColor,
                                            ),
                                          )),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(
                    left: 15.0,
                    right: 5.0,
                    top: 15.0,
                    bottom: 15.0,
                  ),
                  child: ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    child: Material(
                      elevation: 10.0,
                      shape: BeveledRectangleBorder(
                        borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      ),
                      child: Container(
                        color: CustomColors.primaryColor,
                        width: MediaQuery.of(context).size.width * .4,
                        height: MediaQuery.of(context).size.width * .6,
                        child: Column(
                          children: [
                            Expanded(
                                flex: 4,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: [
                                        Center(
                                          child: SizedBox(
                                            width: MediaQuery.of(context)
                                                    .size
                                                    .width *
                                                .35,
                                            child: platillos[3].image != null &&
                                                    platillos[3].image != ''
                                                ? Image.network(
                                                    platillos[3]?.image,
                                                    loadingBuilder: (BuildContext
                                                            context,
                                                        Widget child,
                                                        ImageChunkEvent
                                                            loadingProgress) {
                                                      if (loadingProgress ==
                                                          null) return child;
                                                      return Center(
                                                        child:
                                                            CircularProgressIndicator(
                                                          color: CustomColors
                                                              .greyColor,
                                                          value: loadingProgress
                                                                      .expectedTotalBytes !=
                                                                  null
                                                              ? loadingProgress
                                                                      .cumulativeBytesLoaded /
                                                                  loadingProgress
                                                                      .expectedTotalBytes
                                                              : null,
                                                        ),
                                                      );
                                                    },
                                                  )
                                                : Image.asset(
                                                    'assets/images/hamburguesa.png',
                                                  ),
                                          ),
                                        ),
                                        Center(
                                          child: Text(
                                            platillos[3].titulo,
                                            style: TextStyle(
                                              fontSize: 22.0,
                                              color: Colors.white,
                                              fontWeight: FontWeight.bold,
                                            ),
                                          ),
                                        )
                                      ],
                                    )
                                  ],
                                )),
                            Visibility(
                              visible: platillos[3].votado,
                              child: Expanded(
                                flex: 1,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.all(8.0),
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.all(
                                          Radius.circular(5),
                                        ),
                                        child: Container(
                                          color: CustomColors.primaryColorDark
                                              .withOpacity(0.35),
                                          child: Padding(
                                            padding: const EdgeInsets.all(4.0),
                                            child: Text(
                                              platillos[3]
                                                      .porcentaje
                                                      .toString() +
                                                  "%",
                                              style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                fontSize: 12.0,
                                                color: Colors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Container(
                                      width: MediaQuery.of(context).size.width *
                                          .25,
                                      // width: MediaQuery.of(context).size.width * .2,
                                      //    height:
                                      //         MediaQuery.of(context).size.width *
                                      //             .1,
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.all(
                                            Radius.circular(10)),
                                        child: LinearProgressIndicator(
                                          valueColor:
                                              AlwaysStoppedAnimation<Color>(
                                                  Colors.white),
                                          value: platillos[3].porcentaje / 100,
                                          backgroundColor:
                                              CustomColors.primaryColorDark,
                                          color: Colors.white,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                            Visibility(
                              visible: !platillos[3].votado,
                              child: Expanded(
                                flex: 1,
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  crossAxisAlignment: CrossAxisAlignment.end,
                                  children: [
                                    GestureDetector(
                                      onTap: () async {
                                        print('Votar por cuarto platillo');
                                        final idPlatillo = platillos[3].id;
                                        final dia = platillos[3].dia;
                                        final idResult = await realizarVotacion(
                                            idPlatillo,
                                            dia: dia);
                                        sleepPageAfterReload();
                                      },
                                      child: ClipRRect(
                                        borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(20),
                                        ),
                                        child: Container(
                                          width: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .22,
                                          height: MediaQuery.of(context)
                                                  .size
                                                  .width *
                                              .1,
                                          color: Colors.white,
                                          child: Center(
                                              child: Text(
                                            'Votar',
                                            style: TextStyle(
                                              fontWeight: FontWeight.bold,
                                              color: CustomColors.greyColor,
                                            ),
                                          )),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            ),
          ],
        ),
        Visibility(
          visible: _inVoting,
          child: Container(
            color: Colors.black38,
            height: double.infinity,
            width: double.infinity,
            child: Center(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Container(
                    height: 50.0,
                    width: 50.0,
                    child: CircularProgressIndicator(
                      color: CustomColors.primaryColor,
                    ),
                  ),
                  Text(
                    "Cargando los resultados",
                    style: TextStyle(
                      color: Colors.white,
                      fontWeight: FontWeight.bold,
                      fontSize: 18.0,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ],
    );
  }

  Future<void> sleepPageAfterReload() async {
    ///Mandamos a llamar el set state para que refresque
    ///y se traiga el otro platillo.

    _inVoting = false;

    this.platillosList = await this.getVoting();

    final resulta = await Future<void>.delayed(
      const Duration(seconds: 3),
      () => setState(
        () {
          _inVoting = false;
        },
      ),
    );
  }

  Future<int> realizarVotacion(int idPlatillo, {String dia = "-1"}) async {
    setState(() {
      _inVoting = true;
    });
    var result = await VotacionService.addVotoPlatillo(idPlatillo);
    //Actualizamos los platillos.
    this.platillosList = await this.getVoting(dia: dia, votaciones: false);
    setState(() {});
    return result;
  }

  Future<List<Platillo>> getPorcentaje({String dia = "-1"}) async {
    try {
      final dias = await VotacionService.getPlatillosParaVotacion(dia: dia);
      final platillos = dias.first.platillos;

      return platillos;
    } catch (e) {
      print('Ocurrio un error: $e');
      throw e;
    }
  }

  Future<List<Platillo>> getVoting(
      {String dia = "-1", bool votaciones = true}) async {
    if (votaciones) {
      try {
        var dias = await VotacionService.getPlatillosParaVotacion(dia: dia);
        // Obtenemos el dia actual mediante el index.
        final index = dias.indexWhere((element) => !element.votado);
        _currentDay = index + 1;

        /// Validamos que no esten votados todos los dias.
        if (index == -1) {
          // ignore: deprecated_member_use
          final dataEmpty = new List<Platillo>();
          return dataEmpty;
        }
        //Obtenemos los dias totales
        this._diasTotales = dias.length;

        _votacionActual = dias[_currentDay - 1];
        dias[_currentDay - 1].platillos.sort((a, b) => a.id.compareTo(b.id));
        platillosList = dias[_currentDay - 1].platillos;
        _inVoting = false;
        return dias[_currentDay - 1].platillos;
      } catch (e) {
        print('Ocurrio un error: $e');
        throw e;
      }
    } else {
      try {
        List<Platillo> platillos = await getPorcentaje(dia: dia);
        platillos.forEach((element) {
          element.votado = true;
        });
        platillos.sort((a, b) => a.id.compareTo(b.id));
        platillosList = platillos;
        _inVoting = false;

        return platillos;
      } catch (e) {
        print('Ocurrio un error al obtener el porcentaje: $e');
        throw e;
      }
    }
  }
}
