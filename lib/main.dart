import 'dart:io';
// My imports
import 'package:alcorp_app/app/core/routes/router.dart';
import 'package:alcorp_app/app/shared/widgets/tab_component.dart';
import 'package:alcorp_app/firebase_options.dart';
import 'package:alcorp_app/app/pages/Incident/incident.page.dart';
import 'package:alcorp_app/app/pages/Incident/incident_detail.page.dart';
import 'package:alcorp_app/app/pages/auth/login_page.dart';
import 'package:alcorp_app/app/pages/home/home_page.dart';
import 'package:alcorp_app/app/pages/home/othe_metrics.page.dart';
import 'package:alcorp_app/app/pages/home/weekly_menu.page.dart';
import 'package:alcorp_app/app/pages/home/work_team.page.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
<<<<<<< HEAD
import 'package:jwt_decode/jwt_decode.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../shared/utils/globals.dart' as globals;
import 'core/custom/colors.dart';
=======
import 'package:get/get.dart';
import 'package:new_version/new_version.dart';
import 'package:alcorp_app/app/core/custom/colors.dart';
import 'package:firebase_core/firebase_core.dart';

import 'app/routes/app_pages.dart';
>>>>>>> release/1.0.25

void main() async {
  HttpOverrides.global = new MyHttpOverrides();
  WidgetsFlutterBinding.ensureInitialized();
  //Firebase configuration
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );
  await FirebaseAnalytics.instance.logAppOpen();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
<<<<<<< HEAD
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: CustomColors.primaryColor,
        fontFamily: 'Roboto',
      ),
      home: LoginPage(),
=======
    // Statusbar color
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarColor: Colors.transparent),
    );

    // Orientacion
    SystemChrome.setPreferredOrientations(
      [DeviceOrientation.portraitUp, DeviceOrientation.portraitDown],
>>>>>>> release/1.0.25
    );
    return MaterialApp(
        //getPages: AppPages.pages,
        debugShowCheckedModeBanner: false,
        theme: ThemeData(
          primaryColor: CustomColors.primaryColor,
          fontFamily: 'Roboto',
          hintColor: CustomColors.greyColor,
          focusColor: CustomColors.primaryColor,
          disabledColor: CustomColors.greyColor,
          indicatorColor: CustomColors.primaryColor,
        ),
        // },
        home: TabBarComponent(currentPageIndex: 0)
        // home: LoginPage(),
        // home: OtherMetricsPage(),
        );
  }
}

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}
